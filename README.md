# Simulation Framework
This project builds a simulation environment for trajectory planning and motion control.

## Documentation
This project consists of four ROS packages, which are:      
`vehicle_description `: This package describes a vehicle model with Ackermann steering.      
` vehicle_control`: Package for the control of joints and localization of vehicle model.    
` vehicle_einspurmodell`: Package for lateral control.     
`vehicle_pathplanning`: Package for trajectory planning with RRT* and Spline.     

All ROS nodes are written in C++. `rosdoc_lite` is used to generate the Documentation with `Doxygen`. The detailed C++ class information is stored in `doc/html/index.html` file for each ROS `package`. (See https://industrial-training-master.readthedocs.io/en/melodic/_source/session6/Documentation-Generation.html for further information about `rosdot`, also https://developer.lsst.io/cpp/api-docs.html for `Doxygen`).

## Dependencies
This project is running in `ros-kinetic` and need following dependencies, you can install them with:
```shell
sudo apt-get install ros-kinetic-gazebo-ros-control
sudo apt-get install ros-kinetic-effort-controllers
sudo apt-get install ros-kinetic-velocity-controllers
sudo apt-get install ros-kinetic-joint-state-controller
sudo apt-get install ros-kinetic-controller-manager
sudo apt-get install ros-kinetic-map-server
```
The full list of dependencies can be found in the `package.xml` file of each package.

## Installation
Clone all these packages to your `workspace`, maybe `catkin_ws`:
```shell
cd ~/catkin_ws/src
git clone git@bitbucket.org:MiralemSaljanin/mpros.git
```
Or you can just download them if you cannot enter the BitBucket repository.    
Then run `catkin_make` to build it:
```shell
cd ~/catkin_ws
catkin_make
source devel/setup.bash
```


## Hardware and Software
Laptop:  
`CPU: i5 9300H`, `GPU: Nvdia 1650`, `RAM: 16G DDR4`, `SSD: 1T`

VMware:  
`CPU: 3 core for Ubuntu 16.04`, `RAM: 6G`, `SSD: 30G`

Software:  
`Gazebo: 7.16.1`, `Compile: C++ 11`, `Rviz 1.12.17`

Map Building Software:     
`KolourPaint: 4.14.16`

## Attention
Some `nodes` use `csv` files to load and store simulation data, but they are all `absolute path`. You may want to replace them with your own file paths to ensure the correct operation of the program:    
```c++
vehicle_control package: waypoint_load.cpp, 
    line 36: // load test trajectory for testing of lateral controller
    ifstream waypoints_csv("/home/yang/catkin_ws/src/vehicle_control/csv/SplineAll.csv");

vehicle_pathplanning package: CubicBSpline.cpp, 
    line 24: // load the initial path points for spline interpolation
    ifstream waypoints_csv("/home/yang/catkin_ws/src/vehicle_pathplanning/csv/InitialWayPoints.csv");
    line 237: // save the interpolated trajectory, same file as used in waypoint_load.cpp, line 36
    outputSplineAll.open("/home/yang/Spline/SplineAll.csv", ios::out);

vehicle_pathplanning package: RRTSpline.cpp, 
    line 224: // save the interpolated RRTStar trajectory
    outputSplineAll.open("/home/yang/Spline/RRTSplineAll.csv", ios::out);

vehicle_enspurmodell package: main.cpp, // do not have to change
    line 81: // save the simulation result in the path of current Terminal
    outFile_LQR_Result.open("LQRResult.csv", ios::out);
```

## Quick Start
To start the simulation environment, run:
```shell
roslaunch vehicle_control vehicle_control.launch
```
This command will launch the vehicle model in gazebo simulator, Rviz for visualization, nodes in vehicle_control package for controlling the joints and localization in Gazebo. At same time, vehicle_control.launch file includes the vehicle_pathplanning.launch, which also launches a preselected map for test.

If an error occurs when running Gazebo, you can try to upgrade it to `Gazebo 7.16.0`. (See this issue for more information about upgrading:  https://github.com/siemens/ros-sharp/issues/151)

You should see the vehicle model in the left bottom of test map, as shown below:   
![](Media/SimulationStart.png)

If you cannot see the environment, use "Add" button to add some topic in Rviz. First, set "Fixed Frame" to ```world```, then set map topic ```/map```.  

In oder to run trajectory planning module, run the following command in a new `Terminal`:
```shell
rosrun vehicle_pathplanning path_main
```
A global trajectory should be shown as below:      
![](Media/Trajectory.png)    

Make sure you have already added two path topic `rrt_spline/path` and `rrt_star/path` in Rviz.    
The start and goal point is predefined in `RRTStar.cpp`.    
More information can be found in program and `README` file of `vehicle_pathplanning` package.     

The detailed trajectory information is shown in `Terminal`:     
![](Media/Planning.png)

After getting the global trajectory, run following command to move the vehicle:
```shell
rosrun vehicle_einspurmodell main
```
Vehicle starts to track the trajectory by using `LQR lateral controller`.      
The forward velocity is set in `RRTSpline.h` with a default value `10m/s`.    
![](Media/Motion.png)

The whole topics should be like:
![](Media/rosgraph.png)

### Rviz view 
If you want to have a third person view of the vehicle model, you can add the target frame in Rviz (right side of picture below) and set it to base_link.    
In this view, the analyse of vehicle's motion could be little easier.   
This kind of view looks like:   
![](Media/newRviz.png)




## Note 
The previous mentioned quick start is just for the entire simulation.    

### Trajectory planning
If you want to test some trajectory planning algorithm like in `Section 4.3`, just run:
```shell
roslaunch vehicle_pathplanning vehicle_pathplanning.launch
```
and open Rviz by using `rviz` command in the new Terminal.   

The map used in this test can be found in `/map` folder in `vehicle_pathplanning` package. By changing `vehicle_pathplanning.launch`, you can load different maps. Always remember to change to goal point after using a new map. Detailed process can be found in `README` of `vehicle_pathplanning` package.

Also, you can use `KolourPaint` to modify the `.pgm` map. The map of University Stuttgart is just a screenshot from website https://www.scribblemaps.com/create/#/lat=48.74969874&lng=9.11189853&z=17&t=custom_style, push the button `custom`, change it to `black/white`:   
![](Media/map.png)   

Then use `Advanced Mode`:     
![](Media/map2.png)   


### Motion Control
If you want to test the lateral controller like in `Section 5.4`, first find `vehicle_control.launch`, disable the `vehicle_pathplanning.launch`, and then enable the `waypoints_pub` node.    

The test trajectory should be published to `/global/bahn` topic. Add `/global/path` topic in Rviz for visualization.


## To Do
Add longitudinal controller;   

Add velocity profile for the interpolated trajectory.


