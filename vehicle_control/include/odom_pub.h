/**
 * @file odom_pub.h
 * 
 * @brief publish the trajectory of vehiclemodel
 * 
 * subscribe topic /vehicle/center_pose and publish odom topic
 *          
 * @author Yang Qiu st163984@stud.uni-stuttgart.de
 * 
 * @date 2021-01-29
 */

#ifndef _ODOM_PUB_H
#define _ODOM_PUB_H

#include <sstream>
#include <cmath>

#include <ros/ros.h>

#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>

#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>

/**
 * @brief publish the odometry of vehicle model
 * 
 * just for visualization in Rviz
 */
class OdomPublisher
{
private:
    /// subscribe /vehicle/center_pose
    ros::Subscriber pose_sub;

    /// publish odometry
    ros::Publisher odom_pub;

    /// timer to set publish rate
    ros::Timer timer_pose_pub;

    /// default ROS odometry message
    nav_msgs::Odometry odom;

    /// time in odometry
    ros::Time current_time;

public:
    /**
     * @brief Construct a new Odom Publisher object
     * 
     * @param nh nodehandle
     */
    OdomPublisher(ros::NodeHandle *nh);

    /**
     * @brief Destroy the Odom Publisher object
     * 
     */
    virtual ~OdomPublisher();

    /// callback for subscriber, get current position
    void poseCallback(const geometry_msgs::PoseStamped &pose_msg);

    /// callback for timer
    void timerCallback(const ros::TimerEvent &event);

    /// publish odom to /vehicle/odom
    void odometry_pub();
};
#endif