/**
 * @file cmdvel_pub.h
 * 
 * @brief publish messages to control the vehiclemodel
 * 
 * subscribe /vehicle/cmd_vel topic (type: geometry_msgs::Twist) and
 * compute the commands for joints separately. then publish this commands
 * 
 * @author Yang Qiu st163984@stud.uni-stuttgart.de
 * 
 * @date 2021-01-08
 */

#ifndef _CMDVEL_PUB_H
#define _CMDVEL_PUB_H

#include <sstream>
#include <iostream>
#include <cmath>
#include <algorithm>

#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <geometry_msgs/Twist.h>

/**
 * @brief Control the movements of vehicle joints
 * 
 * Subscribing cmd_vel from lateral controller and calculate the 
 * command for each joints, then publish this command. The topics 
 * is defined in vehicle_control pkg model_control.yaml file
 * 
 * Detailed calculation process is described in Section 3.2.2
 */
class CmdVelPublisher
{
private:
    /* Subscriber and Publisher */
    ros::Subscriber cmd_vel_sub;                            // subscribe /vehicle/cmd_vel topic

    ros::Publisher steer_fL_pub;                            // publish the position command for front left_steering_position_controller
    ros::Publisher steer_fR_pub;                            // publish the position command for front right_steering_position_controller
    ros::Publisher steer_rL_pub;                            // publish the position command for rear left_steering_position_controller
    ros::Publisher steer_rR_pub;                            // publish the position command for rear right_steering_position_controller

    ros::Publisher drive_fL_pub;                            // publish the velocity command for left_back_wheel_velocity_controller
    ros::Publisher drive_fR_pub;                            // publish the velocity command for right_back_wheel_velocity_controller
    ros::Publisher drive_rL_pub;                            // publish the velocity command for left_back_wheel_velocity_controller
    ros::Publisher drive_rR_pub;                            // publish the velocity command for right_back_wheel_velocity_controller

    ros::Timer timer_cmd_pub;                               // timer to set publish rate

    /* msg when z != 0, vehicle steering, command for joints are different */
    std_msgs::Float64 msgDrive_fL;                          // front left wheel angular velocity
    std_msgs::Float64 msgDrive_fR;                          // front right wheel angular velocity
    std_msgs::Float64 msgDrive_rL;                          // rear left wheel angular velocity
    std_msgs::Float64 msgDrive_rR;                          // rear right wheel angular velocity
    std_msgs::Float64 msgSteering_fL;                       // front left wheel steering position
    std_msgs::Float64 msgSteering_fR;                       // front right wheel steering position
    std_msgs::Float64 msgSteering_rL;                       // rear left wheel steering position
    std_msgs::Float64 msgSteering_rR;                       // rear right wheel steering position

    /* msg when z = 0, go straight, command for left and right wheels are same */
    std_msgs::Float64 msgDrive;                             // rear wheel angular velocity
    std_msgs::Float64 msgSteering;                          // front wheel steering position

    double x = 0;                                           // initial velocity
    double z = 0;                                           // initial steering angle

    double L;                                               // wheelbase L = lf +lr
    double lw;                                              // track width
    double wheelRadius;                                     // wheel radius
    double maxSteeringAngle;                                // max steering angle for inner wheel
    double rInnerMin;                                       // min turing radius for inner wheel
    double rCenterMin;                                      // min turing radius for center of vehicle
    double maxSteeringCenter;                               // max steering angle for vehicle center

public:
    /**
     * @brief Construct a new Cmd Vel Publisher object
     * 
     * @param nh A reference to nodehandel
     */
    CmdVelPublisher(ros::NodeHandle *nh);

    /**
     * @brief Destroy the Cmd Vel Publisher object
     * 
     */
    virtual ~CmdVelPublisher();

    /**
     * @brief call back function for subscriber
     * 
     * @param cmd_msg the command from motion controller
     */
    void cmdvelCallback(const geometry_msgs::Twist &cmd_msg);

    /**
     * @brief callback func for timer
     * 
     * @param event defalt timer event
     */
    void timerCallback(const ros::TimerEvent &event);

    /**
     * @brief publish velocity and position command for joints
     * 
     */
    void cmdVelPub();
};

#endif