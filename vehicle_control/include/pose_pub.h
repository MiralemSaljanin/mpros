/**
 * @file pose_pub.h
 * 
 * @author Yang Qiu st163984@stud.uni-stuttgart.de
 * 
 * @brief publish the position and velocity of vehiclemodel
 * 
 * subscribe topic gazebo_msgs/ModelStates and convert to three new
 * topic: /vehicle/center_pose; /vehicle/rear_pose; /vehicle/velocity.
 * 
 * @version 0.1
 * 
 * @date 2021-02-21
 * 
 * @copyright Copyright (c) 2021
 */
#ifndef _POSE_PUB_H
#define _POSE_PUB_H

#include <sstream>
#include <cmath>

#include <ros/ros.h>
#include <std_msgs/String.h>

#include <gazebo_msgs/ModelStates.h>

#include <tf2/LinearMath/Quaternion.h>
#include <tf/transform_datatypes.h>
#include <tf/LinearMath/Matrix3x3.h>

#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Quaternion.h>

/**
 * @brief publish current position and velocity by subscribing Gazebo topic
 * 
 */
class PosePublisher
{
private:
    /// subscribe gazebo/model_states topic, about 900hz
    ros::Subscriber modelStates_sub;

    /// timer to set publish rate, 50hz
    ros::Timer timer_pose_pub;

    /// publish the pose of vehicle center point
    ros::Publisher center_pose_pub;
    /// publish the pose of rear axle center point
    ros::Publisher rear_pose_pub;
    /// publish the velocity of vehicle
    ros::Publisher vel_pub;

    /// message to be published for center_pose_pub
    geometry_msgs::PoseStamped center_pose;
    /// message to be published for rear_pose_pub
    geometry_msgs::PoseStamped rear_pose;
    /// message to be published for vel_pub
    geometry_msgs::Twist veolcity;

    /// storage Pose from ModelStates
    geometry_msgs::Pose vehicle_position;
    /// storage Twist from ModelStates
    geometry_msgs::Twist vehicle_velocity;
    /// storage Quaternion from ModelStates.Pose
    geometry_msgs::Quaternion vehicle_orientation;

public:
    /**
     * @brief Construct a new Pose Publisher object
     * 
     * @param nh modehandle
     */
    PosePublisher(ros::NodeHandle *nh);

    virtual ~PosePublisher();

    /**
     * @brief call back function for subscriber
     * 
     * @param vehicle_curr_pose 
     */
    void modelStatesCallback(const gazebo_msgs::ModelStates &vehicle_curr_pose);

    /**
     * @brief callback func for timer
     * 
     * @param event 
     */
    void timerCallback(const ros::TimerEvent &event);
};

#endif
