/**
 * @file waypoint_load.h
 * 
 * @author Yang Qiu st163984@stud.uni-stuttgart.de
 * 
 * @brief load waypoints from csv file and publish Path topic
 * 
 * two topic will be published:
 *    1. /global/path with nav_msgs::Path message, only this type of message
 *       can be showed in Rviz
 *    2. /global/bahn with vehicle_control::Bahn message. This message is customized,
 *       it includes not only position information, but also velocity and curvature
 *       information.
 * 
 * @note the node is just used to load a preset smooth trajectory for testing
 * lateral controller
 * 
 * @attention
 * csv file path is absolute path, change it befor use
 * 
 * @date 2021-01-15
 */

#ifndef _WAYPOINT_LOAD_H
#define _WAYPOINT_LOAD_H

#include <string>
#include <cmath>
#include <fstream>
#include <iostream>

#include <ros/ros.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <nav_msgs/Path.h>

#include <vehicle_control/Bahn.h>
#include <vehicle_control/Waypoint.h>

/**
 * @brief load waypoints from csv file and publish Path topic
 * 
 */
class WaypointsPublisher
{
private:
    /// publish messages to /global/bahn topic
    ros::Publisher bahn_pub;
    /// publish messages to /global/path topic
    ros::Publisher path_pub;

    /// Bahn message to be pulished
    vehicle_control::Bahn global_bahn;
    /// Path message to be pulished
    nav_msgs::Path global_path;

    /// storage single waypoint from csv file
    vehicle_control::Waypoint single_waypoint;
    /// storage single pathpoint from csv file
    geometry_msgs::PoseStamped single_pathpoint;

public:
    WaypointsPublisher(ros::NodeHandle *nh);

    virtual ~WaypointsPublisher();

    /**
     * @brief load csv file and assign messages
     * 
     */
    void waypoints_csv_loader();

    /**
     * @brief publish messages
     * 
     */
    void pub();
};

#endif