/**
 * @file tf_pub.h
 * 
 * @brief publish tf between /world and /base_link
 * 
 * subscribe /vehicle/center_pose topic and use TF to convert it to /world 
 * frame, so the movements of vehiclemodel can be showed in Rviz 
 *          
 * @author Yang Qiu st163984@stud.uni-stuttgart.de
 * 
 * @date 2021-01-12
 */
#ifndef _TF_PUB_H
#define _TF_PUB_H

#include <sstream>

#include <ros/ros.h>
#include <tf/transform_broadcaster.h>
#include <geometry_msgs/PoseStamped.h>

/**
 * @brief publish the transform between /world and /base_link
 * 
 * make the vehicle have the correct transform in Rviz
 */
class TransformPublisher
{
private:
    /// subscribe /vehicle/center_pose topic
    ros::Subscriber vehicle_pose_sub;

    /// message from /vehicle/center_pose topic
    geometry_msgs::Pose p;

    /// tf broadcaster
    tf::TransformBroadcaster tf_broadcaster;
    
    /// broadcastered message
    geometry_msgs::TransformStamped tf_msg;

public:
    /**
     * @brief Construct a new Transform Publisher object
     * 
     * @param nh 
     */
    TransformPublisher(ros::NodeHandle *nh);

    /**
     * @brief Destroy the Transform Publisher object
     * 
     */
    virtual ~TransformPublisher();

    /// callback function for subscriber
    void tfCallback(const geometry_msgs::PoseStamped &vehicle_center_pose);    
};

#endif