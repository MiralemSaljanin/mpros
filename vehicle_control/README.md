# README
This is a package for the control and loaclization of vehicle model.   

The urdf file of ackermann vehicle model can be found in package vehicle_description.

# Usage

after catkin_make and source run:

```shell
roslaunch vehicle_control vehicle_control.launch
```

the model and path will be showed in Gazebo and rviz, at same time 4 nodes runs.

![](pic/rviz.png)

# Nodes

## pose_pub: 

This node is used for the localization of the model.  

See Section 3.3.1;  

It subscribes topic gazebo/model_states and converts message into three new messages, include the pose of vehicle center point, the pose of rear axle center point, and the velocity of vehicle center point.        

Then publish this messages into /vehicle/center_pose, /vehicle/rear_pose, /vehicle/velocity topic.  
Use this command to show this messages:

```shell
rostopic echo /vehicle/center_pose
rostopic echo /vehicle/rear_pose
rostopic echo /vehicle/velocity
```

## cmdvel_pub: 

This node is used to control the movement of vehicle model.  

See Section 3.2.2;  

It subscribe topic /vehicle/cmd_vel from lateral controller, and then compute and publish the command for rear_wheel_velocity_controller and steering_position_controller.   
linear.x and angular.z in /vehicle/cmd_vel represent the velocity(m/s) and steering angel(rad):  

```shell
rostopic pub /vehicle/cmd_vel geometry_msgs/Twist "linear:
  x: 3.0
  y: 0.0
  z: 0.0
angular:
  x: 0.0
  y: 0.0
  z: 0.5" 
```
or use rqt_gui to set this topic: 
```
rosrun rqt_gui rqt_gui
```

## tf_pub: 

This node provides a tf between /world and /base_link. 

So the vehicle can move in Rviz

![](pic/rviz_tf.png)



## waypoints_pub:

This node is only activated when test lateral controller.  
See Section 4.2.2

SplineAll.casv file contains a trajectory test bench.  

Load SplineAll.csv and publish message nav_msg::Path /global/path. The path can be showed in Rviz.  
At same time a customized message vehicle_control::Bahn /global/bahn is been published.   
Compare to nav_msg::Path, vehicle_control::Bahn contains velocity and curvature information.    

## odom_pub
Publish odom message to topic /vehicle/odom to show the current-trajektorie in Rviz.  


# Topic:

| topic name | message type | rate (Hz) |
| ------ | ------ | ------ |
| /vehicle/center_pose | geometry_msgs::PoseStamped | 100 |
| /vehicle/rare_pose | geometry_msgs::PoseStamped | 100 |
| /vehicle/velocity | geometry_msgs::Twist | 100 |
| /vehicle/odom | nav_msgs::Odometry | 100 |
| /global/bahn | vehicle_control::Bahn | static(latch = true) |
| /global/path | nav_msgs::Path | static(latch = true) |


