/**
 * @file tf_pub.cpp
 * 
 * @brief publish tf between /world and /base_link
 * 
 * subscribe /vehicle/center_pose topic and use TF to convert it to /world 
 * frame, so the movements of vehiclemodel can be showed in Rviz 
 *          
 * @author Yang Qiu st163984@stud.uni-stuttgart.de
 * 
 * @date 2021-01-12
 */

#include <tf_pub.h>

TransformPublisher::TransformPublisher(ros::NodeHandle *nh)
{
    vehicle_pose_sub = nh->subscribe("/vehicle/center_pose", 10, &TransformPublisher::tfCallback, this);
}

TransformPublisher::~TransformPublisher(){};

void TransformPublisher::tfCallback(const geometry_msgs::PoseStamped &vehicle_center_pose)
{

    p.position = vehicle_center_pose.pose.position;
    p.orientation = vehicle_center_pose.pose.orientation;

    tf_msg.header.stamp = ros::Time::now();
    tf_msg.header.frame_id = "world";
    tf_msg.child_frame_id = "base_link";
    tf_msg.transform.translation.x = p.position.x;
    tf_msg.transform.translation.y = p.position.y;
    tf_msg.transform.translation.z = p.position.z;
    tf_msg.transform.rotation.x = p.orientation.x;
    tf_msg.transform.rotation.y = p.orientation.y;
    tf_msg.transform.rotation.z = p.orientation.z;
    tf_msg.transform.rotation.w = p.orientation.w;

    tf_broadcaster.sendTransform(tf_msg);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "tf_pub");

    ros::NodeHandle nh;

    TransformPublisher tfpub = TransformPublisher(&nh);

    ros::spin();

    return 0;
}