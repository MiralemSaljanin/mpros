/**
 * @file odom_pub.cpp
 * 
 * @brief publish the trajectory of vehiclemodel
 * 
 * subscribe topic /vehicle/center_pose and publish odom topic
 *          
 * @author Yang Qiu st163984@stud.uni-stuttgart.de
 * 
 * @date 2021-01-29
 */

#include <odom_pub.h>

OdomPublisher::OdomPublisher(ros::NodeHandle *nh)
{
    pose_sub = nh->subscribe("/vehicle/center_pose", 10, &OdomPublisher::poseCallback, this);

    timer_pose_pub = nh->createTimer(ros::Duration(1.0 / 100.0), &OdomPublisher::timerCallback, this);

    odom_pub = nh->advertise<nav_msgs::Odometry>("/vehicle/odom", 50);
}

OdomPublisher::~OdomPublisher(){};


void OdomPublisher::poseCallback(const geometry_msgs::PoseStamped &pose_msg)
{
    odom.pose.pose = pose_msg.pose;
    // std::cout << "pose get " << std::endl;
}

void OdomPublisher::timerCallback(const ros::TimerEvent &event)
{
    odometry_pub();
}

void OdomPublisher::odometry_pub()
{
    current_time = ros::Time::now();

    odom.header.stamp = current_time;
    odom.header.frame_id = "world";

    odom_pub.publish(odom);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "odom_pub");

    ros::NodeHandle nh;

    OdomPublisher odomPub = OdomPublisher(&nh);

    ros::spin();

    return 0;
}