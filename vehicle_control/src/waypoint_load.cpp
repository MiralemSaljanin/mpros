#include <waypoint_load.h>

using namespace std;

WaypointsPublisher::WaypointsPublisher(ros::NodeHandle *nh)
{
    // latch is true for static message
    bool latch = true;
    bahn_pub = nh->advertise<vehicle_control::Bahn>("/global/bahn", 10, latch);
    path_pub = nh->advertise<nav_msgs::Path>("/global/path", 10, latch);

    waypoints_csv_loader();
    pub();
}

WaypointsPublisher::~WaypointsPublisher(){}

/// @warning change the file path before use
void WaypointsPublisher::waypoints_csv_loader()
{
    // csv file absolute path
    ifstream waypoints_csv("/home/yang/catkin_ws/src/vehicle_control/csv/SplineAll.csv");

    // csv file relative path
    //ifstream waypoints_csv("../csv/SplineAll.csv");

    if (!waypoints_csv.is_open())
        ROS_INFO("Error loading CSV");
    if (waypoints_csv.is_open())
        ROS_INFO("CSV loaded");

    string line;

    string x_str;
    string y_str;
    string yaw_str;
    string kappa_str;
    string velocity_str;

    tf2::Quaternion q;

    while (getline(waypoints_csv, line))
    {
        istringstream sin(line);
        vector<string> Points;
        string info;
        // read csv line by line, until oef
        while (getline(sin, info, ','))
        {
            Points.push_back(info);
        }
        // storage the x, y, yaw and curvature each line
        x_str = Points[0];
        y_str = Points[1];
        yaw_str = Points[2];
        kappa_str = Points[3];
        velocity_str = Points[4];

        // string to double by using stringstream
        double x, y, yaw, kappa, velocity;
        stringstream sx, sy, syaw, skappa, svelocity;
        sx << x_str;
        sy << y_str;
        syaw << yaw_str;
        skappa << kappa_str;
        svelocity << velocity_str;
        sx >> x;
        sy >> y;
        syaw >> yaw;
        skappa >> kappa;
        svelocity >> velocity;

        // assign single_waypoint
        single_waypoint.pose.pose.position.x = x;
        single_waypoint.pose.pose.position.y = y;
        single_waypoint.twist.twist.linear.x = velocity;
        single_waypoint.curvature = kappa;
        q.setRPY(0, 0, yaw);
        q.normalize();
        single_waypoint.pose.pose.orientation = tf2::toMsg(q);
        // push back single_waypoint to global_bahn
        global_bahn.waypoints.push_back(single_waypoint);

        // assign single_pathpoint
        single_pathpoint.pose.position.x = x;
        single_pathpoint.pose.position.y = y;
        // push back single_pathpoint to global_path
        global_path.poses.push_back(single_pathpoint);
    }

    global_bahn.header.frame_id = "/map";

    global_path.header.frame_id = "map";
}

void WaypointsPublisher::pub()
{
    bahn_pub.publish(global_bahn);
    path_pub.publish(global_path);
    ROS_INFO("Path published");
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "waypoints_pub");

    ros::NodeHandle nh;

    WaypointsPublisher wp_pub = WaypointsPublisher(&nh);

    ros::spin();
}
