#include <cmdvel_pub.h>

CmdVelPublisher::CmdVelPublisher(ros::NodeHandle *nh)
{
   
    L = 3.6;
    lw = 2;
    wheelRadius = 0.3515;
    maxSteeringAngle = 0.6;
    rInnerMin = L / tan(maxSteeringAngle);
    rCenterMin = rInnerMin + lw / 2.0;
    maxSteeringCenter = atan(L / rCenterMin);
    
    /* Subscription for Controller Command */
    cmd_vel_sub = nh->subscribe("/vehicle/cmd_vel", 10, &CmdVelPublisher::cmdvelCallback, this);
    timer_cmd_pub = nh->createTimer(ros::Duration(1.0 / 100.0), &CmdVelPublisher::timerCallback, this);

    /* Subscriptions for Current Steering and Drive Status */
    steer_fL_pub = nh->advertise<std_msgs::Float64>("/vehiclemodel/front_left_steering_position_controller/command", 10);
    steer_fR_pub = nh->advertise<std_msgs::Float64>("/vehiclemodel/front_right_steering_position_controller/command", 10);
    steer_rL_pub = nh->advertise<std_msgs::Float64>("/vehiclemodel/rear_left_steering_position_controller/command", 10);
    steer_rR_pub = nh->advertise<std_msgs::Float64>("/vehiclemodel/rear_right_steering_position_controller/command", 10);

    drive_fL_pub = nh->advertise<std_msgs::Float64>("/vehiclemodel/front_left_wheel_velocity_controller/command", 10);
    drive_fR_pub = nh->advertise<std_msgs::Float64>("/vehiclemodel/front_right_wheel_velocity_controller/command", 10);
    drive_rL_pub = nh->advertise<std_msgs::Float64>("/vehiclemodel/rear_left_wheel_velocity_controller/command", 10);
    drive_rR_pub = nh->advertise<std_msgs::Float64>("/vehiclemodel/rear_right_wheel_velocity_controller/command", 10);
}

CmdVelPublisher::~CmdVelPublisher(){}

void CmdVelPublisher::cmdvelCallback(const geometry_msgs::Twist &cmd_msg)
{
    // wheel angular velocity, omega = v / radius
    // x = cmd_msg.linear.x / wheelRadius;
    x = cmd_msg.linear.x;
    // steering angle, Range:(-maxSteeringCenter, maxSteeringCenter)
    z = cmd_msg.angular.z;

    if (z > maxSteeringCenter)
    {
        z = maxSteeringCenter;
    }
    if (z < -maxSteeringCenter)
    {
        z = -maxSteeringCenter;
    }
}

void CmdVelPublisher::timerCallback(const ros::TimerEvent &event)
{
    cmdVelPub();
}

void CmdVelPublisher::cmdVelPub()
{
    if (z != 0)
    {
        /* when steering
        if z > 0, turning left, right rear wheel should be faster
        steering angle for left front wheel will be bigger */
        
        double rCenter = L / fabs(tan(z));                                  // turning radius for vehicle center line
        double rL = rCenter - copysign(1, 0.5*z) * (lw / 2);                // turning radius for left wheel
        double rR = rCenter + copysign(1, 0.5*z) * (lw / 2);                // turning radius for right wheel

        // msgWheelL.data = 0.5* x * rL / rCenter;                          // left rear wheel velocity
        // msgWheelR.data = 0.5* x * rR / rCenter;                          // right rear wheel velocity
        // msgSteering_fL.data = atan(L / rL) * copysign(1, z);             // left front wheel steering angle (rad)
        // msgSteering_fR.data = atan(L / rR) * copysign(1, z);             // right front wheel steering angle (rad)

        /* Allocate Steering and Drive Control Input */
        // msgSteering_fL.data = atan(L / rL) * copysign(1, 0.5*z);
        // msgSteering_fR.data = atan(L / rR) * copysign(1, 0.5*z);
        // msgSteering_rL.data = atan(L / rL) * copysign(1, -0.5*z); 
        // msgSteering_rR.data = atan(L / rR) * copysign(1, -0.5*z);
        msgSteering_fL.data = z;
        msgSteering_fR.data = z;
        msgSteering_rL.data = -z; 
        msgSteering_rR.data = -z;

        msgDrive_fL.data = 0.25 * x;                                        //Control input is Motor Torque M
        msgDrive_fR.data = 0.25 * x;                                        //P = M*w -> w = P/M -> v = w*r_dyn
        msgDrive_rL.data = 0.25 * x;                                        //P = 2,7 KW
        msgDrive_rR.data = 0.25 * x; 

        // double P = 2700;
        // double omega = P/x; 
        // double rdyn = 0.3515; 
        // double v = omega*rdyn; 
        // msgDrive_fL.data = 0.25*v; 
        // msgDrive_fR.data = 0.25*v;                                 
        // msgDrive_rL.data = 0.25*v;                                        
        // msgDrive_rR.data = 0.25*v; 

        

        /** Publishing Steering and Drive Messages **/
        steer_fL_pub.publish(msgSteering_fL);
        steer_fR_pub.publish(msgSteering_fR);
        steer_rL_pub.publish(msgSteering_rL);
        steer_rR_pub.publish(msgSteering_rR);
        drive_fL_pub.publish(msgDrive_fL);
        drive_fR_pub.publish(msgDrive_fR);
        drive_rL_pub.publish(msgDrive_rL);
        drive_rR_pub.publish(msgDrive_rR);
    }
    else
    {
        // z=0, go straight
        msgDrive.data = x;
        msgSteering.data = z;
        steer_fL_pub.publish(msgSteering);
        steer_fR_pub.publish(msgSteering);
        steer_rL_pub.publish(msgSteering);
        steer_rR_pub.publish(msgSteering);

        drive_fL_pub.publish(msgDrive);
        drive_fR_pub.publish(msgDrive);
        drive_rL_pub.publish(msgDrive);
        drive_rR_pub.publish(msgDrive);
    }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "cmdvel_pub");

    ros::NodeHandle nh;

    CmdVelPublisher cmdvel_pub = CmdVelPublisher(&nh);

    ros::spin();
}