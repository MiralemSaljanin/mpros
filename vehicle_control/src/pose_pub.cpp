/**
 * @file pose_pub.cpp
 * 
 * @brief publish the position and velocity of vehiclemodel
 * 
 * subscribe topic gazebo_msgs/ModelStates and convert to three new
 * topic: /vehicle/center_pose; /vehicle/rear_pose; /vehicle/velocity.
 *          
 * @author Yang Qiu st163984@stud.uni-stuttgart.de
 * 
 * @date 2021-01-10
 */

#include<pose_pub.h>

PosePublisher::PosePublisher(ros::NodeHandle *nh)
{
    modelStates_sub = nh->subscribe("gazebo/model_states", 10, &PosePublisher::modelStatesCallback, this);

    timer_pose_pub = nh->createTimer(ros::Duration(1.0 / 100.0), &PosePublisher::timerCallback, this);

    rear_pose_pub = nh->advertise<geometry_msgs::PoseStamped>("/vehicle/rear_pose", 10);
    center_pose_pub = nh->advertise<geometry_msgs::PoseStamped>("/vehicle/center_pose", 10);
    vel_pub = nh->advertise<geometry_msgs::Twist>("/vehicle/velocity", 10);
}

PosePublisher::~PosePublisher(){};

void PosePublisher::modelStatesCallback(const gazebo_msgs::ModelStates &vehicle_curr_pose)
{
    // find /vehiclemodel index in gazebo ModelStates topic
    // two names in ModelStates: /ground_plane and /vehiclemodel
    int vehicle_model_index = -1;
    std::vector<std::string> model_names = vehicle_curr_pose.name;
    for (size_t i = 0; i < model_names.size(); i++)
    {
        if (model_names[i] == "vehiclemodel")
            vehicle_model_index = i;
    }

    // get the vehicle status from vehicle_curr_pose
    vehicle_position = vehicle_curr_pose.pose[vehicle_model_index];
    vehicle_velocity = vehicle_curr_pose.twist[vehicle_model_index];
    vehicle_orientation = vehicle_position.orientation;

    // convert quaternion to euler, get roll pitch and yaw
    // in this case just yaw will be used
    tf::Quaternion q(
        vehicle_orientation.x,
        vehicle_orientation.y,
        vehicle_orientation.z,
        vehicle_orientation.w);
    tf::Matrix3x3 matrix(q);
    double roll, pitch, yaw;
    matrix.getRPY(roll, pitch, yaw);

    // publish vehicle center position, used in LQR
    center_pose.header.frame_id = "/map";
    center_pose.header.stamp = ros::Time::now();
    center_pose.pose.position = vehicle_position.position;
    center_pose.pose.orientation = vehicle_position.orientation;
    // center_pose_pub.publish(center_pose);

    // publish vehicle rear axle position, used in Pure Pursuit
    rear_pose.header.frame_id = "/map";
    rear_pose.header.stamp = ros::Time::now();
    double center_x = vehicle_position.position.x;
    double center_y = vehicle_position.position.y;
    double lr = 1.4; // distance between rear axle and vehicle center
    double rear_x = center_x - cos(yaw) * lr;
    double rear_y = center_x - sin(yaw) * lr;
    rear_pose.pose.position.x = rear_x;
    rear_pose.pose.position.y = rear_y;
    rear_pose.pose.orientation = vehicle_position.orientation;
    // rear_pose_pub.publish(rear_pose);

    // publish vehicle velocity
    veolcity.linear = vehicle_velocity.linear;
    veolcity.angular = vehicle_velocity.angular;
    // vel_pub.publish(veolcity);
}

void PosePublisher::timerCallback(const ros::TimerEvent &event)
{
    center_pose_pub.publish(center_pose);
    rear_pose_pub.publish(rear_pose);
    vel_pub.publish(veolcity);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "pose_pub");

    ros::NodeHandle nh;

    PosePublisher posepub = PosePublisher(&nh);

    ros::spin();

    return 0;
}