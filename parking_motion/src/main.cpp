#include <vector>

#include "ros/ros.h"
#include "FMTstar.h"
#include "RRTSpline.h"
#include "visualization_msgs/Marker.h"
#include "nav_msgs/OccupancyGrid.h"
#include "nav_msgs/Path.h"

#include "ros/console.h"

void PubMap(const NavMap &map, ros::Publisher &pub)
{
    nav_msgs::OccupancyGrid grid_map;
    grid_map.header.stamp = ros::Time::now();
    grid_map.header.frame_id = "map";
    grid_map.info.origin.position.x = map.map_origin_x_;
    grid_map.info.origin.position.y = map.map_origin_y_;
    grid_map.info.origin.position.z = 0.;
    grid_map.info.origin.orientation.w = 1.0;

    grid_map.info.height = map.map_height_;
    grid_map.info.width = map.map_width_;
    grid_map.info.resolution = map.map_resolution_;
    grid_map.info.map_load_time = ros::Time::now();

    for (int i = 0; i < map.map_.size(); ++i)
    {
        for (int j = 0; j < map.map_[0].size(); ++j)
        {
            grid_map.data.push_back(map.map_[i][j] * 100);
        }
    }

    pub.publish(grid_map);
}

void VisWaypoints(const ros::Publisher &pub,
                  const std::vector<std::vector<double>> &waypoints)
{
    ros::Time now = ros::Time::now();
    visualization_msgs::Marker points;
    points.header.frame_id = "map";
    points.header.stamp = now;
    points.action = visualization_msgs::Marker::ADD;
    points.pose.orientation.w = 1.0;
    points.id = 0;
    points.type = visualization_msgs::Marker::POINTS;
    points.scale.x = 0.5;
    points.scale.y = 0.5;
    points.color.b = 1.0f;
    points.color.a = 1.0;

    for (int i = 0; i < waypoints.size(); ++i)
    {
        geometry_msgs::Point pt;
        pt.x = waypoints[i][0];
        pt.y = waypoints[i][1];
        points.points.push_back(pt);
    }

    pub.publish(points);
}

void PubPath(const std::vector<std::vector<double>> &path_vec, ros::Publisher &pub)
{
    nav_msgs::Path path_msg;
    path_msg.header.stamp = ros::Time::now();
    path_msg.header.frame_id = "map";

    geometry_msgs::PoseStamped ps;

    for (int i = 0; i < path_vec.size(); ++i)
    {
        ps.header.frame_id = "map";
        ps.pose.position.x = path_vec[i][0];
        ps.pose.position.y = path_vec[i][1];
        ps.pose.orientation.w = 1.0;
        path_msg.poses.push_back(ps);
    }
    pub.publish(path_msg);
    ROS_INFO("Path published");
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "planning_node");

    // control ros console, change ros::console::levels::Info to ros::console::levels::Debug to show all ROS_DEBUG string
    if (ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Info))
    {
        ros::console::notifyLoggerLevelsChanged();
    }

    ros::NodeHandle nh;
    ros::Publisher waypoints_pub = nh.advertise<visualization_msgs::Marker>("waypoints", 1);
    ros::Publisher map_pub = nh.advertise<nav_msgs::OccupancyGrid>("loaded_map", 1);
    ros::Publisher path_pub = nh.advertise<nav_msgs::Path>("path", 1, true);

    std::vector<std::vector<double>> waypoints = {{8.0, 42.8}, // global goal point
                                                  {8.0, 31.0},
                                                  {8.0, 21.4},
                                                  {7.1, 19.0},
                                                  {4.1, 18.0},
                                                  {2.0, 13.0},
                                                  {2.0, 8.0},
                                                  {3.0, 3.0},
                                                  {6.0, 2.0},
                                                  {9.5, 3.0},
                                                  {10.5, 11.0},
                                                  {10.5, 26.0},
                                                  {10.5, 42.8}}; // global start point

    NavMap map(nh);
    RRTSpline *traj_gen = new RRTSpline(&nh);
    ros::Rate rate(1.0);

    while (nh.ok())
    {
        ros::spinOnce();
        VisWaypoints(waypoints_pub, waypoints);
        if (map.get_map_)
        {
            double r_search = 200.0;
            FMT *fmt_planner = new FMT(nh, waypoints, r_search);
            fmt_planner->LoadMap(map);
            PubMap(fmt_planner->GetMap(), map_pub);
            if (fmt_planner->PlanningSquence())
            {
                PubPath(fmt_planner->GetPath(), path_pub);
            }
            else
            {
                PubPath(waypoints, path_pub);
            }
            map.get_map_ = false;
        }
        rate.sleep();
    }

    return 0;
}