#include "nav_map.h"
#include <vector>
#include <iostream>

NavMap::NavMap()
{
}

NavMap::NavMap(ros::NodeHandle &nh)
{
    map_sub = nh.subscribe("map", 1, &NavMap::GetOccupancyMap, this);
}

NavMap::~NavMap()
{
}

void NavMap::GetOccupancyMap(const nav_msgs::OccupancyGrid::ConstPtr &map_msg)
{
    ROS_INFO("---Loading Map---");
    map_height_ = map_msg->info.height;
    map_width_ = map_msg->info.width;
    map_resolution_ = map_msg->info.resolution;
    map_origin_x_ = map_msg->info.origin.position.x;
    map_origin_y_ = map_msg->info.origin.position.y;

    x_max_ = map_origin_x_ + map_resolution_ * map_width_;
    x_min_ = map_origin_x_;
    y_max_ = map_origin_y_ + map_resolution_ * map_height_;
    y_min_ = map_origin_y_;

    int obstacle_inflation_pixel_num = int(obstacle_inflation_radius_ / map_resolution_);

    std::vector<uint8_t> map_empty_row(map_width_, 0); 
    std::vector<uint8_t> map_inf_empty_row(map_width_ + 2 * obstacle_inflation_pixel_num, 0);

    map_.resize(map_height_, map_empty_row);
    map_inflation_.resize(map_height_ + 2 * obstacle_inflation_pixel_num, map_inf_empty_row);
    
    ROS_INFO("map of size (%d, %d)", map_height_, map_width_);
    // ROS_DEBUG("inflation map of size (%u, %u)", map_inflation_.size(),(map_inflation_.end()-1)->size());
    // ROS_DEBUG("inflation pixel num of %d", obstacle_inflation_pixel_num);

    int current_pos;
    for (int i = 0; i < map_height_; ++i)
    {
        for (int j = 0; j < map_width_; ++j)
        {
            current_pos = i * map_width_ + j;
            if (map_msg->data[current_pos] == 0)
            {
                map_[i][j] = 0; // unoccupied cell
            }
            else
            {
                map_[i][j] = 1; // occupied (100) or unknown cell (-1)
            }
        }
    }

    InflateObstacle(obstacle_inflation_pixel_num);
    ROS_DEBUG("map loaded");
    get_map_ = true;
}

void NavMap::InflateObstacle(const int &obstacle_inflation_pixel_num)
{
    ROS_DEBUG("Inflating");
    // change obstacle_inflation_radius to pixel number
    for (int i = 0; i < map_height_; ++i)
    {
        for (int j = 0; j < map_width_; ++j)
        {
            if (map_[i][j] == 1)
            {
                int inflation_i_min = i - obstacle_inflation_pixel_num + obstacle_inflation_pixel_num;
                int inflation_i_max = i + obstacle_inflation_pixel_num + obstacle_inflation_pixel_num;
                int inflation_j_min = j - obstacle_inflation_pixel_num + obstacle_inflation_pixel_num;
                int inflation_j_max = j + obstacle_inflation_pixel_num + obstacle_inflation_pixel_num;
                // ROS_DEBUG("Doing inflation at %d %d", i, j);
                // ROS_DEBUG("Boundaries are %d ~ %d, %d ~ %d", inflation_i_min, inflation_i_max, inflation_j_min, inflation_j_max);
                for (int inflation_i = inflation_i_min;
                     inflation_i < inflation_i_max + 1;
                     ++inflation_i)
                {
                    for (int inflation_j = inflation_j_min;
                         inflation_j < inflation_j_max + 1;
                         ++inflation_j)
                    {
                        map_inflation_[inflation_i][inflation_j] = 1;
                    }
                }
            }
        }
    }

    ROS_DEBUG("Inflated");

    for (int i = 0; i < map_height_; ++i)
    {
        for (int j = 0; j < map_width_; ++j)
        {
            map_[i][j] = map_inflation_[i + obstacle_inflation_pixel_num][j + obstacle_inflation_pixel_num];
        }
    }
}