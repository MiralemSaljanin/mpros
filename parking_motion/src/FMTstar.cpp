#include "ros/ros.h"
#include "FMTstar.h"
#include <cmath>
#include <random>
#include <limits>
#include <cmath>
#include <iostream>
#include <algorithm>

FMT::FMT(ros::NodeHandle &nh, std::vector<std::vector<double>> &waypoints, const double &r_search)
{
    nh_ = &nh;
    fmt_path_pub_ = nh.advertise<nav_msgs::Path>("global_path", 10);
    num_sample_ = 100;
    r_near_ = r_search * sqrt(log(num_sample_) / num_sample_);
    ROS_INFO("r_near %f", r_near_);

    for (auto &point : waypoints)
    {
        goal_sequence_.push_back(new Node(point[0], point[1], 0.0, nullptr));
    }
}

FMT::~FMT()
{
}

void FMT::LoadMap(const NavMap &map)
{
    nav_map_ = map;

    grid_map_ = map.map_;
    map_resolution_ = map.map_resolution_;
    map_origin_x_ = map.map_origin_x_;
    map_origin_y_ = map.map_origin_y_;

    x_min_ = map.x_min_;
    x_max_ = map.x_max_;
    y_min_ = map.y_min_;
    y_max_ = map.y_max_;
    ROS_DEBUG("map range x_min %f x_max %f y_min %f y_max %f", x_min_, x_max_, y_min_, y_max_);
    ROS_DEBUG("map loaded by planning module");
}

void FMT::SampleFree()
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> distrib_x(x_min_, x_max_);
    std::uniform_real_distribution<double> distrib_y(y_min_, y_max_);

    for (int i = 0; i < num_sample_; ++i)
    {

        Node *new_node = new Node(distrib_x(gen),
                                  distrib_y(gen),
                                  .0,
                                  nullptr);
        if (!PointInCollision(new_node->x_, new_node->y_))
        {
            v_unvisited_.push_back(new_node);
            ROS_DEBUG("a node push_backed");
        }
        else
        {
            --i;
            delete new_node;
        }
        ROS_DEBUG("%d samples done", i);
    }

    ROS_DEBUG("sample ends");
}

double FMT::CalDist(const Node *node1, const Node *node2)
{
    return hypot(node1->x_ - node2->x_, node1->y_ - node2->y_);
}

SET FMT::GetNearNodes(SET nodelist,
                      Node *z,
                      const double &r_near)
{
    SET near_nodes;
    for (Node *node : nodelist)
    {
        if (CalDist(node, z) <= r_near_)
        {
            near_nodes.push_back(node);
        }
    }
    return near_nodes;
}

void FMT::SetPath(Node *local_goal)
{
    const Node *current_node = local_goal;
    // if the current node is not local start node
    while (current_node->parent_ != nullptr)
    {
        // append current node to end of the path list
        std::vector<double> point{current_node->x_, current_node->y_};
        path_.push_back(point);
        current_node = current_node->parent_;
    }
    // the last stage is over, current node is start node of the path
    if (goal_sequence_.size() < 2)
    {
        std::vector<double> point{current_node->x_, current_node->y_};
        path_.push_back(point);
        // path ordered with right order from first start to last goal
        std::reverse(path_.begin(), path_.end());
    }
}

std::vector<std::vector<double>> FMT::GetPath()
{
    return path_;
}

bool FMT::PointInCollision(const double &x, const double &y)
{
    if (x < x_min_ || x > x_max_ ||
        y < y_min_ || y > y_max_)
    {
        return true;
    }
    int i = int((y - y_min_) / map_resolution_);
    int j = int((x - x_min_) / map_resolution_);
    return grid_map_[i][j];
}

bool FMT::CollisionFree(const double &x1, const double &y1, const double &x2, const double &y2)
{
    int num_edge_pixel = int(r_near_ / map_resolution_);

    double dx = (x2 - x1) / num_edge_pixel;
    double dy = (y2 - y1) / num_edge_pixel;

    double current_x = x1;
    double current_y = y1;

    for (int i = 0; i <= num_edge_pixel; i++)
    {
        current_x += dx;
        current_y += dy;

        if (PointInCollision(current_x, current_y))
        {
            return false;
        }
    }
    return true;
}

double FMT::Interpolate(const double &x1, const double &x2, const double &step_size)
{
    double dist = x2 - x1;
    if (abs(dist) <= step_size)
    {
        return x2;
    }
    else if (dist < 0)
    {
        return x1 - step_size;
    }
    else
    {
        return x1 + step_size;
    }
}

bool FMT::Planning(Node *local_start, Node *local_goal)
{
    ROS_DEBUG("Planning begins");
    // push goal node to unvisited set
    v_unvisited_.push_back(local_goal);
    // push start node to open set
    v_open_.push_back(local_start);
    ROS_DEBUG("open set size: %lu", v_open_.size());
    // z as node with lowest cost in open set
    Node *z = local_start;

    while (z != local_goal)
    {
        // new set of nodes gonna be added to open set
        SET v_open_new;

        // set storing unvisited nodes and near z
        SET z_near = GetNearNodes(v_unvisited_, z, r_near_);

        for (auto x : z_near) // for all nodes in this set
        {
            // get open nodes near this unvisited node
            SET x_near = GetNearNodes(v_open_, x, r_near_);
            // sort them according to cost, the one with lowest cost at front
            std::sort(x_near.begin(), x_near.end(), NodeCostCmp);
            // get the open node closest to x
            Node *begin_node = *(x_near.begin());
            // if the path between x and begin node is collsion-free
            if (CollisionFree(x->x_, x->y_, begin_node->x_, begin_node->y_))
            {
                // connect x to begin node
                x->parent_ = begin_node;
                // append x to open set and remove it from unvisited set
                v_open_new.push_back(x);
                v_unvisited_.erase(std::find(v_unvisited_.begin(), v_unvisited_.end(), x));
                // update x node cost
                x->cost_ = begin_node->cost_ + CalDist(x, begin_node);
            }
        }
        // remove z node from open set
        v_open_.erase(std::find(v_open_.begin(), v_open_.end(), z));
        // concatenate new set to open set
        v_open_.insert(v_open_.end(), v_open_new.begin(), v_open_new.end());
        // put z node into close set
        v_close_.push_back(z);

        // if open set is empty, the map can't be explored anymore, the goal is not reached
        if (v_open_.empty())
        {
            ROS_ERROR("open set empty!");
            return false;
        }

        // sort open set
        std::sort(v_open_.begin(), v_open_.end(), NodeCostCmp);
        // take front node in open set as z
        z = v_open_.front();
    }

    return true;
}

bool FMT::PlanningSquence()
{
    SampleFree();
    int count = 0;
    while (goal_sequence_.size() > 1)
    {
        Node *goal = goal_sequence_.back();
        goal_sequence_.pop_back();
        Node *start = goal_sequence_.back();
        if (Planning(start, goal))
        {
            SetPath(goal);
            v_open_.erase(std::find(v_open_.begin(), v_open_.end(), goal));
            v_close_.erase(std::find(v_close_.begin(), v_close_.end(), start));
            v_unvisited_.insert(v_unvisited_.end(), v_open_.begin(), v_open_.end());
            v_unvisited_.insert(v_unvisited_.end(), v_close_.begin(), v_close_.end());
            ROS_DEBUG("Length of v_unvisited is %lu", v_unvisited_.size());
            v_open_.clear();
            v_close_.clear();
        }
        else
        {
            ROS_ERROR("Planning at stage %d failed", count);
            return false;
        }
        ++count;
    }
    ROS_INFO("Planning done");
    return true;
}

NavMap FMT::GetMap()
{
    return nav_map_;
}

std::vector<std::vector<double>> FMT::GetTree()
{
    return tree_;
}