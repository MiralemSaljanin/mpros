#include "RRTStar_new.h"

#include <utility>
#include <random>
#include <limits>

#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Point.h"

#include "ros/console.h"

namespace NEW
{

    RRTStarNew::RRTStarNew(ros::NodeHandle *nh)
    {
        map_sub_ = nh->subscribe("map", 1, &RRTStarNew::GetOccupancyMap, this);
        path_pub_ = nh->advertise<nav_msgs::Path>("rrt_star/path", 10, true);
        tree_pub_ = nh->advertise<visualization_msgs::Marker>("rrt_star/tree", 10, false);

        // if (ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug))
        // {
        //     ros::console::notifyLoggerLevelsChanged();
        // }

        msg_tree_vertex_.header.frame_id = "map";
        msg_tree_vertex_.header.stamp = ros::Time::now();
        msg_tree_vertex_.action = visualization_msgs::Marker::ADD;
        msg_tree_vertex_.pose.orientation.w = 1.0;
        msg_tree_vertex_.id = 0;
        msg_tree_vertex_.type = visualization_msgs::Marker::POINTS;

        msg_tree_vertex_.scale.x = 1.0;
        msg_tree_vertex_.scale.y = 1.0;

        msg_tree_vertex_.color.b = 1.0f;
        msg_tree_vertex_.color.a = 1.0;
    }

    RRTStarNew::~RRTStarNew()
    {
    }

    void RRTStarNew::GetOccupancyMap(const nav_msgs::OccupancyGrid::ConstPtr &map_msg)
    {
        ROS_INFO_STREAM("------------------Map Params-------------------------------");
        ROS_INFO_STREAM("Successfully got the map");

        // ----------------------------------Set Start and Goal-----------------------------------------
        // set the start and goal points
        start_point_ = {0.0, 0.0};
        start_node_ = Node(start_point_, .0, nullptr, 0);

        // Map
        // Node goal_node(120.0, 0.0, 0.0, 0);

        // final simulation
        goal_point_ = {359.0, 217.0};
        goal_node_ = Node(goal_point_, std::numeric_limits<double>::max(), nullptr, std::numeric_limits<int>::max());

        ROS_INFO_STREAM("The (goal_x, goal_y) is (" << goal_node_.point_.x << ","
                                                    << goal_node_.point_.y << ")");

        // ------------------------------------Map Information-----------------------------------------
        map_size_ij_ = {(int)map_msg->info.height,
                        (int)map_msg->info.width};
        map_resolution_ = map_msg->info.resolution;
        map_size_xy_ = {map_msg->info.width * map_resolution_,
                        map_msg->info.height * map_resolution_};
        map_origin_ = {map_msg->info.origin.position.x,
                       map_msg->info.origin.position.y};

        // print the information of the map
        ROS_INFO_STREAM("The height, width of the map is (" << map_size_ij_.i << ","
                                                            << map_size_ij_.j << ")");
        ROS_INFO_STREAM("The resolution of this map is " << map_resolution_);
        ROS_INFO_STREAM("The (map_origin_x, map_origin_y) is ("
                        << map_origin_.x << "," << map_origin_.y << ") in /world frame");

        // --------------------------------------Get Grid Map------------------------------------------
        // resize the vector
        grid_map_.resize(map_size_ij_.i);
        grid_inflation_.resize(map_size_ij_.i);
        for (int i = 0; i < map_size_ij_.i; i++)
        {
            grid_map_[i].resize(map_size_ij_.j);
            grid_inflation_[i].resize(map_size_ij_.j);
        }
        ROS_INFO_STREAM(
            "grid_map_ resized to height of " << grid_map_.size()
                                              << " and width of "
                                              << grid_map_[0].size());

        // storage the map in 2D vector
        for (int i = 0; i < map_size_ij_.i; i++)
        {
            for (int j = 0; j < map_size_ij_.j; j++)
            {
                // int curr_cell_position = i + map_size_ij_.j * j;
                int curr_cell_position = i * map_size_ij_.j + j;

                if (map_msg->data[curr_cell_position] == 0)
                {
                    grid_map_[i][j] = 0; // unoccupied cell
                }
                else
                {
                    grid_map_[i][j] = 1; // occupied (100) or unknown cell (-1)
                }
            }
        }
        // // ROS_DEBUG_STREAM("Map stored!");

        InflateObstacle();

        // TODO： Bring Run function outside callback to be compatible with map updates
        RunRRTStar();
    }

    void RRTStarNew::InflateObstacle()
    {
        // change obstacle_inflation_radius to pixel number
        int obstacle_inflation_pixel_num = int(obstacle_inflation_radius_ / map_resolution_);
        for (int i = 0; i < map_size_ij_.i; i++)
        {
            for (int j = 0; j < map_size_ij_.j; j++)
            {
                if (grid_map_[i][j] == 1)
                {
                    int inflation_i_min = i - obstacle_inflation_pixel_num;
                    int inflation_i_max = i + obstacle_inflation_pixel_num;
                    int inflation_j_min = j - obstacle_inflation_pixel_num;
                    int inflation_j_max = j + obstacle_inflation_pixel_num;
                    for (int inflation_i = inflation_i_min;
                         inflation_i < inflation_i_max + 1;
                         inflation_i++)
                    {
                        for (int inflation_j = inflation_j_min;
                             inflation_j < inflation_j_max + 1;
                             inflation_j++)
                        {
                            grid_inflation_[inflation_i][inflation_j] = 1;
                        }
                    }
                }
            }
        }

        for (int i = 0; i < map_size_ij_.i; i++)
        {
            for (int j = 0; j < map_size_ij_.j; j++)
            {
                grid_map_[i][j] = grid_inflation_[i][j];
            }
        }
        // // ROS_DEBUG_STREAM("Map inflation done!");
    }

    bool RRTStarNew::RunRRTStar()
    {
        if (IsPointInCollision(start_node_.point_))
        {
            ROS_INFO("The Start point is occupied, please change it");
            return false;
        }
        if (IsPointInCollision(goal_node_.point_))
        {
            ROS_INFO("The Goal point is occupied, please change it");
            return false;
        }

        ROS_INFO_STREAM("--------RRT Star--------");

        start_ = clock();

        tree_.push_back(&start_node_);
        PubVertex(start_node_);
        // // // ROS_DEBUG_STREAM("Start node is pushed.");

        int iter_count = 0;
        while (iter_count < max_iter_)
        {
            iter_count++;
            if (iter_count % 10000 == 0)
            {
                ROS_INFO_STREAM("Done " << iter_count << " iterations.");
            }

            Point sample_point = SampleRandomPoint();
            // Get index of nearest node in the tree
            int nearest_idx = NearestNodeIdx(sample_point);
            // Create new node with step_size_ and assign nearest as parent
            Node *new_node = new Node();
            *new_node = SteerNewNode(*tree_[nearest_idx], sample_point);

            if (CollisionFree(*tree_[nearest_idx], *new_node))
            {
                // TODO: Expand does not need nearest_idx param
                ExpandTree(*new_node, nearest_idx);
                // // ROS_DEBUG_STREAM("Expand new node done");

                // Do one expand further
                if (IsNearGoal(*new_node) && !path_improving)
                {
                    int GoalNearestIdx = NearestNodeIdx(goal_point_);
                    goal_node_.parent_ = tree_[GoalNearestIdx];
                    // // // ROS_DEBUG_STREAM("Found a node near goal");
                    ExpandTree(goal_node_, GoalNearestIdx);
                    // // ROS_DEBUG_STREAM("Expand goal node done");
                    goal_found_ = CollisionFree(goal_node_, *(goal_node_.parent_));
                }
            }

            if (goal_found_ && !path_improving)
            {
                ROS_INFO_STREAM("-------Path found!-------");
                GetPath(goal_node_);
                PubPath();
                ROS_INFO_STREAM("The global path has been found at the " << iter_count << " iteration");
                ROS_INFO_STREAM("The nodes number in the Tree: " << tree_.size());
                ROS_INFO_STREAM("The nodes number in the Path: " << path_msg_.poses.size());
                ROS_INFO_STREAM("The cost of goal node is: " << goal_node_.cost_);
                middle_ = clock();
                ROS_INFO_STREAM("Time to find a path: " << double(middle_ - start_) / CLOCKS_PER_SEC << "s");

                goal_found_iter_ = iter_count;
                path_improving = true;
#if PATH_IMPROVE == 0
                return true;
#endif
            }
            else if (path_improving && (iter_count - goal_found_iter_) % 10000 == 0) // update every 1000 iter
            {
                ROS_INFO_STREAM("Path updated after 10000 iters.");
                ROS_INFO_STREAM("The nodes number in the Tree: " << tree_.size());
                ROS_INFO_STREAM("The nodes number in the Path: " << path_msg_.poses.size());
                ROS_INFO_STREAM("The cost of goal node is: " << goal_node_.cost_);
                GetPath(goal_node_);
                PubPath();
                end_ = clock();
                ROS_INFO_STREAM("Time for updating a path: " << double(end_ - middle_) / CLOCKS_PER_SEC << "s");
                middle_ = end_;
            }
        }
        if (!goal_found_)
        {
            ROS_INFO_STREAM("Fail to find a path!");
            ROS_INFO_STREAM("Expanded " << tree_.size() << " Nodes");
            return false;
        }
        else
        {
            ROS_INFO_STREAM("Planning ends after " << max_iter_ << " iterations");
            ROS_INFO_STREAM("Expanded " << tree_.size() << " Nodes");
            return true;
        }
    }

    bool RRTStarNew::IsPointInCollision(Point point)
    {
        Grid grid = PointToGrid(point);
        return grid_map_[grid.i][grid.j];
    }

    Point RRTStarNew::SampleRandomPoint()
    {
        std::random_device rd;
        std::mt19937 gen(rd());

        std::uniform_int_distribution<int> sample_rate_distri(1, 100);
        std::uniform_int_distribution<int> i_random_distri(0, map_size_ij_.i - 1);
        std::uniform_int_distribution<int> j_random_distri(0, map_size_ij_.j - 1);

        int sample_rate = sample_rate_distri(gen);
        Grid sample_grid = {i_random_distri(gen),
                            j_random_distri(gen)};

        // // ROS_DEBUG_STREAM("Sample done");

        if (sample_rate <= goal_bias_rate_ && !path_improving)
        {
            return goal_point_;
        }
        else
        {
            // // // ROS_DEBUG_STREAM("sample point at i=" << sample_grid.i << "j=" << sample_grid.j);
            Point p = GridToPoint(sample_grid);
            // // // ROS_DEBUG_STREAM("sample point at x=" << p.x << "y=" << p.y);
            return p;
        }
    }

    int RRTStarNew::NearestNodeIdx(const Point &sample_point)
    {
        int nearest_idx;
        double lowest_metric = std::numeric_limits<double>::max();
        for (int i = 0; i < tree_.size(); i++)
        {
#if MANHATTAN_METRIC == 0
            double metric = pow(tree_[i]->point_.x - sample_point.x, 2) +
                            pow(tree_[i]->point_.y - sample_point.y, 2);
#else
            double metric = PointManhattanDist(tree_[i]->point_, sample_point);
#endif
            // // // ROS_DEBUG_STREAM("Metric with No. " << iter - tree_.begin() << " is : " << metric);
            // // // ROS_DEBUG_STREAM("No. " << iter - tree_.begin() << " is at ("
            //                        << iter->point_.x << " , " << iter->point_.y << ")");
            if (metric < lowest_metric)
            {
                nearest_idx = i;
                lowest_metric = metric;
            }
        }
        // // ROS_DEBUG_STREAM("NearestNodeIdx done");
        return nearest_idx;
    }

    Node RRTStarNew::SteerNewNode(Node &nearest_node, const Point &sample_point)
    {
        // TODO: avoid calculating distance again
        double dx = sample_point.x - nearest_node.point_.x;
        double dy = sample_point.y - nearest_node.point_.y;
        double dist = sqrt(pow(dx, 2) +
                           pow(dy, 2));
        //double theta = atan2(nearest_node.point_.y - sample_point.y,
        //                     nearest_node.point_.x - sample_point.x);

        if (dist < step_size_)
        {
            return Node(sample_point,
                        nearest_node.cost_ + dist,
                        &nearest_node, -1);
        }
        else
        {
            Point point = {nearest_node.point_.x + dx * step_size_ / dist,
                           nearest_node.point_.y + dy * step_size_ / dist};
            return Node(point,
                        nearest_node.cost_ + step_size_,
                        &nearest_node, -1);
        }
    }

    bool RRTStarNew::CollisionFree(const Node &nearest_node, const Node &new_node)
    {
        // // ROS_DEBUG_STREAM("CollisionFree begins");
        // TODO: try use actual distance between two nodes
        int num_edge_pixel = int(search_radius_ / map_resolution_);

        double dx = (new_node.point_.x - nearest_node.point_.x) / num_edge_pixel;
        double dy = (new_node.point_.y - nearest_node.point_.y) / num_edge_pixel;

        double current_x = nearest_node.point_.x;
        double current_y = nearest_node.point_.y;

        for (int i = 0; i <= num_edge_pixel; i++)
        {
            current_x += dx;
            current_y += dy;

            if (PointToMapValue({current_x, current_y}))
            {
                // // ROS_DEBUG_STREAM("CollisionFree Done");
                return false;
            }
        }
        // // ROS_DEBUG_STREAM("CollisionFree Done");
        return true;
    }

    void RRTStarNew::ExpandTree(Node &new_node, const int &nearest_idx)
    {
        // // ROS_DEBUG_STREAM("Expand begins");
        std::vector<int> near_nodes_idx = FindNearNodes(new_node);
        ChooseParentNode(near_nodes_idx, nearest_idx, new_node);
        if (new_node.parent_ == nullptr)
        {
            ROS_WARN_STREAM("Adding a node with null parent");
        }
        new_node.idx_ = tree_.size();
        if (new_node != goal_node_ || !goal_found_)
        {
            tree_.push_back(&new_node);
        }
        // ROS_INFO_STREAM("Push node with idx:" << new_node.idx_ << " in tree");
        // ROS_DEBUG_STREAM("----------Tree----------");
        // publish all tree
        // for (int i = 1;
        //      i < tree_.size();
        //      i++)
        // {
        //     ROS_DEBUG_STREAM("Node No." << tree_[i]->idx_
        //                                 << " with x = " << tree_[i]->point_.x
        //                                 << " y = " << tree_[i]->point_.y
        //                                 << " cost = "<<tree_[i]->cost_
        //                                 << " parent idx = " << tree_[i]->parent_->idx_);
        // }

        PubVertex(new_node);
        Rewire(near_nodes_idx, new_node);
        // // ROS_DEBUG_STREAM("Expand done");
    }

    bool RRTStarNew::IsNearGoal(Node &new_node)
    {
        // // ROS_DEBUG_STREAM("Is Near Goal Begin");
        return NodeDist(goal_node_, new_node) <= step_size_;
    }

    std::vector<int> RRTStarNew::FindNearNodes(const Node &new_node)
    {
        std::vector<int> near_nodes;

        for (int i = 0; i < tree_.size(); i++)
        {
            // TODO: modify search radius or number of neighbours
            if (NodeDist(*(tree_[i]), new_node) <= search_radius_)
            {
                near_nodes.push_back(i);
            }
        }
        // // ROS_DEBUG_STREAM("FindNearNodes done");
        return near_nodes;
    }

    void RRTStarNew::ChooseParentNode(const std::vector<int> &near_nodes_idx,
                                      const int &nearest_idx,
                                      Node &new_node)
    {
        if (near_nodes_idx.empty())
        {
            ROS_WARN_STREAM("Found a node with no neightbour");
            return;
        }

        double min_cost = new_node.cost_;
        for (auto iter = near_nodes_idx.begin();
             iter != near_nodes_idx.end();
             iter++)
        {
            if (tree_[*iter]->cost_ + NodeDist(*(tree_[*iter]), new_node) < min_cost &&
                CollisionFree(*(tree_[*iter]), new_node))
            {
                new_node.parent_ = tree_[*iter];
                min_cost = new_node.parent_->cost_ + NodeDist(*(new_node.parent_), new_node);
            }
        }

        new_node.cost_ = min_cost;
        // // ROS_DEBUG_STREAM("ChooseParentNode done");
    }

    void RRTStarNew::Rewire(const std::vector<int> &near_nodes_idx, Node &new_node)
    {
        for (auto iter = near_nodes_idx.begin();
             iter != near_nodes_idx.end();
             iter++)
        {
            double new_cost = new_node.cost_ + NodeDist(new_node, *(tree_[*iter]));
            if (new_cost < tree_[*iter]->cost_ &&
                CollisionFree(new_node, *(tree_[*iter])))
            {
                tree_[*iter]->parent_ = &new_node;
                tree_[*iter]->cost_ = new_cost;
            }
        }
        // // ROS_DEBUG_STREAM("Rewire done");
    }

    void RRTStarNew::GetPath(Node &final_node)
    {
        ROS_DEBUG_STREAM("GetPath begins");
        // for (int i = tree_.size() - 1;
        //      i > 0;
        //      i--)
        // {
        // //     ROS_DEBUG_STREAM("Node No." << tree_[i].idx_ << " with parent idx=" << tree_[i].parent_->idx_
        //                                 << " parent_x = " << tree_[i].parent_->point_.x
        //                                 << " parent_y = " << tree_[i].parent_->point_.y);
        // }
        path_msg_.poses.clear();

        if (path_improving)
            ROS_DEBUG_STREAM("Old path cleared");

        geometry_msgs::PoseStamped pose;
        Node *current_node = &final_node;
        pose.pose.position.x = current_node->point_.x;
        pose.pose.position.y = current_node->point_.y;
        path_msg_.poses.push_back(pose);

        if (path_improving)
            ROS_DEBUG_STREAM("A node is pushed to path");

        while (current_node->idx_ != 0)
        {
            // // ROS_DEBUG_STREAM("Trying to add parent node");
            current_node = current_node->parent_;

            if (path_improving)
                ROS_DEBUG_STREAM("Get parent node as new node with idx: " << current_node->idx_);

            pose.pose.position.x = current_node->point_.x;

            if (path_improving)
                ROS_DEBUG_STREAM("x=" << current_node->point_.x);

            pose.pose.position.y = current_node->point_.y;
            // // ROS_DEBUG_STREAM("A path point is assigned");
            path_msg_.poses.push_back(pose);

            if (path_improving)
                ROS_DEBUG_STREAM("A node is pushed to path");
        }

        pose.pose.position.x = start_node_.point_.x;
        pose.pose.position.y = start_node_.point_.y;
        path_msg_.poses.push_back(pose);

        if (path_improving)
            ROS_DEBUG_STREAM("GetPath done");
    }

    void RRTStarNew::PubPath()
    {
        path_msg_.header.frame_id = "world";
        path_msg_.header.stamp = ros::Time::now();

        path_pub_.publish(path_msg_);
        ROS_DEBUG_STREAM("PubPath done");
    }

    Point RRTStarNew::GridToPoint(const Grid grid)
    {
        Point point;
        point.x = map_origin_.x + grid.j * map_resolution_;
        point.y = map_origin_.y + grid.i * map_resolution_;
        return point;
    }

    Grid RRTStarNew::PointToGrid(const Point point)
    {
        Grid grid;
        grid.i = int((point.y - map_origin_.y) / map_resolution_);
        grid.j = int((point.x - map_origin_.x) / map_resolution_);

        return grid;
    }

    int RRTStarNew::PointToMapValue(const Point point)
    {
        Grid grid = PointToGrid(point);
        return grid_map_[grid.i][grid.j];
    }

    void RRTStarNew::PubVertex(Node &node)
    {
        geometry_msgs::Point p;
        p.x = node.point_.x;
        p.y = node.point_.y;
        p.z = 1.0;

        msg_tree_vertex_.points.push_back(p);
        tree_pub_.publish(msg_tree_vertex_);
    }

}

/*
grid map: every grid in (i,j)

(1,-1)  (1,0)   (1,1)
(0,-1)  (0,0)   (0,1)
(-1,-1) (-1,0)  (-1,1)

map: every point in (x,y)
(0, 2*resol)    (resol, 2*resol)    (2*resol, 2*resol)
(0, resol)      (resol, resol)      (2*resol, resol)
(0,0)           (resol, 0)          (2*resol, 0)
map_origin_.x = resol
map_origin_.y = resol


*/