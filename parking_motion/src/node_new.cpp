#include "node_new.h"

namespace NEW
{
    Node::Node() : cost_(.0), parent_(nullptr), point_({.0, .0}), idx_(0)
    {
    }

    Node::Node(const Point point, const double cost, Node *parent, const int idx)
    {
        point_ = point;
        cost_ = cost;
        parent_ = parent;
        idx_ = idx;
    }

    Node::~Node()
    {
    }

    double PointDist(const Point &lpoint,
                     const Point &rpoint)
    {
        return sqrt(pow(lpoint.x - rpoint.x, 2) +
                    pow(lpoint.y - rpoint.y, 2));
    }

    double PointManhattanDist(const Point &lpoint,
                              const Point &rpoint)
    {
        return abs(lpoint.x - rpoint.x) +
               abs(lpoint.y - rpoint.y);
    }

    bool operator==(const Point &lpoint,
                    const Point &rpoint)
    {
        return lpoint.x == rpoint.x && lpoint.y == rpoint.y;
    };

    bool operator!=(const Point &lpoint,
                    const Point &rpoint)
    {
        return lpoint.x != rpoint.x || lpoint.y != rpoint.y;
    };

    Point operator-(const Point &lpoint,
                    const Point &rpoint)
    {
        return {lpoint.x - rpoint.x, lpoint.y - rpoint.y};
    };

    bool operator==(const Grid &lgrid,
                    const Grid &rgrid)
    {
        return lgrid.i == rgrid.i && lgrid.j == rgrid.j;
    };

    bool operator==(const Node &lnode,
                    const Node &rnode)
    {
        return lnode.point_ == rnode.point_;
    };

    bool operator!=(const Node &lnode,
                    const Node &rnode)
    {
        return lnode.point_ != rnode.point_;
    };
#if MANHATTAN_METRIC == 0
    double NodeDist(const Node &node1,
                    const Node &node2)
    {
        return sqrt(pow(node1.point_.x - node2.point_.x, 2) +
                    pow(node1.point_.y - node2.point_.y, 2));
    }

#else

    double NodeDist(const Node &node1,
                             const Node &node2)
    {
        return abs(node1.point_.x - node2.point_.x) +
               abs(node1.point_.y - node2.point_.y);
    }
#endif // MANHATTAN_METRIC

}