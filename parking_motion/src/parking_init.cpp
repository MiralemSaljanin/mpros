#include "parking_init.h"
#include "nav_msgs/Path.h"
#include "geometry_msgs/Point.h"
#include "geometry_msgs/PoseStamped.h"
#include "visualization_msgs/Marker.h"

void ParkingInit::PubTopic(const ros::Publisher &pub,
                           const std::vector<std::vector<double>> &waypoints,
                           const std::string &topicName)
{
    ros::Time now = ros::Time::now();
    nav_msgs::Path path;
    path.header.frame_id = "map";
    path.header.stamp = now;

    visualization_msgs::Marker points;
    points.header.frame_id = "map";
    points.header.stamp = now;
    points.action = visualization_msgs::Marker::ADD;
    points.pose.orientation.w = 1.0;
    points.id = 0;
    points.type = visualization_msgs::Marker::POINTS;
    points.scale.x = 0.5;
    points.scale.y = 0.5;
    points.color.b = 1.0f;
    points.color.a = 1.0;

    for (int i = 0; i < waypoints.size(); ++i)
    {
        geometry_msgs::PoseStamped pose;
        pose.header.frame_id = "map";
        pose.header.stamp = now;
        pose.pose.position.x = waypoints[i][0];
        pose.pose.position.y = waypoints[i][1];
        pose.pose.orientation.w = 1.0;
        path.poses.push_back(pose);

        geometry_msgs::Point pt;
        pt.x = waypoints[i][0];
        pt.y = waypoints[i][1];
        points.points.push_back(pt);
    }

    if (topicName == "path")
    {
        pub.publish(path);
    }
    else if (topicName == "waypoints")
    {
        pub.publish(points);
    }
    else
    {
        return;
    }
}

ParkingInit::ParkingInit(ros::NodeHandle &nh)
{
    goalpoints_pub_ = nh.advertise<visualization_msgs::Marker>("waypoints", 1);
    path_pub_ = nh.advertise<nav_msgs::Path>("path", 1, false);

    // waypoints are aligned counter-clockwise from start point to start point to form a closed loop
    goalpoints_ = {{11.4, 23.8}, // start point
                   {7.6, 24.16},
                   {7.7, 18.1},
                   {7.7, 13.8},
                   {4.2, 13.7},
                   {1.0, 13.5},
                   {1.0, 6.5},
                   {0.9, 0.9},
                   {6.2, 0.7},
                   {11.3, 0.8},
                   {11.2, 10.1},
                   {11.4, 23.8}};

    PubTopic(goalpoints_pub_, goalpoints_, "waypoints");
    //PubTopic(path_pub_, goalpoints_, "path");
}