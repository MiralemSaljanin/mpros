#include "FMT_node.h"

Node::Node() : x_(.0), y_(.0), cost_(.0), parent_(nullptr)
{
}

Node::Node(const double& x, const double& y, const double& cost, Node* parent)
{
    x_ = x;
    y_ = y;
    cost_ = cost;
    parent_ = parent;
}

Node::~Node()
{
}

