# flexcar_roundtrip
This package is for flexcar roundtrip test in the parking lot behind ARENA. The smooth and feasible trajectory on topic `spline/path` with format `nav_msgs/path` and trajectory including velocity infomation on topic `global/bahn` with format `Bahn`

## How to use
---
Clone the repo and switch to this branch
```shell
cd ~/catkin_ws/src
git clone https://RunningChang@bitbucket.org/MiralemSaljanin/mpros.git
cd mpros
git checkout control_devel
```

Resolving dependencies
```shell
cd ~/catkin_ws
rosdep install --from-paths . --ignore-src --ros-distro noetic -r -y
```

Build the packages
```shell
cd ~/catkin_ws && catkin_make`
```
Run following commands in separate terminals
```shell
roslaunch vehicle_control vehicle_control_parking.launch
rosrun parking_motion parking_path
rosrun vehicle_einspurmodell main
```
<p align="center">
    <image src="./media/vehilcle follows path.png" width=200>
</p>

## TODO
---
- Adapt curvature bound to the path
- Make control capable of repeating
