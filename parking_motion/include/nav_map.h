#ifndef NAV_MAP_H
#define NAV_MAP_H

#include "ros/ros.h"
#include "nav_msgs/OccupancyGrid.h"
#include <vector>

class NavMap
{
public:
    NavMap();
    NavMap(ros::NodeHandle &nh);
    ~NavMap();

    void GetOccupancyMap(const nav_msgs::OccupancyGrid::ConstPtr &map_msg);
    void InflateObstacle(const int &obstacle_inflation_pixel_num);
    int XToCol(const double &x);
    int YToRow(const double &y);

    ros::Subscriber map_sub;

    std::vector<std::vector<uint8_t>> map_;
    std::vector<std::vector<uint8_t>> map_inflation_;

    int map_height_, map_width_;
    double map_resolution_;
    double map_origin_x_, map_origin_y_;
    double x_max_, x_min_, y_max_, y_min_;
    double obstacle_inflation_radius_ = 1.0;

    bool get_map_ = false;
};

#endif // !NAV_MAP_H