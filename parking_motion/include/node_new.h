#ifndef NODE_NEW_H
#define NODE_NEW_H

#define MANHATTAN_METRIC 0
#define PATH_IMPROVE 0

#include <cmath>

namespace NEW
{
    /**
 * @brief struct for 2D point, member x, y
 * 
 */
    struct Point
    {
        double x;
        double y;
    };

    /**
 * @brief struct for index of a grid, member: i, j
 * 
 */
    struct Grid
    {
        int i;
        int j;
    };

    /**
 * @brief Node for sample-based planning algorithm
 * @param point_ coordinate in 2D map
 * @param cost_ path cost from start point
 * @param parent_ points to parent node in Tree
 * 
 */

    class Node
    {
    public:
        Node();
        Node(const Point point, const double cost, Node *parent, const int idx);
        virtual ~Node();

        Point point_;
        double cost_;
        Node *parent_;
        int idx_;
    };

    bool operator==(const Point &lpoint,
                    const Point &rpoint);

    bool operator!=(const Point &lpoint,
                    const Point &rpoint);

    Point operator-(const Point &lpoint,
                    const Point &rpoint);

    bool operator==(const Grid &lgrid,
                    const Grid &rgrid);

    bool operator==(const Node &lnode,
                    const Node &rnode);

    bool operator!=(const Node &lnode,
                    const Node &rnode);

    double NodeDist(const Node &node1,
                    const Node &node2);

    double NodeManhattanDist(const Node &node1,
                             const Node &node2);

    double PointDist(const Point &lpoint,
                     const Point &rpoint);

    double PointManhattanDist(const Point &lpoint,
                              const Point &rpoint);
}
#endif // NODE_NEW_H
