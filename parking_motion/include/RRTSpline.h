/**
 * @file RRTSpline.h
 * @author Yang Qiu (st163984@stud.uni-stuttgart.de)
 * @brief Using cubic B-Spline to get a smooth trajectory from initial RRT* path
 * 
 * Input:
 *  1. Subscriber to get the global path from topic /rrt_star_path
 *  2. The maximum speed of the vehicle
 *  3. Half length of vehicle, to add redundant control points at start and end waypoints
 *  4. Interpolation number between two control points
 *  5. Direction of the start and end waypoints
 * 
 * Output:
 *  1. CSV file, which contains all information about the Spline (x, y, theta, curvature, velocity)
 *  2. Publish the topic to topic /final_path
 * 
 * Process:
 *  1. Get the global path from topic /rrt_star/path
 *  2. Add redundant control points at start and end waypoints
 *  3. Compute the basis function of Cubic B-Spline
 *  4. Compute the spline points position (x,y)
 *  5. Compute the curvature, theta and velocity of each spline point
 *  6. Save the matrix with all information in CSV file and publish into /global/bahn topic
 * 
 * @attention change csv file path before using
 * 
 * @version 0.1
 * @date 2021-03-21
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef _RRTSPLINE_H
#define _RRTSPLINE_H

#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <vector>
#include <Eigen/Eigen>
#include <Eigen/LU>

#include <time.h>

#include <ros/ros.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>

#include <vehicle_control/Bahn.h>
#include <vehicle_control/Waypoint.h>

using namespace std;

/**
 * @brief use cubic B-spline to get a smooth trajectory from RRT path
 * 
 */
class RRTSpline
{
public:
    /**
     * @brief Construct a new RRTSpline object
     * 
     * @param nh 
     */
    RRTSpline(ros::NodeHandle *nh);

    virtual ~RRTSpline();

    /**
     * @brief call back function for subscribing the RRT* path
     * 
     * @param rrt_path_msg path message from RRT*
     */
    void callbackRRTPath(const nav_msgs::Path &rrt_path_msg);

    /**
     * @brief compute the cubic B-Spline functions
     * 
     */
    void computeBSpline();

    /**
     * @brief compute the curvature theta and velocity of each trajectory point
     * 
     */
    void computeCurvature();

    /**
     * @brief save spline matrix in csv file
     * 
     * @warning change the csv path before using
     */
    void splinePointsCsvSaver();

    /**
     * @brief publish the calculated path
     * 
     * bahn: trajectory following (defined in vehicle_control package)
     * path: visualization in Rviz
     */
    void splinePointsPublish();

    // ------------------------------------------ROS-----------------------------------------------
    /// subscriber to get the occupancy grid map from topic /map, callback func: GetOccupancyMap
    ros::Subscriber rrt_path_sub;
    /// publisher to publish the path message to /rrt/path topic
    ros::Publisher spline_path_pub;
    /// publisher to publish the path to /global/bahn
    ros::Publisher global_bahn_pub;
    /// Path message to be pulished
    nav_msgs::Path spline_path_msg;
    /// Bahn message to be published
    vehicle_control::Bahn global_bahn_msg;

    // ----------------------------------------Vehicle---------------------------------------------
    // the following parameter are vehicle parameter
    /// the max velocity in [m/s] 1.4 m/s == 5km/h
    double velocity_max = 1.4;
    /// half of the length of vehicle
    double L = 1.0;
    /// gravity Coefficient
    double g = 9.81;
    /// friction Coefficient
    double mu = 1.0;

    // ------------------------------------Cubic B Spline------------------------------------------
    // the following parameter are initial waypoints revelant
    /// number of initial waypoints
    int waypoints_num = 0;
    /// matrix to storage (x,y) of initial waypoints
    Eigen::MatrixXd matrix_waypoints;

    // the following parameter are B-Spline revelant
    // parametrizations of cubic segments, from 0 to 1, step size 0.01,
    /// interpolation points number of one segment
    int interpolation_num = 51;

    // vector at begin and end of waypoints
    Eigen::Vector2d vector_v_begin; // direction of begin waypoint
    Eigen::Vector2d vector_v_end;   // direction of end waypoint

    // basic functions for Cubic B-Spline
    Eigen::VectorXd vector_f1 = Eigen::VectorXd::Zero(interpolation_num); // basic function B1
    Eigen::VectorXd vector_f2 = Eigen::VectorXd::Zero(interpolation_num); // basic function B2
    Eigen::VectorXd vector_f3 = Eigen::VectorXd::Zero(interpolation_num); // basic function B3
    Eigen::VectorXd vector_f4 = Eigen::VectorXd::Zero(interpolation_num); // basic function B4

    // first Derivative of basic functions
    Eigen::VectorXd vector_v1 = Eigen::VectorXd::Zero(interpolation_num); // B'1
    Eigen::VectorXd vector_v2 = Eigen::VectorXd::Zero(interpolation_num); // B'2
    Eigen::VectorXd vector_v3 = Eigen::VectorXd::Zero(interpolation_num); // B'3
    Eigen::VectorXd vector_v4 = Eigen::VectorXd::Zero(interpolation_num); // B'4

    // second Derivative of basic functions
    Eigen::VectorXd vector_a1 = Eigen::VectorXd::Zero(interpolation_num); // B''1
    Eigen::VectorXd vector_a2 = Eigen::VectorXd::Zero(interpolation_num); // B''2
    Eigen::VectorXd vector_a3 = Eigen::VectorXd::Zero(interpolation_num); // B''3
    Eigen::VectorXd vector_a4 = Eigen::VectorXd::Zero(interpolation_num); // B''4

    // matrix to storage spline points of each segment
    Eigen::MatrixXd matrix_segment_points = Eigen::MatrixXd::Zero(2, interpolation_num);
    Eigen::MatrixXd matrix_segment_points_v = Eigen::MatrixXd::Zero(2, interpolation_num);
    Eigen::MatrixXd matrix_segment_points_a = Eigen::MatrixXd::Zero(2, interpolation_num);
    // matrix to storage total spline points
    Eigen::MatrixXd matrix_spline_points;   // position matrix
    Eigen::MatrixXd matrix_spline_points_v; // first Derivative of position matrix
    Eigen::MatrixXd matrix_spline_points_a; // second Derivative of position matrix

    /// matrix to storage curvature of spline points
    Eigen::MatrixXd matrix_curvature;
    /// matrix to storage theta of spline points
    Eigen::MatrixXd matrix_theta;
    /// matrix to storage velocity of spline points
    Eigen::MatrixXd matrix_velocity;

    /// matrix to storage all information (x, y, theta, curvature, velocity)
    Eigen::MatrixXd matrix_spline_all;

    // length of the total path
    double path_length;

    // -----------------------------------------TIME-----------------------------------------------
    clock_t start, middle, end;
};
#endif