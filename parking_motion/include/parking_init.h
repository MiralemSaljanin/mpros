#ifndef PARKING_INIT_H
#define PARKING_INIT_H

#include <vector>
#include <string>

#include "ros/ros.h"


class ParkingInit
{
public:
    ParkingInit(ros::NodeHandle &nh);
    ~ParkingInit();
    void PubTopic(const ros::Publisher &pub,
                  const std::vector<std::vector<double>> &waypoints,
                  const std::string &topicName);

    std::vector<std::vector<double>> goalpoints_;
    ros::Publisher goalpoints_pub_;
    ros::Publisher path_pub_;
};

#endif // !PARKING_INIT_H