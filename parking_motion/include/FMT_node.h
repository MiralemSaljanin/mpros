#ifndef FMT_NODE_H
#define FMT_NODE_H

#include <cmath>

class Node
{
public:
    Node();
    Node(const double &x, const double &y, const double &cost, Node *parent);
    virtual ~Node();

    double x_;
    double y_;
    double cost_;
    Node *parent_;
};

struct
{
    bool operator()(const Node *lhs, const Node *rhs) const
    {
        return lhs->cost_ < rhs->cost_;
    }
} NodeCostCmp;

#endif // !FMT_NODE_H
