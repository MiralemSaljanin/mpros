#ifndef RRTSTAR_NEW_H
#define RRTSTAR_NEW_H

#include "node_new.h"

#include "ros/ros.h"
#include "nav_msgs/OccupancyGrid.h"
#include "nav_msgs/Path.h"
#include "visualization_msgs/Marker.h"

#include <vector>
#include <map>

namespace NEW
{

    class RRTStarNew
    {
    public:
        RRTStarNew(ros::NodeHandle *nh);

        virtual ~RRTStarNew();

    private:
        void GetOccupancyMap(const nav_msgs::OccupancyGrid::ConstPtr &map_msg);

        void InflateObstacle();

        bool RunRRTStar();

        bool IsPointInCollision(Point point);

        Point SampleRandomPoint();

        int NearestNodeIdx(const Point &sample_point);

        Node SteerNewNode(Node &nearest_node, const Point &sample_point);

        bool CollisionFree(const Node &nearest_node, const Node &new_node);

        void ExpandTree(Node &new_node, const int &nearest_idx);

        bool IsNearGoal(Node &new_node);

        void GetPath(Node &final_node);

        void PubPath();

        std::vector<int> FindNearNodes(const Node &new_node);

        void ChooseParentNode(const std::vector<int> &near_nodes_idx, const int &nearest_idx, Node &new_node);

        void Rewire(const std::vector<int> &near_node_index_list, Node &new_node);

        Point GridToPoint(const Grid grid);

        Grid PointToGrid(const Point point);

        int PointToMapValue(const Point point);

        void PubVertex(Node &new_node);

    private:
        ros::Subscriber map_sub_;
        ros::Publisher path_pub_;
        ros::Publisher tree_pub_;
        nav_msgs::Path path_msg_;
        visualization_msgs::Marker msg_tree_vertex_;
        visualization_msgs::Marker msg_tree_edge_;

        Grid map_size_ij_; // height, width
        Point map_size_xy_;
        double map_resolution_;
        Point map_origin_; // coordinate of grid (i=0, j=0)
        double obstacle_inflation_radius_ = 1.8;
        std::vector<std::vector<int>> grid_map_;
        std::vector<std::vector<int>> grid_inflation_;

        int max_iter_ = 1000000;
        int goal_found_iter_ = -1;
        int goal_bias_rate_ = 10;
        double step_size_ = 2.0;
        double goal_tolerance_ = step_size_;
        double search_radius_ = 5 * step_size_;
        Point start_point_;
        Point goal_point_;
        Node start_node_;
        Node goal_node_;
        bool goal_found_ = false;
        bool path_improving = false;

        std::vector<Node*> tree_;

        clock_t start_, middle_, end_;
    };
}

#endif // RRTSTAR_NEW_H