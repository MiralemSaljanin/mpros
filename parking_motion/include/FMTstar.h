#ifndef FMTSTAR_H
#define FMTSTAR_H

#include "FMT_node.h"
#include "nav_map.h"
#include "ros/ros.h"
#include "nav_msgs/Path.h"
#include <set>
#include <vector>

typedef std::vector<Node *> SET;

class FMT
{
public:
    FMT(ros::NodeHandle &nh, std::vector<std::vector<double>> &waypoints, const double &r_search);
    ~FMT();
    void LoadMap(const NavMap &map);
    NavMap GetMap();
    /**
     * @brief Generate num_sample_ collision free samples
     * 
     */
    void SampleFree();
    double Cost(const Node *node1, const Node *node2);
    double CalDist(const Node *node1, const Node *node2);
    /**
     * @brief Get the nodes near z
     * 
     * @param nodelist Search in this list
     * @param z central node
     * @param r_near search radius
     * @return node pointer list containing nodes near z
     */
    SET GetNearNodes(SET nodelist, Node *z, const double &r_near);
    void SetPath(Node *local_goal);
    /**
     * @brief Check if point (x, y) is in collsion
     * 
     * @param x 
     * @param y 
     * @return true 
     * @return false 
     */
    bool PointInCollision(const double &x, const double &y);
    /**
     * @brief Check if path from (x1, y1) to (x2, y2) is collision-free
     * 
     * @param x1 
     * @param y1 
     * @param x2 
     * @param y2 
     * @return true 
     * @return false 
     */
    bool CollisionFree(const double &x1, const double &y1, const double &x2, const double &y2);
    /**
     * @brief Interpolate a coordinate from x1 towards x2 with length of step
     * 
     * @param x1 start point
     * @param x2 end point
     * @param step step size
     * @return double
     */
    double Interpolate(const double &x1, const double &x2, const double &step);
    bool Planning(Node *local_start, Node *local_goal);
    bool PlanningSquence();
    void PubPath(const std::vector<std::vector<double>> &path_vec);
    std::vector<std::vector<double>> GetPath();
    std::vector<std::vector<double>> GetTree();

private:
    ros::NodeHandle* nh_;
    ros::Publisher fmt_path_pub_;

    nav_msgs::Path fmt_path_msg_;
    SET v_open_, v_close_, v_unvisited_;
    double num_sample_; // number of samples
    double r_near_;     // radius for searching near nodes
    Node *start_node_;
    SET goal_sequence_;
    double x_max_, x_min_, y_max_, y_min_;

    NavMap nav_map_;
    std::vector<std::vector<uint8_t>> grid_map_;
    double map_resolution_;
    double map_origin_x_, map_origin_y_;
    int map_height_, map_width_;
    std::vector<std::vector<double>> path_;
    std::vector<std::vector<double>> tree_;
};

#endif // !FMTSTAR_H
