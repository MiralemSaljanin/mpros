# README

This package describes a car-like model with Ackermann-Steering.

# Usage

```
roslaunch vehicle_description vehicle_description.launch
```

show model in Gazebo and Rviz

# Model Description

## Link:

base_link;

left_steering_hinge; 
right_steering_hinge;

left_front_wheel; 
right_front_wheel; 
left_back_wheel; 
right_back_wheel;

## Joint:

| joint name                 | type       | controller              |
| -------------------------- | ---------- | ----------------------- |
| left_steering_hinge_joint  | revolute   | JointPositionController |
| right_steering_hinge_joint | revolute   | JointPositionController |
| left_back_wheel_joint      | continuous | JointVelocityController |
| right_back_wheel_joint     | continuous | JointVelocityController |

## tf:

![](pic/tf.png)


# Preview

![](pic/rviz.png)
![](pic/gazebo.png)