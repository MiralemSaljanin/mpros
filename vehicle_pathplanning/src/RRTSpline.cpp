/**
 * @file CubicBSpline.cpp
 * @author Yang Qiu (st163984@stud.uni-stuttgart.de)
 * @brief Using cubic B spline interpolation
 * @version 0.1
 * @date 2021-02-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <RRTSpline.h>

using namespace std;

RRTSpline::RRTSpline(ros::NodeHandle *nh)
{
    rrt_path_sub = nh->subscribe("/rrt_star/path", 1, &RRTSpline::callbackRRTPath, this);

    bool latch = true;
    spline_path_pub = nh->advertise<nav_msgs::Path>("/rrt_spline/path", 10, latch);
    global_bahn_pub = nh->advertise<vehicle_control::Bahn>("/global/bahn", 10, latch);
}

RRTSpline::~RRTSpline() {}

void RRTSpline::callbackRRTPath(const nav_msgs::Path &rrt_path_msg)
{
    start = clock(); // time to end the calculate

    // set the direction of start and end point
    vector_v_begin << 1.0, 0.0;
    vector_v_end << 1.0, 1.0;

    cout << "------------------RRT Spline-------------------------------" << endl;
    cout << "Got the RRT path" << endl;
    // get the waypoints number from path_msg
    waypoints_num = rrt_path_msg.poses.size();
    // resize the matrix
    matrix_waypoints.resize(2, waypoints_num);

    // push back the original waypoints position to the matrix
    for (int i = 0; i < waypoints_num; i++)
    {
        // the waypoints in rrt_path_msg is from goal to start
        // should read reverse from the rrt_path_msg
        geometry_msgs::PoseStamped temp_pathpoint = rrt_path_msg.poses[waypoints_num - i - 1];
        matrix_waypoints(0, i) = temp_pathpoint.pose.position.x;
        matrix_waypoints(1, i) = temp_pathpoint.pose.position.y;

        // cout << matrix_waypoints(0, i) << ", " << matrix_waypoints(1, i) << endl;
    }
    // std::cerr << "the max velocity of vehicle is " << velocity_max << std::endl;
    std::cerr << "the number of initial waypoints is " << waypoints_num << std::endl;

    computeBSpline();
    computeCurvature();
    splinePointsCsvSaver();
    splinePointsPublish();

    end = clock(); // time to end the calculate
    cout << "Time to smooth RRT* with Cubic-Spine: " << double(end - start) / CLOCKS_PER_SEC << "s" << endl;
}

void RRTSpline::computeBSpline()
{
    // see https://math.stackexchange.com/questions/699113/
    // web page: what-is-the-relationship-between-cubic-b-splines-and-cubic-splines
    // normalization range [0, 1]
    for (int i = 0; i < interpolation_num; i++)
    {
        double j = double(i) / (interpolation_num - 1);

        // basic functions for Cubic B-Spline
        vector_f1(i) = pow((1.0 - j), 3.0) / 6.0;
        vector_f2(i) = (3.0 * pow(j, 3.0) - 6.0 * pow(j, 2.0) + 4.0) / 6.0;
        vector_f3(i) = (-3.0 * pow(j, 3.0) + 3.0 * pow(j, 2.0) + 3.0 * j + 1.0) / 6.0;
        vector_f4(i) = pow(j, 3.0) / 6.0;

        // first Derivative of basic functions
        vector_v1(i) = -1.0 / 2.0 * pow((1.0 - j), 2.0);
        vector_v2(i) = 1.0 / 2.0 * (3.0 * pow(j, 2.0) - 4.0 * j);
        vector_v3(i) = 1.0 / 2.0 * (-3.0 * pow(j, 2.0) + 2.0 * j + 1.0);
        vector_v4(i) = 1.0 / 2.0 * pow(j, 2.0);

        // second Derivative of basic functions
        vector_a1(i) = 1.0 - j;
        vector_a2(i) = 3.0 * j - 2.0;
        vector_a3(i) = -3.0 * j + 1.0;
        vector_a4(i) = j;
    }
    // std::cerr << "......................................." << std::endl;
    // std::cerr << vector_v1 << vector_v2 << vector_v3 << vector_v4 << std::endl;

    // add control points on both sides of the begin and end waypoint to
    // make sure the spline get through start and end waypoints
    Eigen::MatrixXd matrix_waypoints_new;
    int waypoints_num_new = waypoints_num + 4;
    matrix_waypoints_new.resize(2, waypoints_num_new);

    // first 3 cols of new matrix are begin point relevant
    matrix_waypoints_new.col(0) = matrix_waypoints.col(0) - vector_v_begin * L;
    matrix_waypoints_new.col(1) = matrix_waypoints.col(0);
    matrix_waypoints_new.col(2) = matrix_waypoints.col(0) + vector_v_begin * L;
    // the middle of new matrix are same as original matrix
    int block_num = waypoints_num - 2;
    matrix_waypoints_new.block(0, 3, 2, block_num) = matrix_waypoints.block(0, 1, 2, block_num);
    // last 3 cols of new matrix are end point relevant
    matrix_waypoints_new.col(waypoints_num_new - 3) = matrix_waypoints.col(waypoints_num - 1) -
                                                      vector_v_end * L;
    matrix_waypoints_new.col(waypoints_num_new - 2) = matrix_waypoints.col(waypoints_num - 1);
    matrix_waypoints_new.col(waypoints_num_new - 1) = matrix_waypoints.col(waypoints_num - 1) +
                                                      vector_v_end * L;

    // resize the total interpolation points matrix
    matrix_spline_points = Eigen::MatrixXd::Zero(2, interpolation_num * (waypoints_num_new - 3));
    matrix_spline_points_v = Eigen::MatrixXd::Zero(2, interpolation_num * (waypoints_num_new - 3));
    matrix_spline_points_a = Eigen::MatrixXd::Zero(2, interpolation_num * (waypoints_num_new - 3));

    // compute the interpolation points for each segment
    for (int i = 0; i < waypoints_num_new - 3; i++)
    {
        // y = B1 * C1 + B2 * C2 + B3 * C3 + B4 * C4
        matrix_segment_points = matrix_waypoints_new.col(i) * vector_f1.transpose() +
                                matrix_waypoints_new.col(i + 1) * vector_f2.transpose() +
                                matrix_waypoints_new.col(i + 2) * vector_f3.transpose() +
                                matrix_waypoints_new.col(i + 3) * vector_f4.transpose();

        matrix_segment_points_v = matrix_waypoints_new.col(i) * vector_v1.transpose() +
                                  matrix_waypoints_new.col(i + 1) * vector_v2.transpose() +
                                  matrix_waypoints_new.col(i + 2) * vector_v3.transpose() +
                                  matrix_waypoints_new.col(i + 3) * vector_v4.transpose();

        matrix_segment_points_a = matrix_waypoints_new.col(i) * vector_a1.transpose() +
                                  matrix_waypoints_new.col(i + 1) * vector_a2.transpose() +
                                  matrix_waypoints_new.col(i + 2) * vector_a3.transpose() +
                                  matrix_waypoints_new.col(i + 3) * vector_a4.transpose();

        // accumulate the matrix_segment_points into matrix_spline_points
        matrix_spline_points.block(0, i * interpolation_num, 2, interpolation_num) = matrix_segment_points;
        matrix_spline_points_v.block(0, i * interpolation_num, 2, interpolation_num) = matrix_segment_points_v;
        matrix_spline_points_a.block(0, i * interpolation_num, 2, interpolation_num) = matrix_segment_points_a;
    }
}

void RRTSpline::computeCurvature()
{
    int num = matrix_spline_points.cols();
    matrix_curvature.resize(1, num);
    matrix_theta.resize(1, num);
    matrix_velocity.resize(1, num);

    std::cerr << "the number of spline points is " << matrix_curvature.cols() << std::endl;

    double curvature_max = 0.0;
    double radius_min = 0.0;

    // compute curvature and theta of the spline
    for (int i = 0; i < matrix_curvature.cols(); i++)
    {
        double curvature_numerator = matrix_spline_points_v(0, i) * matrix_spline_points_a(1, i) -
                                     matrix_spline_points_v(1, i) * matrix_spline_points_a(0, i);
        double curvature_denominator = pow(sqrt(pow(matrix_spline_points_v(0, i), 2) + pow(matrix_spline_points_v(1, i), 2)), 3);
        matrix_curvature(0, i) = curvature_numerator / curvature_denominator;

        // find the max curvature / min turning radius of the entire spline
        if (curvature_max < abs(matrix_theta(0, i)))
        {
            curvature_max = abs(matrix_theta(0, i));
        }

        matrix_theta(0, i) = atan2(matrix_spline_points_v(1, i), matrix_spline_points_v(0, i));
    }

    radius_min = 1 / curvature_max;
    std::cerr << "the min turning radius of the spline is " << radius_min << std::endl;

    // ----------------------set the velocity of each spline points--------------------------------
    // at a given turing radius, the max allowed velocity can be calculated as:
    // v_max = sqrt(mu * g * radius) = sqrt(mu * g / curvature)

    // the allowable curvature at max defined velocity
    // double curvature_at_max_velocity = mu * g / pow(velocity_max, 2);
    // // compute the velocity of the spline
    // for (int i = 0; i < matrix_curvature.cols(); i++)
    // {
    //     if (abs(matrix_curvature(0, i)) < curvature_at_max_velocity)
    //     {
    //         matrix_velocity(0, i) = velocity_max;
    //     }
    //     else
    //     {
    //         matrix_velocity(0, i) = sqrt(mu * g / abs(matrix_curvature(0, i)));
    //     }
    // }

    //--------------------------------deceleration phase-------------------------------------------
    int decelerate_points_num = int(velocity_max * 15);
    for (int i = 0; i < matrix_curvature.cols(); i++)
    {
        if (i < matrix_curvature.cols() - decelerate_points_num)
        {
            matrix_velocity(0, i) = velocity_max;
        }
        else
        {
            matrix_velocity(0, i) = velocity_max / decelerate_points_num * (matrix_curvature.cols() - i -1);
        }
    }

    // add all information into matrix_spline_all
    int end_num = matrix_spline_points.cols();
    matrix_spline_all.resize(5, end_num);
    matrix_spline_all.row(0) = matrix_spline_points.row(0);
    matrix_spline_all.row(1) = matrix_spline_points.row(1);
    matrix_spline_all.row(2) = matrix_theta.row(0);
    matrix_spline_all.row(3) = matrix_curvature.row(0);
    matrix_spline_all.row(4) = matrix_velocity.row(0);
}

void RRTSpline::splinePointsCsvSaver()
{
    ofstream outputSplineAll;
    outputSplineAll.open("/home/yang/Spline/RRTSplineAll.csv", ios::out);
    // translate the matrix from (5,n) to (n,5)
    Eigen::MatrixXd matrix_spline_all_trans = matrix_spline_all.transpose();
    for (int i = 0; i < matrix_spline_all_trans.rows(); i++)
    {
        for (int j = 0; j < matrix_spline_all_trans.cols(); j++)
        {
            outputSplineAll << matrix_spline_all_trans(i, j);
            if (j < (matrix_spline_all_trans.cols() - 1))
            {
                outputSplineAll << ",";
            }
        }
        if (i < (matrix_spline_all_trans.rows() - 1))
        {
            outputSplineAll << endl;
        }
    }
    outputSplineAll.close();
}

void RRTSpline::splinePointsPublish()
{
    geometry_msgs::PoseStamped temp_pathpoint;
    vehicle_control::Waypoint temp_bahnpoint;

    int num = matrix_spline_points.cols();
    for (int i = 0; i < num; i++)
    {
        // path point, just for visualization
        temp_pathpoint.pose.position.x = matrix_spline_points(0, i);
        temp_pathpoint.pose.position.y = matrix_spline_points(1, i);
        this->spline_path_msg.poses.push_back(temp_pathpoint);

        // waypoint, customized message
        temp_bahnpoint.pose.pose.position.x = matrix_spline_points(0, i);
        temp_bahnpoint.pose.pose.position.y = matrix_spline_points(1, i);
        temp_bahnpoint.curvature = matrix_spline_all(3, i);
        temp_bahnpoint.twist.twist.linear.x = matrix_spline_all(4, i);

        tf2::Quaternion q;
        double yaw = matrix_spline_all(2, i);
        q.setRPY(0, 0, yaw);
        q.normalize();
        temp_bahnpoint.pose.pose.orientation = tf2::toMsg(q);

        this->global_bahn_msg.waypoints.push_back(temp_bahnpoint);
    }

    this->spline_path_msg.header.frame_id = "world";
    this->global_bahn_msg.header.frame_id = "/world";

    this->spline_path_pub.publish(spline_path_msg);
    this->global_bahn_pub.publish(global_bahn_msg);

    // calculate the total length of trajectory
    path_length = 0.0;
    for (int i = 1; i < num; i++)
    {
        double parent_x = matrix_spline_points(0, i - 1);
        double parent_y = matrix_spline_points(1, i - 1);
        double current_x = matrix_spline_points(0, i);
        double current_y = matrix_spline_points(1, i);
        double dx = current_x - parent_x;
        double dy = current_y - parent_y;
        path_length += sqrt(pow(dx, 2) + pow(dy, 2));
    }
    cout << "The length of the path is: " << this->path_length << endl;
}
