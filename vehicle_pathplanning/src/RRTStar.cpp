#include <RRTStar.h>

using namespace std;

RRTStar::RRTStar(ros::NodeHandle *nh)
{
    map_sub = nh->subscribe("/map", 1, &RRTStar::GetOccupancyMap, this);
    bool latch = true;
    rrt_star_path_pub = nh->advertise<nav_msgs::Path>("/rrt_star/path", 10, latch);
}

RRTStar::~RRTStar()
{
}

void RRTStar::GetOccupancyMap(const nav_msgs::OccupancyGrid::ConstPtr &map_msg)
{
    cout << "------------------Map Params-------------------------------" << endl;
    cout << "Successfully got the map" << endl;

    // ----------------------------------Set Start and Goal-----------------------------------------
    // set the start and goal points
    Node start_node(0.0, 0.0, 0.0, -1);
    
    // Map 
    // Node goal_node(120.0, 0.0, 0.0, 0);

    // final simulation
    Node goal_node(359.0, 217.0, 0.0, 0);

    start_x = start_node.x;
    start_y = start_node.y;
    goal_x = goal_node.x;
    goal_y = goal_node.y;
    cout << "The (goal_x, goal_y) is (" << goal_x << "," << goal_y << ")" << endl;

    // ------------------------------------Map Information-----------------------------------------
    map_height = map_msg->info.height;
    map_width = map_msg->info.width;
    map_resolution = map_msg->info.resolution;
    x_origin = map_msg->info.origin.position.x;
    y_origin = map_msg->info.origin.position.y;

    // print the information of the map
    cout << "The height, width of the map is (" << map_height << "," << map_width << ")" << endl;
    cout << "The resolution of this map is " << map_resolution << endl;
    cout << "The (x_origin, y_origin) is (" << x_origin << "," << y_origin << ") in /world frame" << endl;

    // --------------------------------------Get Grid Map------------------------------------------
    // resize the vector
    grid.resize(map_height);
    grid_inflation.resize(map_height);
    for (int i = 0; i < map_height; i++)
    {
        grid[i].resize(map_width);
        grid_inflation[i].resize(map_width);
    }

    // storage the map in 2D vector
    for (int i = 0; i < map_height; i++)
    {
        for (int j = 0; j < map_width; j++)
        {
            // int curr_cell_position = i + map_width * j;
            int curr_cell_position = i * map_width + j;

            if (map_msg->data[curr_cell_position] == 0)
            {
                grid[i][j] = 0; // unoccupied cell
            }
            else
            {
                grid[i][j] = 1; // occupied (100) or unknown cell (-1)
            }
        }
    }

    // ----------------------------------Obstacle Inflation----------------------------------------
    InflateObstacle();

    // check the start and goal point is occupied or not
    if (IsPointObstacle(start_node.x, start_node.y))
    {
        ROS_INFO("The Start point is occupied, please change it");
        return;
    }
    if (IsPointObstacle(goal_node.x, goal_node.y))
    {
        ROS_INFO("The Goal point is occupied, please change it");
        return;
    }

    cout << "------------------RRT Star-------------------------------" << endl;

    start = clock(); // time to start the calculate

    // -------------------------------------Main LOOP-----------------------------------------------
    // push back the start_node into tree list
    this->tree_list.push_back(start_node);

    bool is_path_found;
    int rrt_star_count = 0;
    while (rrt_star_count < this->max_rrt_iteration)
    {
        rrt_star_count++;

        // create a random sample point
        auto sample_point = SampleRandomPoint();
        // find the nearest node with sample point
        int nearest_node_index = FindNearestNode(this->tree_list, sample_point);
        // create the new node, with nearest node and sample point
        Node new_node = CreateNewNode(this->tree_list[nearest_node_index], nearest_node_index, sample_point);

        // check the path collision, true: collision
        bool path_collision = IsPathCollision(this->tree_list[nearest_node_index], new_node);
        if (!path_collision) // no collision
        {
            // find the neighbour nodes of the new_node
            auto near_node_index_list = FindNearNodes(this->tree_list, new_node);
            // get the cost and parent of the new_node
            new_node = ChooseParentNode(near_node_index_list, new_node);
            // push back the new_node to tree_list
            this->tree_list.push_back(new_node);
            // rewire the neighbour nodes
            RewireNearNodes(near_node_index_list, new_node);

            // check is the new node near goal_node
            if (IsNearGoal(new_node))
            {
                // treat goal_node as a new_node
                auto near_goal_index_list = FindNearNodes(this->tree_list, goal_node);
                goal_node = ChooseParentNode(near_goal_index_list, goal_node);
                RewireNearNodes(near_goal_index_list, goal_node);

                // check the final step is collision or not
                bool goal_collision = IsPathCollision(this->tree_list[goal_node.parent_index], goal_node);
                // if not collision
                if (!goal_collision)
                {
                    cout << "Path found!" << endl;

                    this->tree_list.push_back(goal_node);
                    // the index of last new node
                    int last_index = this->tree_list.size() - 1;
                    // get the final path from RRT tree
                    GetFinalPath(last_index);

                    // publish the path
                    PubRRTPath();

                    cout << "The global path has been found at the " << rrt_star_count << " interation" << endl;
                    cout << "The nodes number in the Tree: " << this->tree_list.size() << endl;
                    cout << "The nodes number in the Path: " << this->rrt_star_path_msg.poses.size() << endl;

                    is_path_found = true;

                    end = clock(); // time to end the calculate
                    cout << "Time to solve RRT*: " << double(end - start) / CLOCKS_PER_SEC << "s" << endl;

                    break;
                }
            }
        }
    }

    if (!is_path_found)
    {
        ROS_INFO("Fail to find the path! ");
    }
}

bool RRTStar::IsPointObstacle(double x, double y)
{
    int is_obstacle = CoordinateToValue(x, y);
    if (is_obstacle)
        return true;
    else
        return false;
}

void RRTStar::InflateObstacle()
{
    // change obstacle_inflation_radius to pixel number
    int obstacle_inflation_pixel_num = int(obstacle_inflation_radius / this->map_resolution);
    for (int i = 0; i < map_height; i++)
    {
        for (int j = 0; j < map_width; j++)
        {
            if (grid[i][j] == 1)
            {
                int inflation_i_min = i - obstacle_inflation_pixel_num;
                int inflation_i_max = i + obstacle_inflation_pixel_num;
                int inflation_j_min = j - obstacle_inflation_pixel_num;
                int inflation_j_max = j + obstacle_inflation_pixel_num;
                for (int inflation_i = inflation_i_min; inflation_i < inflation_i_max + 1; inflation_i++)
                {
                    for (int inflation_j = inflation_j_min; inflation_j < inflation_j_max + 1; inflation_j++)
                    {
                        grid_inflation[inflation_i][inflation_j] = 1;
                    }
                }
            }
        }
    }

    // push the inflated obstacle back to original grid vector
    for (int i = 0; i < map_height; i++)
    {
        for (int j = 0; j < map_width; j++)
        {
            grid[i][j] = grid_inflation[i][j];
        }
    }
}

std::array<double, 2> RRTStar::SampleRandomPoint()
{
    std::random_device rd;  // get the random seed from device
    std::mt19937 gen(rd()); // seeded with rd()

    std::uniform_int_distribution<int> goal_sample(1, 100);                 // set random number range for goal sampling                                               // seeded with rd()
    std::uniform_int_distribution<int> col_random(0, this->map_width - 1);  // set random number range for col j
    std::uniform_int_distribution<int> row_random(0, this->map_height - 1); // set random number range for row i

    int goal_random = goal_sample(gen);
    int i = row_random(gen);
    int j = col_random(gen);

    double sample_x;
    double sample_y;

    if (goal_random <= goal_sample_rate) // sample point is goal point
    {
        sample_x = this->goal_x;
        sample_y = this->goal_x;
    }
    else // sample point is random point in the map
    {
        sample_x = ColToX(j);
        sample_y = RowToY(i);
    }

    return {sample_x, sample_y};
}

int RRTStar::FindNearestNode(const std::vector<Node> &node_list, const array<double, 2> &sample_point)
{
    int nearest_node_index;
    double nearest_node_distance = std::numeric_limits<double>::max();
    for (int i = 0; i < node_list.size(); i++)
    {
        // distance compare, do not have to sqrt
        double distance = pow(node_list[i].x - sample_point[0], 2) + pow(node_list[i].y - sample_point[1], 2);
        if (distance < nearest_node_distance)
        {
            nearest_node_index = i;
            nearest_node_distance = distance;
        }
    }
    return nearest_node_index;
}

Node RRTStar::CreateNewNode(const Node &nearest_node, const int &nearest_node_index, const array<double, 2> &sample_point)
{
    double dx = sample_point[0] - nearest_node.x;    // x distance
    double dy = sample_point[1] - nearest_node.y;    // y distance
    double distance = sqrt(pow(dx, 2) + pow(dy, 2)); // distance between nearest node and sample point
    double theta = atan2(dy, dx);                    // angle between nearest node and sample point

    Node new_node(0.0, 0.0, 0.0, 0);

    if (distance < this->step_size) // the sample point is in the range of step size
    {
        new_node.x = sample_point[0];
        new_node.y = sample_point[1];
        new_node.cost = nearest_node.cost + distance;
    }
    else //  the sample point is out of range of step size
    {
        new_node.x = nearest_node.x + cos(theta) * this->step_size;
        new_node.y = nearest_node.y + sin(theta) * this->step_size;
        new_node.cost = nearest_node.cost + this->step_size;
    }

    new_node.parent_index = nearest_node_index;

    return (new_node);
}

// the new node is not yet pushes back to the tree
vector<int> RRTStar::FindNearNodes(const std::vector<Node> &node_list, const Node &new_node)
{
    vector<int> near_nodes_index_list;

    // Variable search distance: with increase list length, the search range should be smaller
    // int near_length = node_list.size() + 1;
    // double near_radius = 50 * sqrt(log(near_length) / near_length);

    // Constant search distance
    double near_radius = this->search_radius;

    for (int i = 0; i < node_list.size(); i++)
    {
        // calculate the distance between new_node and all nodes
        double dx = new_node.x - node_list[i].x;
        double dy = new_node.y - node_list[i].y;
        double distance = sqrt(pow(dx, 2) + pow(dy, 2));

        // if the node is in the search range
        if (distance < near_radius)
        {
            near_nodes_index_list.push_back(i);
        }
    }
    return near_nodes_index_list;
}

Node RRTStar::ChooseParentNode(const std::vector<int> &near_node_index_list, Node &new_node)
{
    if (near_node_index_list.empty()) // if there is no neighbour nodes near the new_node
    {
        return new_node;
    }

    vector<double> cost_list; // vector to storage all cost (from new_node to all neighbour nodes)
    double huge_cost = 10000000.0;

    for (int i = 0; i < near_node_index_list.size(); i++)
    {
        int near_node_index = near_node_index_list[i];               // the index of near_node
        double dx = new_node.x - this->tree_list[near_node_index].x; // x
        double dy = new_node.x - this->tree_list[near_node_index].y; // y
        double distance = sqrt(pow(dx, 2) + pow(dy, 2));             // distance

        // check if collision between near_node and new_node
        bool temp_collision = this->IsPathCollision(this->tree_list[near_node_index], new_node);

        if (!temp_collision)
        {
            // if there is no collision, push back the cost of the new_node into cost_list
            cost_list.push_back(this->tree_list[near_node_index].cost + distance);
        }
        else
        {
            // if there is collision, push back a huge number, which the cost of a node can never reach
            cost_list.push_back(this->tree_list[near_node_index].cost + huge_cost);
        }
    }

    // the index of the parent node with smallest cost
    int min_cost_index = std::min_element(cost_list.begin(), cost_list.end()) - cost_list.begin();
    // the cost of the new_node
    double min_cost = *std::min_element(cost_list.begin(), cost_list.end());

    if (min_cost >= huge_cost)
    {
        // if all the neighbour nodes are collision
        return new_node;
    }

    new_node.cost = min_cost;
    new_node.parent_index = near_node_index_list[min_cost_index];

    return new_node;
}

void RRTStar::RewireNearNodes(const std::vector<int> &near_node_index_list, const Node &new_node)
{
    // push back the new_node back to tree before rewire
    // the index of the new_node
    int current_index = this->tree_list.size() - 1;
    for (int i = 0; i < near_node_index_list.size(); i++)
    {
        int near_node_index = near_node_index_list[i];               // the index of near_node in the tree
        double dx = new_node.x - this->tree_list[near_node_index].x; // x
        double dy = new_node.x - this->tree_list[near_node_index].y; // y
        double distance = sqrt(pow(dx, 2) + pow(dy, 2));             // distance

        double new_cost = new_node.cost + distance; // the cost when the new node is the parent node

        // if the old cost bigger than new cost, rewire the near node to new node
        if (this->tree_list[near_node_index].cost > new_cost)
        {
            bool path_collision = this->IsPathCollision(this->tree_list[near_node_index], new_node);
            if (!path_collision)
            {
                this->tree_list[near_node_index].parent_index = current_index;
                this->tree_list[near_node_index].cost = new_cost;
            }
        }
    }
}

bool RRTStar::IsPathCollision(const Node &nearest_node, const Node &new_node)
{
    // the step_size is 1.0 m, the resolution of the map is 0.1 m
    // thus, max 10 pixel between nearest_node and new_node
    // to RRTstar, the search radius is bigger than step size
    int pixel_between_nodes = int(this->search_radius / this->map_resolution);

    double current_x = nearest_node.x;
    double current_y = nearest_node.y;

    double dx = (new_node.x - nearest_node.x) / pixel_between_nodes;
    double dy = (new_node.y - nearest_node.y) / pixel_between_nodes;

    for (int i = 0; i < pixel_between_nodes; i++)
    {
        current_x += dx;
        current_y += dy;

        if (CoordinateToValue(current_x, current_y))
        {
            return true;
        }
    }

    return false;
}

bool RRTStar::IsNearGoal(const Node &new_node)
{
    // compare the distance between new_node and goal_node with goal_tolerance
    double distance = sqrt(pow(new_node.x - this->goal_x, 2) + pow(new_node.y - this->goal_y, 2));
    if (distance < this->goal_tolerance)
    {
        return true; // the new_node is near goal_node
    }
    else
    {
        return false; // the new_node is far away from goal_node
    }
}

void RRTStar::GetFinalPath(int &last_index)
{
    geometry_msgs::PoseStamped temp_pathpoint;

    // before reaching the start point
    while (this->tree_list[last_index].parent_index != 0)
    {
        // push back currnet node
        temp_pathpoint.pose.position.x = this->tree_list[last_index].x;
        temp_pathpoint.pose.position.y = this->tree_list[last_index].y;
        this->rrt_star_path_msg.poses.push_back(temp_pathpoint);

        // change to the parent node
        last_index = this->tree_list[last_index].parent_index;
    }

    // in the end push back the start node
    temp_pathpoint.pose.position.x = this->start_x;
    temp_pathpoint.pose.position.y = this->start_y;
    this->rrt_star_path_msg.poses.push_back(temp_pathpoint);
}

void RRTStar::PubRRTPath()
{
    this->rrt_star_path_msg.header.frame_id = "world";

    this->rrt_star_path_pub.publish(this->rrt_star_path_msg);

    // ROS_INFO("Successfully publish the path");
}

int RRTStar::CoordinateToValue(double x, double y)
{
    int i = YToRow(y);
    int j = XToCol(x);
    return grid[i][j];
}

int RRTStar::XToCol(double x)
{
    int j = int((x - this->x_origin) / this->map_resolution);
    return j;
}

int RRTStar::YToRow(double y)
{
    int i = int((y - this->y_origin) / this->map_resolution);
    return i;
}

double RRTStar::ColToX(int j)
{
    double x = this->x_origin + j * this->map_resolution;
    return x;
}

double RRTStar::RowToY(int i)
{
    double y = this->y_origin + i * this->map_resolution;
    return y;
}