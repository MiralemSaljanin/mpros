/**
 * @file main.cpp
 * @author Yang Qiu (st163984@stud.uni-stuttgart.de)
 * @brief path planning with RRT and Spline
 * @version 0.1
 * @date 2021-02-15
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <time.h>

#include <CubicBSpline.h>
#include <RRT.h>
#include <RRTStar.h>
#include <RRTSpline.h>

#include "ros/ros.h"

using namespace std;

int main(int argc, char **argv)
{
    ros::init(argc, argv, "pathPlanning_node");

    ros::NodeHandle nh;

    //-------------------------------------------RRT-----------------------------------------------
    // RRT rrt_path_planning = RRT(&nh);

    //-----------------------------------------RRT Star--------------------------------------------
    RRTStar rrtstar_path_planning = RRTStar(&nh);

    //---------------------------------------Spline after RRT Star---------------------------------
    RRTSpline rrt_spline_path_smoothing = RRTSpline(&nh);

    //----------------------Just Spline to create test trajectory for motion control---------------
    // // using cubic B spline interpolation
    // CubicBSpline cubic_b_spline;
    // // load the initial waypoints from csv file
    // cubic_b_spline.waypointsCsvLoader();
    // // compute the position of cubic B-Spline
    // cubic_b_spline.computeBSpline();
    // // compute the curvature theta and velocity of each interpolation point
    // cubic_b_spline.computeCurvature();
    // // save spline matrix in csv file
    // cubic_b_spline.splinePointsCsvSaver();

    ros::spin();

    return 0;
}