/**
 * @file Node.h
 * @author Yang Qiu (st163984@stud.uni-stuttgart.de)
 * @brief Node for path planning
 * @version 0.1
 * @date 2021-03-19
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <Node.h>

using namespace std;

Node::Node() {}

Node::~Node() {}

Node::Node(const double x, const double y, const double cost, const int parent_index)
    : x(x), y(y), cost(0.0), parent_index(parent_index) {}