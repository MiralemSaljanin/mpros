/**
 * @file CubicBSpline.cpp
 * @author Yang Qiu (st163984@stud.uni-stuttgart.de)
 * @brief Using cubic B spline interpolation
 * @version 0.1
 * @date 2021-02-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <CubicBSpline.h>

using namespace std;

CubicBSpline::CubicBSpline(){};

CubicBSpline::~CubicBSpline(){};

///@warning change the absolute path before using
void CubicBSpline::waypointsCsvLoader()
{
    // csv file absolute path
    ifstream waypoints_csv("/home/yang/catkin_ws/src/vehicle_pathplanning/csv/InitialWayPoints.csv");
    if (!waypoints_csv.is_open())
        ROS_INFO("Error loading CSV");
    if (waypoints_csv.is_open())
        ROS_INFO("CSV loaded");

    string line;
    string x_str;
    string y_str;
    // std vector to sourage x, y
    vector<double> location_x, location_y;

    while (getline(waypoints_csv, line))
    {
        istringstream sin(line);
        vector<string> Points;
        string info;
        // read csv line by line, until oef
        while (getline(sin, info, ','))
        {
            Points.push_back(info);
        }
        // storage the x, y each line
        x_str = Points[0];
        y_str = Points[1];

        // string to double by using stringstream
        double x, y, yaw, kappa;
        stringstream sx, sy;
        sx << x_str;
        sy << y_str;
        sx >> x;
        sy >> y;

        location_x.push_back(x);
        location_y.push_back(y);
    }

    // eigen::matrix get data from std::vector
    waypoints_num = location_x.size();
    matrix_waypoints.resize(2, waypoints_num);
    for (int i = 0; i < waypoints_num; i++)
    {
        matrix_waypoints(0, i) = location_x[i];
        matrix_waypoints(1, i) = location_y[i];
    }

    std::cerr << "the max velocity of vehicle is " << velocity_max << std::endl;
    std::cerr << "the number of initial waypoints is " << waypoints_num << std::endl;
}

void CubicBSpline::computeBSpline()
{
    vector_v_begin << 1.0, 0;
    vector_v_end << 1.0, 0;

    // see https://math.stackexchange.com/questions/699113/
    // web page: what-is-the-relationship-between-cubic-b-splines-and-cubic-splines
    // normalization range [0, 1]
    for (int i = 0; i < interpolation_num; i++)
    {
        double j = double(i) / (interpolation_num - 1);

        // basic functions for Cubic B-Spline
        vector_f1(i) = pow((1.0 - j), 3.0) / 6.0;
        vector_f2(i) = (3.0 * pow(j, 3.0) - 6.0 * pow(j, 2.0) + 4.0) / 6.0;
        vector_f3(i) = (-3.0 * pow(j, 3.0) + 3.0 * pow(j, 2.0) + 3.0 * j + 1.0) / 6.0;
        vector_f4(i) = pow(j, 3.0) / 6.0;

        // first Derivative of basic functions
        vector_v1(i) = -1.0 / 2.0 * pow((1.0 - j), 2.0);
        vector_v2(i) = 1.0 / 2.0 * (3.0 * pow(j, 2.0) - 4.0 * j);
        vector_v3(i) = 1.0 / 2.0 * (-3.0 * pow(j, 2.0) + 2.0 * j + 1.0);
        vector_v4(i) = 1.0 / 2.0 * pow(j, 2.0);

        // second Derivative of basic functions
        vector_a1(i) = 1.0 - j;
        vector_a2(i) = 3.0 * j - 2.0;
        vector_a3(i) = -3.0 * j + 1.0;
        vector_a4(i) = j;
    }
    // std::cerr << "......................................." << std::endl;

    // add control points on both sides of the begin and end waypoint to
    // make sure the spline get through start and end waypoints
    Eigen::MatrixXd matrix_waypoints_new;
    int waypoints_num_new = waypoints_num + 4;
    matrix_waypoints_new.resize(2, waypoints_num_new);

    // first 3 cols of new matrix are begin point relevant
    matrix_waypoints_new.col(0) = matrix_waypoints.col(0) - vector_v_begin * L;
    matrix_waypoints_new.col(1) = matrix_waypoints.col(0);
    matrix_waypoints_new.col(2) = matrix_waypoints.col(0) + vector_v_begin * L;
    // the middle of new matrix are same as original matrix
    int block_num = waypoints_num - 2;
    matrix_waypoints_new.block(0, 3, 2, block_num) = matrix_waypoints.block(0, 1, 2, block_num);
    // last 3 cols of new matrix are end point relevant
    matrix_waypoints_new.col(waypoints_num_new - 3) = matrix_waypoints.col(waypoints_num - 1) -
                                                      vector_v_end * L;
    matrix_waypoints_new.col(waypoints_num_new - 2) = matrix_waypoints.col(waypoints_num - 1);
    matrix_waypoints_new.col(waypoints_num_new - 1) = matrix_waypoints.col(waypoints_num - 1) +
                                                      vector_v_end * L;

    // resize the total interpolation points matrix
    matrix_spline_points = Eigen::MatrixXd::Zero(2, interpolation_num * (waypoints_num_new - 3));
    matrix_spline_points_v = Eigen::MatrixXd::Zero(2, interpolation_num * (waypoints_num_new - 3));
    matrix_spline_points_a = Eigen::MatrixXd::Zero(2, interpolation_num * (waypoints_num_new - 3));

    // compute the interpolation points for each segment
    for (int i = 0; i < waypoints_num_new - 3; i++)
    {
        // y = B1 * C1 + B2 * C2 + B3 * C3 + B4 * C4
        matrix_segment_points = matrix_waypoints_new.col(i) * vector_f1.transpose() +
                                matrix_waypoints_new.col(i + 1) * vector_f2.transpose() +
                                matrix_waypoints_new.col(i + 2) * vector_f3.transpose() +
                                matrix_waypoints_new.col(i + 3) * vector_f4.transpose();

        matrix_segment_points_v = matrix_waypoints_new.col(i) * vector_v1.transpose() +
                                  matrix_waypoints_new.col(i + 1) * vector_v2.transpose() +
                                  matrix_waypoints_new.col(i + 2) * vector_v3.transpose() +
                                  matrix_waypoints_new.col(i + 3) * vector_v4.transpose();

        matrix_segment_points_a = matrix_waypoints_new.col(i) * vector_a1.transpose() +
                                  matrix_waypoints_new.col(i + 1) * vector_a2.transpose() +
                                  matrix_waypoints_new.col(i + 2) * vector_a3.transpose() +
                                  matrix_waypoints_new.col(i + 3) * vector_a4.transpose();

        // accumulate the matrix_segment_points into matrix_spline_points
        matrix_spline_points.block(0, i * interpolation_num, 2, interpolation_num) = matrix_segment_points;
        matrix_spline_points_v.block(0, i * interpolation_num, 2, interpolation_num) = matrix_segment_points_v;
        matrix_spline_points_a.block(0, i * interpolation_num, 2, interpolation_num) = matrix_segment_points_a;
    }
}

void CubicBSpline::computeCurvature()
{
    int num = matrix_spline_points.cols();
    matrix_curvature.resize(1, num);
    matrix_theta.resize(1, num);
    matrix_velocity.resize(1, num);

    std::cerr << "the number of spline points is " << matrix_curvature.cols() << std::endl;

    double curvature_max = 0.0;
    double radius_min = 0.0;

    // compute curvature and theta of the spline
    for (int i = 0; i < matrix_curvature.cols(); i++)
    {
        double curvature_numerator = matrix_spline_points_v(0, i) * matrix_spline_points_a(1, i) -
                                     matrix_spline_points_v(1, i) * matrix_spline_points_a(0, i);
        double curvature_denominator = pow(sqrt(pow(matrix_spline_points_v(0, i), 2) + pow(matrix_spline_points_v(1, i), 2)), 3);
        matrix_curvature(0, i) = curvature_numerator / curvature_denominator;

        // find the max curvature / min turning radius of the entire spline
        if (curvature_max < abs(matrix_theta(0, i)))
        {
            curvature_max = abs(matrix_theta(0, i));
        }

        matrix_theta(0, i) = atan2(matrix_spline_points_v(1, i), matrix_spline_points_v(0, i));
    }

    radius_min = 1 / curvature_max;
    std::cerr << "the min turning radius of the spline is " << radius_min << std::endl;

    // // -------------------to create a velocity profile, not used in thesis----------------------
    // // at a given turing radius, the max allowed velocity can be calculated as:
    // v_max = sqrt(mu * g * radius) = sqrt(mu * g / curvature)

    // // the allowable curvature at max defined velocity
    // double curvature_at_max_velocity  = mu * g / pow(velocity_max, 2);
    // // compute the velocity of the spline
    // for (int i = 0; i < matrix_curvature.cols(); i++)
    // {
    //     if (abs(matrix_curvature(0, i)) < curvature_at_max_velocity)
    //     {
    //         matrix_velocity(0, i) = velocity_max;
    //     }
    //     else
    //     {
    //         matrix_velocity(0, i) = sqrt(mu * g / abs(matrix_curvature(0, i)));
    //     }
    // }
    //---------------------------------------------------------------------------------------------

    //--------------------------velocity in decleration phase--------------------------------------
    int decelerate_points_num = int(velocity_max * 10);
    for (int i = 0; i < matrix_curvature.cols(); i++)
    {
        if (i < matrix_curvature.cols() - decelerate_points_num)
        {
            matrix_velocity(0, i) = velocity_max;
        }
        else
        {
            matrix_velocity(0, i) = velocity_max / decelerate_points_num * (matrix_curvature.cols() - i -1);
        }
    }
}

void CubicBSpline::splinePointsCsvSaver()
{
    // add all information into matrix_spline_all
    int num = matrix_spline_points.cols();
    matrix_spline_all.resize(5, num);
    matrix_spline_all.row(0) = matrix_spline_points.row(0);
    matrix_spline_all.row(1) = matrix_spline_points.row(1);
    matrix_spline_all.row(2) = matrix_theta.row(0);
    matrix_spline_all.row(3) = matrix_curvature.row(0);
    matrix_spline_all.row(4) = matrix_velocity.row(0);

    ofstream outputSplineAll;
    outputSplineAll.open("/home/yang/Spline/SplineAll.csv", ios::out);
    Eigen::MatrixXd matrix_spline_all_trans = matrix_spline_all.transpose();
    for (int i = 0; i < matrix_spline_all_trans.rows(); i++)
    {
        for (int j = 0; j < matrix_spline_all_trans.cols(); j++)
        {
            outputSplineAll << matrix_spline_all_trans(i, j);
            if (j < (matrix_spline_all_trans.cols() - 1))
            {
                outputSplineAll << ",";
            }
        }
        if (i < (matrix_spline_all_trans.rows() - 1))
        {
            outputSplineAll << endl;
        }
    }
    outputSplineAll.close();
}
