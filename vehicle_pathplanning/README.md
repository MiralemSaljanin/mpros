# README
This is a package for the trajectory planning of vehicle model.   
The urdf file of ackermann vehicle model can be found in package `vehicle_description`.  
The control relevant nodes can be found in package `vehicle_control` and `vehicle_einspurmodell`.

# Cubic B-Spline   
To test LQR based lateral, we need a test trajectory.
This class use the initial points from `cvs file` to create a trajectory.
Then the calculated trajectory can be published by `waypoints_publisher` in `vehicle_control` package.

But in final simulation, this class is disabled in `main.cpp`. Because we have another `RRTSpline` class to create a trajectory from `RRTStar` path.


# RRT   
## Node Class   
```c++
class Node
{
public:
    /// X coordinate of the node
    double x;
    /// Y coordinate of the node
    double y;
    /// the cost from parent to the node
    double cost;
    /// the index of the parent node
    int parent_index;
};
```

## RRT Class
```c++
class RRT
{
public:
    AlgorithmMethod(); // methods to compute RRT algorithm

    MapMethod(); // methods to deal with map

    ros_subscriber;
    ros_publisher;

    map_variables;

    rrt_parameters;

    vector<Node> tree_list; // the rrt tree to storage the path nodes
};
```

Code for RRT 
```c++
Initialize ros subscriber and publisher in constructor
while the call back function GetOccupancyMap() works
{
    Initialize Start point
    Initialize Goal point

    get the parameter of map from /map topic
    store the grid map in 2D vector grid[][]
    check the occupancy of Start and Goal points

    push back the start point into rrt tree

    set iteration_number to 0
    while iteration_number is less than max_iteration_number
    {
        iteration_number++
        create a random sample_point with SampleRandomPoint()
        find the nearest_node with FindNearestNode()
        create a new_node with CreateNewNode()

        if no collision between nearest_node and new_node !IsPathCollision()
        {
            push back the new_node into tree
            if the new_node is reachable from goal IsNearGoal()
            {
                if no collision between goal_node and new_node !IsPathCollision()
                {
                    cout << path found

                    find the final path from rrt tree with GetFinalPath()
                    publish the final path with PubRRTPath()
                }
            }
        }
    }
}

```

Code for RRT star
```c++
Initialize ros subscriber and publisher in constructor
while the call back function GetOccupancyMap() works
{
    Set Start point
    Set Goal point

    parameters of map <-- map.info
    grid[][] <-- map.data[]
    check the occupancy of Start and Goal points

    rrt_tree[].push_back(start_node)

    set iteration_number to 0
    while iteration_number < max_iteration_number
    {
        iteration_number++
        random_sample_point <-- SampleRandomPoint()
        nearest_node <-- FindNearestNode(rrt_tree, random_sample_point)
        new_node <-- CreateNewNode(nearest_node, random_sample_point)

        if !IsPathCollision(nearest_node, new_node)
        {
            //-------------------------------------------------------------
                neighbor_nodes[] <-- FindNearNodes(rrt_tree, new_node, near_range)
                new_node <-- ChooseParentNode(neighbor_nodes[], new_node)
                rrt_tree[].push_back(new_node)

                neighbor_nodes[] <-- RewireNearNodes(neighbor_nodes[], new_node)
            //-------------------------------------------------------------
            
            if IsNearGoal(goal_node, new_node)
            {
                if !IsPathCollision(goal_node, new_node)
                {
                    print path found
                    path_msg.pose[] <-- GetFinalPath(rrt_tree[])
                    PubRRTPath(path_msg)
                }
            }
        }
    }
}

```