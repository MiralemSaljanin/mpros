/**
 * @file Node.h
 * @author Yang Qiu (st163984@stud.uni-stuttgart.de)
 * @brief Node for RRT path planning
 * @version 0.1
 * @date 2021-03-19
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef _NODE_H_
#define _NODE_H_

#include <iostream>

using namespace std;

/**
 * @brief Node for RRT path planning algorithms
 * 
 */
class Node
{
public:
    /**
     * @brief Construct a new Node object
     * 
     */
    Node();

    virtual ~Node();

    /**
     * @brief Construct a new Node object
     * 
     * @param x x coordinate
     * @param y y coordinate
     * @param cost cost from start to this node
     * @param parent_index parent index of this node
     */
    Node(const double x, const double y, const double cost, const int parent_index);

    /// X coordinate of the node
    double x;
    /// Y coordinate of the node
    double y;
    /// the cost from parent to the node
    double cost;
    /// the index of the parent node
    int parent_index;
};

#endif