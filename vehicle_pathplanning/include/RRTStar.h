/** 
 * @see https://github.com/vss2sn/path_planning
 *      https://github.com/YashTrikannad/f110_rrt_star
 *      https://github.com/AtsushiSakai/PythonRobotics/tree/master/PathPlanning
 * 
 * @attention the start and goal point should set before compile and run
 * 
 * Step for setting start and goal point (same as RRT): 
 * 1. use vehicle_pathplanning.launch and Rviz to visualize the map
 * 2. the start point should be (0,0), where the transform between /world and /map 
 *    is defined in yaml file.
 * 3. use Rviz gui tool "2D Nav Goal" to set the goal position, after you set it
 *    with mouse, the position and orientation can be shown in Terminal
 * 4. set the value in RRTStar.cpp file
 */

#ifndef _RRTSTAR_H_
#define _RRTSTAR_H_

#include <cmath>
#include <iostream>
#include <algorithm>
#include <array>
#include <vector>
#include <random>

#include <time.h>

#include <ros/ros.h>
#include <nav_msgs/OccupancyGrid.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>

#include <Node.h>

using namespace std;

/**
 * @brief RRTStar path planning algorithms
 * 
 */
class RRTStar
{
public:
    // --------------------------------------Same with RRT-----------------------------------------
    /**
     * @brief Construct a new RRT object
     * @param nh ros nodehandel
     */
    RRTStar(ros::NodeHandle *nh);

    virtual ~RRTStar();

    /**
     * @brief Callback function for subscriber, after getting the grid map from topic /map, 
     * do the RRT path finding algorithms
     * @param map_msg map message
     */
    void GetOccupancyMap(const nav_msgs::OccupancyGrid::ConstPtr &map_msg);

    /**
     * @brief Check if the point(x, y) is obstacle or not
     * @param x x coordinate
     * @param y y coordinate
     * @return true: the point is obstacle
     * @return false: the point is free
     */
    bool IsPointObstacle(double x, double y);

    /**
     * @brief Inflate the obstacle in obstacle_inflation_radius
     */
    void InflateObstacle();

    /**
     * @brief Generate a random sample point in the range of whole map
     * @return std::array<double, 2>: (x,y) of the sample point
     */
    array<double, 2> SampleRandomPoint();

    /**
     * @brief Find the nearest node with the sample point
     * @param node_list the rrt tree
     * @param sample_point random sample point (x,y)
     * @return the index of the nearest node
     */
    int FindNearestNode(const std::vector<Node> &node_list, const array<double, 2> &sample_point);

    /**
     * @brief create a new_node with nearest node and sample point
     * @param nearest_node 
     * @param nearest_node_index 
     * @param sample_point 
     * @return new_node 
     */
    Node CreateNewNode(const Node &nearest_node, const int &nearest_node_index, const array<double, 2> &sample_point);

    /**
     * @brief check if there is a obstacle between two node
     * @param nearest_node 
     * @param new_node 
     * @return true: this path is collision
     * @return false: this path is free
     */
    bool IsPathCollision(const Node &nearest_node, const Node &new_node);

    /**
     * @brief Check if the goal_node is reachable from current new node
     * @param new_node 
     * @return true: the goal is reachable from the new_node
     * @return false: the goal is not reachable from the new_node
     */
    bool IsNearGoal(const Node &new_node);

    /**
     * @brief push back the path points back to rrt_path_msg ( nav_msgs::Path)
     * @param last_index the index of the last node in tree (not include goal_node)
     */
    void GetFinalPath(int &last_index);

    /**
     * @brief publish the computed path message rrt_path_msg to /rrt/path topic
     */
    void PubRRTPath();

    // ----------------------------------------RRT Star--------------------------------------------
    /**
     * @brief find the nodes near the new_node
     * @param node_list rrt tree
     * @param new_node 
     * @return vector<int>: the index of this nodes
     */
    vector<int> FindNearNodes(const std::vector<Node> &node_list, const Node &new_node);

    /**
     * @brief find the best parent for the new node
     * @param near_node_index_list the neighbore nodes index
     * @param new_node new_node with cost = 0.0, parent = 0
     * @return the new_node with the index of parent node and cost
     */
    Node ChooseParentNode(const std::vector<int> &near_node_index_list, Node &new_node);

    /**
     * @brief rewire the neighbore nodes
     * @param near_node_index_list the index list 
     * @param new_node new_node with calculated parent and cost
     */
    void RewireNearNodes(const std::vector<int> &near_node_index_list, const Node &new_node);

    // ------------------------------------------Map-----------------------------------------------
    // the following functions are coordinate and index relevant
    /// get the value of grid in (x,y)
    int CoordinateToValue(double x, double y);
    /// compute the col index j from x coordinate
    int XToCol(double x);
    /// compute the row index i from y coordinate
    int YToRow(double y);
    /// compute the coordinate x from col index j
    double ColToX(int j);
    /// compute the coordinate y from row index i
    double RowToY(int i);

    // ------------------------------------------ROS-----------------------------------------------
    // ros relevant publisher, subscriber and msgs
    /// subscriber to get the occupancy grid map from topic /map, callback func: GetOccupancyMap
    ros::Subscriber map_sub;
    /// publisher to publish the path message to /rrt/path topic
    ros::Publisher rrt_star_path_pub;
    /// Path message to be pulished
    nav_msgs::Path rrt_star_path_msg;

    // -------------------------------------Map Parameter-------------------------------------------
    int map_width;                          // the width of map in pixel
    int map_height;                         // the height of map in pixel
    double map_resolution;                  // the resolution of map in pixel, 1 pixel = (map_resolution) meter
    double x_origin;                        // the x offset in meter
    double y_origin;                        // the y offset in meter
    double obstacle_inflation_radius = 1.8; // the radius for the inflation (meter)
    vector<vector<int>> grid;               // 2D vector to storage map, 0(unoccupied/free cell) or 1(obstacle)
    vector<vector<int>> grid_inflation;     // inflate the obstacle with inflation radius, in oder to keep the path away from obstacle

    // -------------------------------------RRT Parameter-------------------------------------------
    int max_rrt_iteration = 100000;        // max iteration for rrt
    int goal_sample_rate = 10;             // probability that the sampling point is the goal point 10%
    double step_size = 2.0;                // the step size each move (meter)
    double goal_tolerance = step_size;     // the tolerance near goal point (meter)
    double start_x;                        // x coordinate of start point
    double start_y;                        // y corrdinate of start point
    double goal_x;                         // x coordinate of goal point
    double goal_y;                         // y corrdinate of goal point
    double search_radius = 5 * step_size; // search radius for RRT*

    // -----------------------------------------TREE-----------------------------------------------
    vector<Node> tree_list; // the tree to storage the rrt nodes

    // -----------------------------------------TIME-----------------------------------------------
    clock_t start, middle, end;
};

#endif