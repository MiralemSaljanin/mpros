/**
 * @file CubicBSpline.h
 * @author Yang Qiu (st163984@stud.uni-stuttgart.de)
 * @brief Using cubic B spline interpolation to get a smooth path from initial waypoints
 * 
 * @see thesis section 4.2.2
 * @attention change the absolute path of csv file before using
 * 
 * Input:
 *  1. CSV file, which storages the initial waypoints(control points)
 *  2. The maximum speed of the vehicle in a straight line
 *  3. Half length of vehicle, to add redundant control points at start and end waypoints
 *  4. Interpolation number between two control points, here 100
 *  5. Direction of the start and end waypoints, here both (1,0)
 * 
 * Output:
 *  1. CSV file, which contains all information about the Spline (x, y, theta, curvature, velocity)
 * 
 * Process:
 *  1. Load the initial waypoints from csv file with function waypointsCsvLoader()
 *  2. Add redundant control points at start and end waypoints
 *  3. Compute the basis function of Cubic B-Spline
 *  4. Compute the spline points position (x,y)
 *  5. Compute the curvature, theta and velocity of each spline point
 *  6. Save the matrix with all information in CSV file
 *  
 * @version 0.1
 * @date 2021-02-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef _CUBICBSPLINE_H
#define _CUBICBSPLINE_H

#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <Eigen/Eigen>
#include <Eigen/LU>

#include <ros/ros.h>

using namespace std;

/**
 * @brief use cubic B-spline to get a smooth path from initial waypoints
 * 
 */
class CubicBSpline
{
public:
    CubicBSpline();

    virtual ~CubicBSpline();

    /// load the initial waypoints from csv file
    void waypointsCsvLoader();

    /// compute the cubic B-Spline
    void computeBSpline();

    /// compute the curvature theta and velocity of each interpolation point
    void computeCurvature();

    /// save spline matrix in csv file
    void splinePointsCsvSaver();

    // the following parameter are vehicle parameter
    /// the max velocity
    double velocity_max = 10.0;
    /// half of the length of vehicle
    double L = 2;
    /// gravity Coefficient
    double g = 9.81;
    /// friction Coefficient
    double mu = 1.0;

    // the following parameter are initial waypoints revelant
    /// number of initial waypoints
    int waypoints_num = 0;
    /// matrix to storage (x,y) of initial waypoints
    Eigen::MatrixXd matrix_waypoints;

    // the following parameter are B-Spline revelant
    // parametrizations of cubic segments, from 0 to 1, step size 0.01,
    /// interpolation points number of one segment
    int interpolation_num = 51;

    // vector at begin and end of waypoints
    Eigen::Vector2d vector_v_begin; // direction of begin waypoint
    Eigen::Vector2d vector_v_end;   // direction of end waypoint

    // basic functions for Cubic B-Spline
    Eigen::VectorXd vector_f1 = Eigen::VectorXd::Zero(interpolation_num); // basic function B1
    Eigen::VectorXd vector_f2 = Eigen::VectorXd::Zero(interpolation_num); // basic function B2
    Eigen::VectorXd vector_f3 = Eigen::VectorXd::Zero(interpolation_num); // basic function B3
    Eigen::VectorXd vector_f4 = Eigen::VectorXd::Zero(interpolation_num); // basic function B4

    // first Derivative of basic functions
    Eigen::VectorXd vector_v1 = Eigen::VectorXd::Zero(interpolation_num); // B'1
    Eigen::VectorXd vector_v2 = Eigen::VectorXd::Zero(interpolation_num); // B'2
    Eigen::VectorXd vector_v3 = Eigen::VectorXd::Zero(interpolation_num); // B'3
    Eigen::VectorXd vector_v4 = Eigen::VectorXd::Zero(interpolation_num); // B'4

    // second Derivative of basic functions
    Eigen::VectorXd vector_a1 = Eigen::VectorXd::Zero(interpolation_num); // B''1
    Eigen::VectorXd vector_a2 = Eigen::VectorXd::Zero(interpolation_num); // B''2
    Eigen::VectorXd vector_a3 = Eigen::VectorXd::Zero(interpolation_num); // B''3
    Eigen::VectorXd vector_a4 = Eigen::VectorXd::Zero(interpolation_num); // B''4

    // matrix to storage spline points of each segment
    Eigen::MatrixXd matrix_segment_points = Eigen::MatrixXd::Zero(2, interpolation_num);
    Eigen::MatrixXd matrix_segment_points_v = Eigen::MatrixXd::Zero(2, interpolation_num);
    Eigen::MatrixXd matrix_segment_points_a = Eigen::MatrixXd::Zero(2, interpolation_num);
    // matrix to storage total spline points
    Eigen::MatrixXd matrix_spline_points;   // position matrix
    Eigen::MatrixXd matrix_spline_points_v; // first Derivative of position matrix
    Eigen::MatrixXd matrix_spline_points_a; // second Derivative of position matrix

    /// matrix to storage curvature of spline points
    Eigen::MatrixXd matrix_curvature;
    /// matrix to storage theta of spline points
    Eigen::MatrixXd matrix_theta;
    /// matrix to storage velocity of spline points
    Eigen::MatrixXd matrix_velocity;

    /// matrix to storage all information (x, y, theta, curvature, velocity)
    Eigen::MatrixXd matrix_spline_all;
};
#endif