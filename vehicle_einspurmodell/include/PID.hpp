#ifndef PID_H
#define PID_H
#pragma once
	
#include <string>
#include <cmath>
#include <iostream>
#include <Eigen/Eigen>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>

#include <vehicle_control/Bahn.h>
#include <vehicle_control/Waypoint.h>

#include <VehicleModel.h>

class PID  
{
	private:

	public:

		PID(VehicleModel &vehicle_model);
		~PID();


	/**
	 * @brief Get the current state of the vehicle
	 * 
	 * @param vehicle_model 
	 */
	void GetState(VehicleModel &vehicle_model);
	
	/**
	 * @brief Callback Fct. Subscribing to /global/bahn to get the Reference Trajectory of the Planning Unit
	 * 
	 * @param global_bahn_msg 
	 */
	void GetGlobalBahn(const vehicle_control::Bahn &global_bahn_msg);

	/**
	 * @brief Computing Error between Reference Trajectory and Current Position
	 * 
	 */
	void ComputeTrajectoryError();

	/**
	 * @brief Feed Forward control for the steering angle
	 * 
	 */
	void ComputeFeedForward();

	/**
	 * @brief Compute lateral and longitudinal control input
	 * 
	 */
	void ComputeControlInput();


	/** Vehicle Params **/
	double g = 0.0;											// gravitation [m/s²]			
	double m = 0.0;											// mass of the vehicle [kg]
	double cf = 0.0;										// corner stiffness front [N/rad]
	double cr = 0.0;										// corner stiffness rear [N/rad]
	double lf = 0.0;										// distance from front wheel to mass center [m]
	double lr = 0.0;										// distance from rear wheel to mass center [m]
	double iz = 0.0;										// rotational inertia around z-axis [kg * m²]
	double max_steering_angle;								// max. steering angle [rad]
	double max_steering_rate;								// max. steering rate [rad/s]
	double vel_ref = 1.7; 									// Velocity Reference [m/s]

	/** Current Vehicle States **/
	double curr_x = 0.0;									// current x pos.
	double curr_y = 0.0;									// current y pos.							
	double curr_vx = 0.0;									// current vel in x-dir.
	double curr_vy = 0.0;									// current vel in y-dir.
	double curr_psi = 0.0;									// current orienation angle
	double curr_psi_dot = 0.0;								// current orientation rate of change

	/** Predicted Vehicle States **/
	double pre_x = 0.0;										// one time step predicted x pos.
	double pre_y = 0.0;										// one time step predicted y pos.
	double pre_vx = 0.0;									// one time step predicted vx
	double pre_vy = 0.0;									// one time step predicted vy
	double pre_psi = 0.0;									// one time step predicted psi
	double pre_psi_dot = 0.0;								// one time step predicted psi_dot

	/** Control Inputs **/
	double steering_angle_feedforword = 0.0;				// Feed Forward Steering [rad]
	double steering_angle_feedback = 0.0;					// Feedback Steering Angle [rad]
	double lateral_steering_angle = 0.0;					// Wheel Steering Angle control input [rad]
	double drive_torque = 0.0;								// Wheel Torque control input [Nm]

	/** Vectors to store Trajectory Information **/
	double max_vel = 0.0;									// Max. velocity [m/s]
	int waypoints_num = 0;									// Number of Waypoints
	double traj_k = 0.0;									// current curvature of trajectory
	double traj_velocity = 0.0;								// current velocity of trajectory
	double ed = 0.0;										// Current position error
	double es = 0.0; 										// Current orientation error
	double epsi = 0.0; 										// Current yaw angle error
	double epsi_dot = 0.0; 									// Current yaw angle error rate

	double ed_dot = 0.0;									// Current position error rate
	double s_dot = 0.0; 									// Current orientation error rate
	double vel_err = 0.0;									// Current velocity error
	bool goal_reached = false; 								// flag to check if goal is reached
	vehicle_control::Waypoint goal_waypoint; 				// goal Waypoint
	vehicle_control::Waypoint curr_waypoint;				// current Waypoint
	double goal_dist;										// current distance to goal node |curr_pos - goal_pos|
	double epsilon = 0.2; 									// 100 cm --- tolerance for goal reached: goal_dist < epsilon



	Eigen::VectorXd vector_traj_X;							// vector to store x coodinate of global path
	Eigen::VectorXd vector_traj_Y;							// vector to store y coodinate of global path
	Eigen::VectorXd vector_traj_Theta;						// vector to store theta of global path
	Eigen::VectorXd vector_traj_Kappa;						// vector to store curvature of global path
	Eigen::VectorXd vector_traj_Velocity;					// vector to store velocity of global path

	/** PID gains for lateral control **/
	// double Kp_lat = 0.3; 								// Constant Gain
	// double Ki_lat = 0.0; 								// Integration Gain
	// double Kd_lat = 0.787; 
	double Kp_lat = 0.5; 									// Constant Gain
	double Ki_lat = 0.0; 									// Integration Gain
	double Kd_lat = 0.587; 									// Differntiation Gain


	/** PID gains for longitudinal control **/
	double Kp_lon = 300.0; 									// Constant Gain
	// double Kp_lon = 1005.0; 								// Constant Gain
	double Ki_lon = 0.0; 									// Integration Gain
	double Kd_lon = 0.0; 									// Differntiation Gain	

};
#endif