/**
* @file VehicleModel.h
*
* @brief create a vehicle model
*
* @author Yang Qiu
*
* @date 05.12.2020
*/

#ifndef _VEHICLEMODEL_H_
#define _VEHICLEMODEL_H_

#include "VehicleParams.h"

#include <cmath>
#include <sstream>

#include <ros/ros.h>

#include <tf2/LinearMath/Quaternion.h>
#include <tf/transform_datatypes.h>
#include <tf/LinearMath/Matrix3x3.h>

#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseStamped.h>

/**
 * @brief create a vehicle model by using parameters from VehicleParams
*/
class VehicleModel
{
public:
	/**
	 * @brief Construct a new Vehicle Model object
	 * 
	 * @param vehicle_params a vehicle parameter object
	 */
	VehicleModel(const VehicleParams &vehicle_params);

	virtual ~VehicleModel();

	/**
	 * @brief Get current position from topic /vehicle/center_pose
	 * 
	 * @param msg pose message
	 */
	void GetPosition(const geometry_msgs::PoseStamped::ConstPtr &msg);

	/**
	 * @brief Get current velocity from topic /vehicle/velocity
	 * 
	 * @param msg velocity message
	 * 
	 * @attention a transmission between world and vehicle coordinate is necessary
	 */
	void GetVelocity(const geometry_msgs::Twist::ConstPtr &msg);

	/**
	 * @brief Predict the status of vehicle in dt_ second
	 * 
	 */
	void ComputePredictState();

	/// gravitational constant
	double g_;
	/// mass of the vehicle
	double mass_;
	/// rotational inertia
	double inertia_;
	/// friction coefficient
	double friction_coeff_;
	/// distance from front axle to mass center
	double distance_f_;
	/// distance from rear axle to mass center
	double distance_r_;
	/// wheel track
	double distance_track_;
	/// corner stiffness of front wheels
	double cornering_stiffness_f_;
	/// corner stiffness of rear wheels
	double cornering_stiffness_r_;
	/// wheel radius
	double wheel_radius_;
	/// max steering angle /rad
	double max_steering_angle_;
	/// max velocity m/s
	double max_velocity_;
	/// max steering rate rad/s
	double max_steering_rate_;

	/// front left wheel steering angle
	double steering_angle_fl_;
	/// front right wheel steering angle
	double steering_angle_fr_;

	/// rear left wheel velocity
	double throttle_rl_;
	/// rear left wheel velocity
	double throttle_rr_;

	/// control time interval
	double dt_;

	/// velocity x in world coordinate
	double global_vX_;
	/// velocity y in world coordinate
	double global_vY_;
	/// total velocity
	double global_v_;
	/// course angle of vehicle
	double course_angle_;
	/// slip angle of vehicle
	double slip_angle_;

	/// position x
	double curr_x_;
	/// position y
	double curr_y_;
	/// velocity vx
	double curr_vx_;
	/// velocity vy
	double curr_vy_;
	/// yaw angle psi
	double curr_psi_;
	/// yaw angle rate psi_dot
	double curr_psi_dot_;

	/// predicted position x
	double pre_x_;
	/// predicted position x
	double pre_y_;
	/// predicted velocity vx
	double pre_vx_;
	/// predicted velocity vx
	double pre_vy_;
	/// predicted yaw angle pre_psi
	double pre_psi_;
	/// predicted yaw angle dot pre_psi_dot
	double pre_psi_dot_;
};

#endif