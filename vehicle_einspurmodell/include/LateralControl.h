/**
* @file LateralControl.h
*
* @brief Implement the LQR based lateral control
*
* Input:
*	1. Parameters from VehicleModel
*	2. Status of vehicle
*	3. Trajectory information
*
* Output:
*	1. Final Steering angel
*
* Control Process:
*	1. Get parameter from VehicleModel
*	2. Get vehicle status from VehicleModel
*	3. Get trajectory information and compute matrix_err and the curvature
*	4. Compute gain matrix_k with discrete LQR
*	5. Compute FeedForword steering angle with matrix_k and curvature
*	6. Compute final steering angle
*
* @author Yang Qiu, University Stuttgart
* Contact: st163984@stud.uni-stuttgart.de
*
* @date 14.12.2020
*/

#ifndef _LATERALCONTROL_H
#define _LATERALCONTROL_H

#include <string>
#include <cmath>
#include <iostream>
#include <Eigen/Eigen>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>

#include <vehicle_control/Bahn.h>
#include <vehicle_control/Waypoint.h>

#include <VehicleModel.h>

/**
 * @brief LQR based lateral controller
 * 
 */
class LateralController
{
public:
	/**
	 * @brief Get parameter from vehicle_model
	 * 
	 * @param vehicle_model vehicle_model object
	 */
	LateralController(VehicleModel &vehicle_model);

	virtual ~LateralController();

	/**
	 * @brief Get the vehicle status from vehicle_model
	 * 
	 * @param vehicle_model vehicle_model object
	 */
	void GetState(VehicleModel &vehicle_model);

	/**
	 * @brief get the trajectory from /global/bahn topic
	 * 
	 * @param global_bahn_msg trajectory message
	 */
	void GetGlobalBahn(const vehicle_control::Bahn &global_bahn_msg);

	/**
	 * @brief compute error matrix 4*1
	 * 
	 */
	void ComputeTrajectoryError();

	// /// compute cornering stiffness with magic formular
	// void ComputeCorneringStiffness();

	/**
	 * @brief initialize matrix A B K R Q for LQR
	 * 
	 */
	void InitMatrix();

	/**
	 * @brief calculate matrix K with Riccati equation
	 * 
	 */
	void ComputeMatrix();

	/**
	 * @brief calculate feedforward angle
	 * 
	 */
	void ComputeFeedForward();

	/**
	 * @brief calculate final steering angle
	 * 
	 */
	void ComputeFinalSteerAngle();

	// the following parameters are vehicle physics related
	/// gravitational constant
	double g = 0.0;
	/// mass of the vehicle
	double m = 0.0;
	/// corner stiffness front
	double cf = 0.0;
	/// corner stiffness rear
	double cr = 0.0;
	/// distance from front wheel to mass center
	double lf = 0.0;
	/// distance from rear wheel to mass center
	double lr = 0.0;
	/// rotational inertia
	double iz = 0.0;
	/// max steering angle /rad
	double max_steering_angle;
	/// max steering rate rad/s
	double max_steering_rate;

	// the following parameters are tire dynamic related
	/// slip angle of front wheel
	double slip_angle_f;
	/// slip angle of rear wheel
	double slip_angle_r;
	/// vertical load of front wheel
	double vertical_tire_load_f;
	/// vertical load of rear wheel
	double vertical_tire_load_r;
	/// lateral force of front tire
	double lateral_tire_force_f;
	/// lateral force of rear tire
	double lateral_tire_force_r;
	/// non-linear corning stiffness
	double Cf;

	// the following parameters are vehicle status related
	/// current position x
	double curr_x = 0.0;
	/// current position y
	double curr_y = 0.0;
	/// current velocity vx
	double curr_vx = 0.0;
	/// current velocity vy
	double curr_vy = 0.0;
	/// current yaw angle curr_psi
	double curr_psi = 0.0;
	/// current yaw angle dot curr_psi_dot
	double curr_psi_dot = 0.0;

	// the following parameters are prediction module related
	/// predicted position x
	double pre_x = 0.0;
	/// predicted position x
	double pre_y = 0.0;
	/// predicted velocity vx
	double pre_vx = 0.0;
	/// predicted velocity vx
	double pre_vy = 0.0;
	/// predicted yaw angle pre_psi
	double pre_psi = 0.0;
	/// predicted yaw angle dot pre_psi_dot
	double pre_psi_dot = 0.0;

	// the following parameters are TrajectoryError module related
	/// number of path points in global path planning
	int waypoints_num = 0;
	/// current curvature of trajectory
	double traj_k = 0.0;
	/// current velocity of trajectory
	double traj_velocity = 0.0;
	/// vector to storage x coodinate of global path
	Eigen::VectorXd vector_traj_X;
	/// vector to storage y coodinate of global path
	Eigen::VectorXd vector_traj_Y;
	/// vector to storage theta of global path
	Eigen::VectorXd vector_traj_Theta;
	/// vector to storage curvature of global path
	Eigen::VectorXd vector_traj_Kappa;
	/// vector to storage velocity of global path
	Eigen::VectorXd vector_traj_Velcity;

	// the following parameters are LQR module related
	/// number of max lqr iterations
	int lqr_max_iteration = 150; // change from 400 to 150
	/// control time interval
	double ts = 0.01;
	/// state weighting coefficient Q11
	double lqr_q1 = 30.0;
	/// state weighting coefficient Q22
	double lqr_q2 = 1.0;
	/// state weighting coefficient Q33
	double lqr_q3 = 1.0;
	/// state weighting coefficient Q44
	double lqr_q4 = 1.0;
	/// control authority weighting coefficient R1
	double lqr_r1 = 20.0;

	// the following parameters are steering angle related
	/// feedforword angle
	double steering_angle_feedforword = 0.0;
	/// feedback angel
	double steering_angle_feedback = 0.0;
	/// final steering angle
	double lateral_steering_angle = 0.0;
	double k3 = 0.0; 

	// the following Matrix are LQR related
	/// error state matrix 4*1
	Eigen::MatrixXd matrix_err = Eigen::MatrixXd::Zero(4, 1);
	/// vehicle state matrix A 4*4
	Eigen::MatrixXd matrix_a = Eigen::MatrixXd::Zero(4, 4);
	/// discrete state matrix A for discrete LQR 4*4
	Eigen::MatrixXd matrix_a_discrete = Eigen::MatrixXd::Zero(4, 4);
	/// vehicle control matrix B 4*1
	Eigen::MatrixXd matrix_b = Eigen::MatrixXd::Zero(4, 1);
	/// discrete control matrix B for discrete LQR 4*1
	Eigen::MatrixXd matrix_b_discrete = Eigen::MatrixXd::Zero(4, 1);
	/// gain matrix 1*4
	Eigen::MatrixXd matrix_k = Eigen::MatrixXd::Zero(1, 4);
	/// state weighting matrix 4*4
	Eigen::MatrixXd matrix_q = Eigen::MatrixXd::Zero(4, 4);
	/// control authority weighting matrix 1*1
	Eigen::MatrixXd matrix_r = Eigen::MatrixXd::Identity(1, 1);
};

#endif