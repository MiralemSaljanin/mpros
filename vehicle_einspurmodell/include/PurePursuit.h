/**
 * @file PurePursuit.h
 * 
 * @author Yang Qiu, University Stuttgart
 * Contact: st163984@stud.uni-stuttgart.de
 * 
 * @brief Implementation of the Pure pursuit path tracking algorithm
 * 
 * @version 0.1
 * 
 * @date 2021-01-29
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#ifndef _PUREPURSUIT_H
#define _PUREPURSUIT_H

#include <string>
#include <cmath>
#include <iostream>
#include <Eigen/Eigen>

#include <ros/ros.h>
#include <std_msgs/String.h>
#include <std_msgs/Header.h>
#include <geometry_msgs/PoseStamped.h>
#include <nav_msgs/Path.h>

#include <vehicle_control/Bahn.h>
#include <vehicle_control/Waypoint.h>

#include <VehicleModel.h>

/**
 * @brief Pure pursuit controller
 * 
 */
class PurePursuit
{
public:
    /**
     * @brief Construct a new Pure Pursuit object by using parameter from vehicle_model
     * 
     * @param vehicle_model 
     */
    PurePursuit(VehicleModel &vehicle_model);

    virtual ~PurePursuit();

    /**
     * @brief Get the vehicle status
     * 
     * @param vehicle_model 
     */
    void GetState(VehicleModel &vehicle_model);

    /**
     * @brief Get the trajectory information
     * 
     * @param global_bahn_msg trajectory message
     */
    void GetGlobalBahn(const vehicle_control::Bahn &global_bahn_msg);

    /**
     * @brief search look ahead point in global trajectory
     * 
     */
    void SearchTargetPoint();

    /**
     * @brief Compute front wheel steering angle
     * 
     */
    void ComputeSteeringAngle();

    // the following parameters are vehicle physics related
    /// wheel base
    double lt = 0.0;
    /// max steering angle /rad
    double max_steering_angle;

    // the following parameters are vehicle status related
    /// current position x
    double curr_x = 0.0;
    /// current position y
    double curr_y = 0.0;
    /// current yaw angle curr_psi
	double curr_psi = 0.0;
    /// predicted position x
    double pre_x = 0.0;
    /// predicted position x
    double pre_y = 0.0;
    /// predicted yaw angle pre_psi
	double pre_psi = 0.0;

    /// number of path points in global path planning
    int waypoints_num = 0;

    // the following parameters are SearchTarget module related
    /// look ahead distance
    double pursuit_dist = 5.0;
    /// x of target point
    double target_x = 0.0;
    /// y of target point
    double target_y = 0.0;

    // the following parameters are ComputeSteeringAngle module related
    /// final steering angle
    double steering_angle = 0.0;

    /// vector to storage x coodinate of global path
    Eigen::VectorXd vector_traj_X;
    /// vector to storage y coodinate of global path
    Eigen::VectorXd vector_traj_Y;
};

#endif