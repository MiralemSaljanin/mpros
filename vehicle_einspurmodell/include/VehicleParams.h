/**
* @file VehicleParams.h
*
* @brief define all vehicle parameters
*
* @author Yang Qiu
*
* @date 05.12.2020
*/
#ifndef _VEHICLEPARAMS_H_
#define _VEHICLEPARAMS_H_

/**
 * @brief define all vehicle parameter
*/
class VehicleParams
{
public:
	VehicleParams();
	virtual ~VehicleParams();

	// gravitational constant
	double g;
	// mass of the vehicle
	double m;

	// rotational inertia
	double inertia;

	// friction coefficient
	double friction_coeff;

	// distance from front axle to mass center
	double distance_f;
	// distance from rear axle to mass center
	double distance_r;
	// wheel track
	double distance_track;

	// Pacejka Tire Params
	double B; 
	double C; 
	double D;

	// corner stiffness of front wheels
	double cornering_stiffness_f;
	// corner stiffness of rear wheels
	double cornering_stiffness_r;
	// wheel radius
	double wheel_radius;

	// max steering angle /rad
	double max_steering_angle;

	// max velocity m/s
	double max_velocity;

	// max steering rate rad/s
	double max_steering_rate;

	// control time interval
	double dt;
};

#endif