# README
This is a package for the lateral control of vehicle model.   
The urdf file of ackermann vehicle model can be found in package vehicle_description.  
The control and localization relevant nodes can be found in package vehicle_control.  

# Node
## main
The node runs in main.cpp.  
By changing variable lateral_control_mode (0 or 1) in main.cpp to choose LQR or Pure Pursuit.  
The main.cpp includes:

### VehicleParams.h
Define all vehicle parameter  

### VehicleModel.h
Create a vehicle model by using parameters from VehicleParams.  
At same time get current position and velocity from topics and compute predicted position and velocity in dt seconds.

### LateralControl.h
Implement the LQR based lateral control.  
Set int lateral_control_mode = 0 in main.cpp.   

### PurePursuit.h
Implementation of the Pure Pursuit path tracking algorithm.  
Set int lateral_control_mode = 1 in main.cpp




