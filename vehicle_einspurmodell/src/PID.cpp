#include "PID.hpp"  
#include <fstream>
#include <sstream>
#include <iostream>

	
PID::PID(VehicleModel &vehicle_model)
{
	m = vehicle_model.mass_;
	cf = vehicle_model.cornering_stiffness_f_;
	cr = vehicle_model.cornering_stiffness_r_;
	lf = vehicle_model.distance_f_;
	lr = vehicle_model.distance_r_;
	iz = vehicle_model.inertia_;
	max_steering_angle = vehicle_model.max_steering_angle_;
	max_steering_rate = vehicle_model.max_steering_rate_;
	g = vehicle_model.g_;	
    max_vel = vehicle_model.max_velocity_;
    goal_waypoint.pose.pose.position.x = std::numeric_limits<int>::max();   // Random Initialization for the goal position
    goal_waypoint.pose.pose.position.y = std::numeric_limits<int>::max();
}
	
PID::~PID()
{
	
}

void PID::GetState(VehicleModel &vehicle_model){
	curr_x = vehicle_model.curr_x_;
	curr_y = vehicle_model.curr_y_;
	curr_vx = vehicle_model.curr_vx_;
	curr_vy = vehicle_model.curr_vy_;
	curr_psi = vehicle_model.curr_psi_;
	curr_psi_dot = vehicle_model.curr_psi_dot_;
	pre_x = vehicle_model.pre_x_;
	pre_y = vehicle_model.pre_y_;
	pre_vx = vehicle_model.pre_vx_;
	pre_vy = vehicle_model.pre_vy_;
	pre_psi = vehicle_model.pre_psi_;
	pre_psi_dot = vehicle_model.pre_psi_dot_;

    std::cerr << "(curr_vx, curr_vy) is : (" << curr_vx << ", " << curr_vy << ")" << std::endl;

}

void PID::GetGlobalBahn(const vehicle_control::Bahn &global_bahn_msg){
    std::ofstream outFile_Result;
	outFile_Result.open("Arena_Trajectory.csv", std::ios::out);

	waypoints_num = global_bahn_msg.waypoints.size();
	std::cerr << "--------------------------------------" << std::endl;
	std::cerr << "get GlobalBahn success" << std::endl;
	std::cerr << "waypoints_num in GetGlobalBahn is " << waypoints_num << std::endl;

    goal_waypoint = global_bahn_msg.waypoints.back();
    std::cerr << "goal waypoint is " << goal_waypoint.pose.pose.position << std::endl;

	// X and Y coordinate,Theta, curvature of the trajectory
	vector_traj_X = Eigen::VectorXd::Zero(waypoints_num, 1);
	vector_traj_Y = Eigen::VectorXd::Zero(waypoints_num, 1);
	vector_traj_Theta = Eigen::VectorXd::Zero(waypoints_num, 1);
	vector_traj_Kappa = Eigen::VectorXd::Zero(waypoints_num, 1);
	vector_traj_Velocity = Eigen::VectorXd::Zero(waypoints_num, 1);
    vehicle_control::Waypoint temp_waypoint;
	for (int i = 0; i < waypoints_num; i++)
	{
		temp_waypoint = global_bahn_msg.waypoints[i];
		vector_traj_X(i) = temp_waypoint.pose.pose.position.x;
		vector_traj_Y(i) = temp_waypoint.pose.pose.position.y;

		tf::Quaternion q(
			temp_waypoint.pose.pose.orientation.x,
			temp_waypoint.pose.pose.orientation.y,
			temp_waypoint.pose.pose.orientation.z,
			temp_waypoint.pose.pose.orientation.w);
		tf::Matrix3x3 m(q);
		double roll, pitch, yaw;
		m.getRPY(roll, pitch, yaw);

		vector_traj_Theta(i) = yaw;
		vector_traj_Kappa(i) = temp_waypoint.curvature;
		vector_traj_Velocity(i) = max_vel;
        // send data to csv file
		outFile_Result  << vector_traj_X(i) << ","
					    << vector_traj_Y(i) << ","
						<< vector_traj_Theta(i) << ","
						<< vector_traj_Kappa(i) << ","
						<< vector_traj_Velocity(i) << ","
                        << temp_waypoint.pose.pose.orientation.x << ","
                        << temp_waypoint.pose.pose.orientation.y << ","
                        << temp_waypoint.pose.pose.orientation.z << ","
						<< temp_waypoint.pose.pose.orientation.w << ","
                        << roll << ","
                        << pitch << ","
                        << yaw << ","
                        << std::endl;
	}

    // close csv file
	outFile_Result.close();
    
}

void PID::ComputeTrajectoryError(){
	// to avoid Eigen error, the size of matrix cannot be 0
	if (waypoints_num == 0)
	{
		waypoints_num = 10000;
	}

	// resize those vector befor access an element
	vector_traj_X.resize(waypoints_num);
	vector_traj_Y.resize(waypoints_num);
	vector_traj_Theta.resize(waypoints_num);
	vector_traj_Kappa.resize(waypoints_num);
	vector_traj_Velocity.resize(waypoints_num);

    // find the match point with shortest distance between (curr_x, curr_y) and trajectory
	// initialize the distance with the first point of trajectory
	double d_min = std::pow(pre_x - vector_traj_X(0), 2) + std::pow(pre_y - vector_traj_Y(0), 2);
	// the index of the point
	int index_min = 0;

	for (int i = 0; i < waypoints_num; i++)
	{
		double d = std::pow(pre_x - vector_traj_X(i), 2) + std::pow(pre_y - vector_traj_Y(i), 2);
		if (d < d_min)
		{
			d_min = d;
			index_min = i;
		}
	}

    /** compute normal and tangent vector of match point **/
	// normal vector of match point
	Eigen::Vector2d vector_normal = Eigen::Vector2d::Zero(2, 1);
	vector_normal(0, 0) = (-std::sin(vector_traj_Theta(index_min)));
	vector_normal(1, 0) = (std::cos(vector_traj_Theta(index_min)));
	// tangent vector of match point
	Eigen::Vector2d vector_tangent = Eigen::Vector2d::Zero(2, 1);
	vector_tangent(0, 0) = (std::cos(vector_traj_Theta(index_min)));
	vector_tangent(1, 0) = (std::sin(vector_traj_Theta(index_min)));

	/** distance vector, from reference trajectory to current position **/
	Eigen::Vector2d vector_distance = Eigen::Vector2d::Zero(2, 1);
	vector_distance(0, 0) = (curr_x - vector_traj_X(index_min));
	vector_distance(1, 0) = (curr_y - vector_traj_Y(index_min));
	// distance error
	ed = vector_normal.transpose() * vector_distance;
	// the arc between match point and projection point
	es = vector_tangent.transpose() * vector_distance;

	// two different assumptions:
	// assumption 1: match point may not be the projection point
	double projection_point_theta = vector_traj_Theta(index_min) + vector_traj_Kappa(index_min) * es;
	// assumption 2: match point is the projection point
	// double projection_point_theta = vector_traj_Theta(index_min);

	// distace err rate
	ed_dot = pre_vy * std::cos(pre_psi - projection_point_theta) +
					pre_vx * std::sin(pre_psi - projection_point_theta);

	s_dot = (pre_vx * std::cos(pre_psi - projection_point_theta) -
					pre_vy * std::sin(pre_psi - projection_point_theta)) /
				   (1 - vector_traj_Kappa(index_min) * ed);

	// yaw angle error, use std::sin to solve multi-value problems
	epsi = std::sin(pre_psi - projection_point_theta);
	// yaw angle error rate
	epsi_dot = pre_psi_dot - vector_traj_Kappa(index_min) * s_dot;

	// curvature of match point, later will be used in FeedForwordControl
	traj_k = vector_traj_Kappa(index_min);
	traj_velocity = vector_traj_Velocity(index_min);
}

void PID::ComputeFeedForward()
{
	// compute FeerForword angle,page57,equation 3.12
	// understeer gradient kv
	double kv = lr * m / 2 / cf / (lf + lr) - lf * m / 2 / cr / (lf + lr);
	double ay = pre_vx * pre_vx * traj_k;
	double k3 = 1.0;

	double feedforword_part1 = traj_k * (lf + lr);
	double feedforword_part2 = kv * ay;
	double feedforword_part3 = lr * traj_k - lf * m * pre_vx * pre_vx * traj_k / 2 / cr / (lf + lr);

	steering_angle_feedforword = feedforword_part1 + feedforword_part2 - k3 * feedforword_part3;
	// or use equation 3.11
	// steering_angle_feedforword = traj_k * (lf + lr - lr * k3 +
	// (m * pre_vx * pre_vx / (lf + lr)) * (lr / 2 / cf  - lf / 2 / cr + (lf / 2 / cr) * k3));

	std::cerr << "steering_angle_feedforword is " << steering_angle_feedforword << std::endl;
}

void PID::ComputeControlInput()
{
    // Current Distance to goal 
    goal_dist = std::sqrt( std::pow(goal_waypoint.pose.pose.position.x - curr_x, 2) + 
                           std::pow(goal_waypoint.pose.pose.position.y - curr_y, 2) );

    std::cerr << "goal position: " << goal_waypoint.pose.pose.position << std::endl;
    std::cerr << "current position: " << curr_x << ", " << curr_y << std::endl;
    std::cerr << "distance to goal is: " << goal_dist << std::endl;
    // int flag = std::getc(stdin); //pause for tab if needed

    if(goal_dist > epsilon){
        // compute Feedbacks using (as of now) P-controller
        std::cerr << "current position error: " << ed << std::endl;
        steering_angle_feedback = Kp_lat * ed + Kd_lat * ed_dot; 
        // steering_angle_feedback = Kp_lat * epsi + Kd_lat * epsi_dot; 
        vel_err = std::abs(curr_vx - vel_ref);
        drive_torque = std::min(300.0, Kp_lon * vel_err);

        // compute final steering angle
        lateral_steering_angle = -steering_angle_feedback + steering_angle_feedforword;
        // lateral_steering_angle = -steering_angle_feedback;

        // steering angle range constrain (-max_steering_angle, max_steering_angle)
        if (lateral_steering_angle > 2*max_steering_angle)
        {
            lateral_steering_angle = 2*max_steering_angle;
        }
        if (lateral_steering_angle < -2*max_steering_angle)
        {
            lateral_steering_angle = -2*max_steering_angle;
        }
    }else{
        goal_reached = true; 
		lateral_steering_angle = 0.0;
        drive_torque = 0.0;       
    }

	// // decelerate phase
	// if(goal_dist < epsilon)
	// {
    //     goal_reached = true; 
	// 	lateral_steering_angle = 0.0;
    //     drive_torque = 0.0;
	// }

	std::cerr << "lateral_steering_angle is " << lateral_steering_angle << std::endl;
	std::cerr << "drive_torque is " << drive_torque << std::endl;
	std::cerr << "--------------------------------------" << std::endl;
}