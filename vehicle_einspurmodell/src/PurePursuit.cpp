/**
 * @file PurePursuit.cpp
 * 
 * @author Yang Qiu, University Stuttgart
 * Contact: st163984@stud.uni-stuttgart.de
 * 
 * @brief Implementation of the Pure Pursuit path tracking algorithm
 * 
 * @version 0.1
 * 
 * @date 2021-01-29
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include <VehicleModel.h>
#include <PurePursuit.h>

using namespace Eigen;

PurePursuit::PurePursuit(VehicleModel &vehicle_model)
{
    lt = vehicle_model.distance_track_;
    max_steering_angle = vehicle_model.max_steering_angle_;
}

PurePursuit::~PurePursuit(){};

void PurePursuit::GetState(VehicleModel &vehicle_model)
{
    // vehicle center position
    curr_x = vehicle_model.curr_x_;
    curr_y = vehicle_model.curr_y_;
    curr_psi = vehicle_model.curr_psi_;
    pre_x = vehicle_model.pre_x_;
    pre_y = vehicle_model.pre_y_;
    pre_psi = vehicle_model.pre_psi_;

    // // vehicle rear axle position
    // curr_x = curr_x - std::cos(curr_psi) * lt;
    // curr_y = curr_y - std::sin(curr_psi) * lt;
    // pre_x = pre_x - std::cos(curr_psi) * lt;
    // pre_y = pre_y - std::sin(curr_psi) * lt;
}

void PurePursuit::GetGlobalBahn(const vehicle_control::Bahn &global_bahn_msg)
{
    waypoints_num = global_bahn_msg.waypoints.size();

    std::cerr << "get GlobalBahn success" << std::endl;
    std::cerr << "waypoints_num in GetGlobalBahn is " << waypoints_num << std::endl;
    std::cerr << "--------------------------------------" << std::endl;
    
    vector_traj_X = VectorXd::Zero(waypoints_num, 1);
    vector_traj_Y = VectorXd::Zero(waypoints_num, 1);

    vehicle_control::Waypoint temp_waypoint;

    for (int i = 0; i < waypoints_num; i++)
    {
        temp_waypoint = global_bahn_msg.waypoints[i];
        vector_traj_X(i) = temp_waypoint.pose.pose.position.x;
        vector_traj_Y(i) = temp_waypoint.pose.pose.position.y;
    }
}

void PurePursuit::SearchTargetPoint()
{
    // to avoid Eigen error, the size of matrix cannot be 0
    if (waypoints_num == 0)
    {
        waypoints_num = 10000;
    }
    // resize those vector befor access an element
    vector_traj_X.resize(waypoints_num);
    vector_traj_Y.resize(waypoints_num);

    // find the match point with shortest distance between (curr_x, curr_y) and global path
    // the index of the point
    int index_min = 0;
    // initialize the distance with the first point of trajectory
    double d_min = std::pow(curr_x - vector_traj_X(0), 2) + std::pow(curr_y - vector_traj_Y(0), 2);
    for (int i = 0; i < waypoints_num; i++)
    {
        double d = std::pow(curr_x - vector_traj_X(i), 2) + std::pow(curr_y - vector_traj_Y(i), 2);
        if (d < d_min)
        {
            d_min = d;
            index_min = i;
        }
    }

    // find the target point
    int index_target = index_min;
    // look ahead distance accumulator
    double distance_temp = 0.0;

    for (int i = index_min; i < waypoints_num - 1; i++)
    {
        double dx = vector_traj_X(i + 1) - vector_traj_X(i);
        double dy = vector_traj_Y(i + 1) - vector_traj_Y(i);
        distance_temp += hypot(dx, dy);
        index_target = i;
        if (distance_temp > pursuit_dist)
            break;
    }

    target_x = vector_traj_X(index_target);
    target_y = vector_traj_Y(index_target);

    std::cerr << "index_min is " << index_min << std::endl;
    std::cerr << "index_target is " << index_target << std::endl;
}

void PurePursuit::ComputeSteeringAngle()
{
    double dist_diff_x = target_x - pre_x;
    double dist_diff_y = target_y - pre_y;
    // angle difference between target point and rear center point
    double alpha = std::atan2(dist_diff_y, dist_diff_x) - pre_psi;
    // position difference between target point and rear center point
    double dist = hypot(dist_diff_x, dist_diff_y);

    if (dist > 0.5)
    {
        steering_angle = std::atan2(2 * lt * std::sin(alpha), dist);
    }
    else
    {
        steering_angle = 0.0;
    }

    if (steering_angle > max_steering_angle)
    {
        steering_angle = max_steering_angle;
    }
    if (steering_angle < -max_steering_angle)
    {
        steering_angle = -max_steering_angle;
    }

    std::cerr << "alpha is " << alpha << std::endl;
    std::cerr << "dist is " << dist << std::endl;
    std::cerr << "steering_angle is " << steering_angle << std::endl;
    std::cerr << "-----------------------------------------" << std::endl;
}

