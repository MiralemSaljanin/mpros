#include "Stanley.hpp"  
#include <fstream>
#include <sstream>
#include <iostream>
#include "tf/tf.h"
#include "tf/transform_listener.h"
#include "tf2_ros/buffer.h"
#include "tf2_ros/transform_listener.h"
#include "tf2_geometry_msgs/tf2_geometry_msgs.h"
	
Stanley::Stanley(VehicleModel &vehicle_model)
{
	m = vehicle_model.mass_;
	cf = vehicle_model.cornering_stiffness_f_;
	cr = vehicle_model.cornering_stiffness_r_;
	lf = vehicle_model.distance_f_;
	lr = vehicle_model.distance_r_;
	iz = vehicle_model.inertia_;
	max_steering_angle = vehicle_model.max_steering_angle_;
	max_steering_rate = vehicle_model.max_steering_rate_;
	g = vehicle_model.g_;	
    max_vel = vehicle_model.max_velocity_;
    goal_waypoint.pose.pose.position.x = std::numeric_limits<int>::max();   // Random Initialization for the goal position
    goal_waypoint.pose.pose.position.y = std::numeric_limits<int>::max();
}
	
Stanley::~Stanley()
{
	
}

void Stanley::GetState(VehicleModel &vehicle_model){
	curr_x = vehicle_model.curr_x_;
	curr_y = vehicle_model.curr_y_;
	curr_vx = vehicle_model.curr_vx_;
	curr_vy = vehicle_model.curr_vy_;
	curr_psi = vehicle_model.curr_psi_;
	curr_psi_dot = vehicle_model.curr_psi_dot_;
	pre_x = vehicle_model.pre_x_;
	pre_y = vehicle_model.pre_y_;
	pre_vx = vehicle_model.pre_vx_;
	pre_vy = vehicle_model.pre_vy_;
	pre_psi = vehicle_model.pre_psi_;
	pre_psi_dot = vehicle_model.pre_psi_dot_;

    std::cerr << "(curr_vx, curr_vy) is : (" << curr_vx << ", " << curr_vy << ")" << std::endl;

}

void Stanley::GetGlobalBahn(const vehicle_control::Bahn &global_bahn_msg){
    std::ofstream outFile_Result;
	outFile_Result.open("Arena_Trajectory.csv", std::ios::out);

	waypoints_num = global_bahn_msg.waypoints.size();
	std::cerr << "--------------------------------------" << std::endl;
	std::cerr << "get GlobalBahn success" << std::endl;
	std::cerr << "waypoints_num in GetGlobalBahn is " << waypoints_num << std::endl;

	vehicle_control::Bahn temp_bahn_msg;
	temp_bahn_msg.waypoints = global_bahn_msg.waypoints;

	tf2_ros::Buffer buffer;
	tf2_ros::TransformListener listener(buffer);

	for(int i = 0; i < temp_bahn_msg.waypoints.size(); i++)
	{
		geometry_msgs::PoseStamped pose_map, pose_world;
		pose_map.header.frame_id = "map";
		pose_map.header.stamp = ros::Time(0);
		pose_map.pose = temp_bahn_msg.waypoints[i].pose.pose;

		pose_world = buffer.transform(pose_map, "world", ros::Duration(1.0));
		temp_bahn_msg.waypoints[i].pose.pose = pose_world.pose;
	}
	

    goal_waypoint = temp_bahn_msg.waypoints.back();
    std::cerr << "goal waypoint is " << goal_waypoint.pose.pose.position << std::endl;

	// X and Y coordinate,Theta, curvature of the trajectory
	vector_traj_X = Eigen::VectorXd::Zero(waypoints_num, 1);
	vector_traj_Y = Eigen::VectorXd::Zero(waypoints_num, 1);
	vector_traj_Theta = Eigen::VectorXd::Zero(waypoints_num, 1);
	vector_traj_Kappa = Eigen::VectorXd::Zero(waypoints_num, 1);
	vector_traj_Velocity = Eigen::VectorXd::Zero(waypoints_num, 1);
    vehicle_control::Waypoint temp_waypoint;

	for (int i = 0; i < waypoints_num; i++)
	{
		temp_waypoint = temp_bahn_msg.waypoints[i];

		vector_traj_X(i) = temp_waypoint.pose.pose.position.x;
		vector_traj_Y(i) = temp_waypoint.pose.pose.position.y;
        if(i>620){
            temp_waypoint.twist.twist.linear.x = std::max(0.0, vector_traj_Velocity(i-1) - 0.03); //Decrease Velocity in 0.03 m/s steps
            // temp_waypoint.twist.twist.linear.x = 0.0;
        }

		tf::Quaternion q(
			temp_waypoint.pose.pose.orientation.x,
			temp_waypoint.pose.pose.orientation.y,
			temp_waypoint.pose.pose.orientation.z,
			temp_waypoint.pose.pose.orientation.w);
		tf::Matrix3x3 m(q);
		double roll, pitch, yaw;
		m.getRPY(roll, pitch, yaw);

		vector_traj_Theta(i) = yaw;
		vector_traj_Kappa(i) = temp_waypoint.curvature;
        vector_traj_Velocity(i) = temp_waypoint.twist.twist.linear.x;
        // send data to csv file
		outFile_Result  << vector_traj_X(i) << ","
					    << vector_traj_Y(i) << ","
						<< vector_traj_Theta(i) << ","
						<< vector_traj_Kappa(i) << ","
						<< vector_traj_Velocity(i) << ","
                        << std::endl;
	}


    // close csv file
	outFile_Result.close();
    
}

void Stanley::ComputeTrajectoryError(){
	// to avoid Eigen error, the size of matrix cannot be 0
	if (waypoints_num == 0)
	{
		waypoints_num = 10000;
	}

	// resize those vector befor access an element
	vector_traj_X.resize(waypoints_num);
	vector_traj_Y.resize(waypoints_num);
	vector_traj_Theta.resize(waypoints_num);
	vector_traj_Kappa.resize(waypoints_num);
	vector_traj_Velocity.resize(waypoints_num);

    // find the match point with shortest distance between (curr_x, curr_y) and trajectory
	// initialize the distance with the first point of trajectory
	double d_min = std::pow(pre_x - vector_traj_X(0), 2) + std::pow(pre_y - vector_traj_Y(0), 2);
	// the index of the point
	int index_min = 0;

	for (int i = 0; i < waypoints_num; i++)
	{
		double d = std::pow(pre_x - vector_traj_X(i), 2) + std::pow(pre_y - vector_traj_Y(i), 2);
		if (d < d_min)
		{
			d_min = d;
			index_min = i;
		}
	}

    /** compute normal and tangent vector of match point **/
	// normal vector of match point
	Eigen::Vector2d vector_normal = Eigen::Vector2d::Zero(2, 1);
	vector_normal(0, 0) = (-std::sin(vector_traj_Theta(index_min)));
	vector_normal(1, 0) = (std::cos(vector_traj_Theta(index_min)));
	// tangent vector of match point
	Eigen::Vector2d vector_tangent = Eigen::Vector2d::Zero(2, 1);
	vector_tangent(0, 0) = (std::cos(vector_traj_Theta(index_min)));
	vector_tangent(1, 0) = (std::sin(vector_traj_Theta(index_min)));

	/** distance vector, from reference trajectory to current position **/
	Eigen::Vector2d vector_distance = Eigen::Vector2d::Zero(2, 1);
	vector_distance(0, 0) = (curr_x - vector_traj_X(index_min));
	vector_distance(1, 0) = (curr_y - vector_traj_Y(index_min));
	// distance error
	ed = vector_normal.transpose() * vector_distance;
	// the arc between match point and projection point
	es = vector_tangent.transpose() * vector_distance;

	// two different assumptions:
	// assumption 1: match point may not be the projection point
	double projection_point_theta = vector_traj_Theta(index_min) + vector_traj_Kappa(index_min) * es;
	// assumption 2: match point is the projection point
	// double projection_point_theta = vector_traj_Theta(index_min);

	// distace err rate
	ed_dot = pre_vy * std::cos(pre_psi - projection_point_theta) +
					pre_vx * std::sin(pre_psi - projection_point_theta);

	s_dot = (pre_vx * std::cos(pre_psi - projection_point_theta) -
					pre_vy * std::sin(pre_psi - projection_point_theta)) /
				   (1 - vector_traj_Kappa(index_min) * ed);

	// yaw angle error, use std::sin to solve multi-value problems
	epsi = std::sin(pre_psi - projection_point_theta);
	// yaw angle error rate
	epsi_dot = pre_psi_dot - vector_traj_Kappa(index_min) * s_dot;

	// curvature of match point, later will be used in FeedForwordControl
	traj_k = vector_traj_Kappa(index_min);
	traj_velocity = vector_traj_Velocity(index_min);
    traj_r = vector_traj_Kappa(index_min) * s_dot;
}

void Stanley::ComputeFeedForward()
{
	// compute FeerForword angle, page57,equation 3.12
	// understeer gradient kv
	double kv = lr * m / 2 / cf / (lf + lr) - lf * m / 2 / cr / (lf + lr);
	double ay = pre_vx * pre_vx * traj_k;
	double k3 = 1.2;

	// double feedforword_part1 = traj_k * (lf + lr);
	// double feedforword_part2 = kv * ay;
	// double feedforword_part3 = lr * traj_k - lf * m * pre_vx * pre_vx * traj_k / 2 / cr / (lf + lr);

	// steering_angle_feedforword = feedforword_part1 + feedforword_part2 - k3 * feedforword_part3;
	// or use equation 3.11
	steering_angle_feedforword = traj_k * (lf + lr - lr * k3 +
	(m * pre_vx * pre_vx / (lf + lr)) * (lr / 2 / cf  - lf / 2 / cr + (lf / 2 / cr) * k3));

	std::cerr << "steering_angle_feedforword is " << steering_angle_feedforword << std::endl;
}

void Stanley::ComputeControlInput()
{
    // Current Distance to goal 
    goal_dist = std::sqrt( std::pow(goal_waypoint.pose.pose.position.x - curr_x, 2) + 
                           std::pow(goal_waypoint.pose.pose.position.y - curr_y, 2) );

    double v = std::sqrt(curr_vx * curr_vx + curr_vy * curr_vy);
    double psi_ss = m*v*traj_r/(2*cf);
    double d_yaw = Kd_yaw*epsi_dot;                                                         // yaw damping
    double ce = std::atan2(K*ed, Ks+v);     
    double steering_temp = epsi + ce + d_yaw;
    double d_steer = Kd_steer*(steering_temp - prev_lateral_steering_angle);            	// steer damping
    steering_angle_feedback = steering_temp; // + d_steer;
    // std::cerr << "lateral_steering_angle feedback is " << steering_angle_feedback << std::endl;
	std::cerr << "goal_dist is: " << goal_dist << std::endl;

        steering_angle_feedback = steering_angle_feedback;                              // compute Feedbacks using (as of now) P-controller
        drive_torque = std::min(max_drive_torque, std::max(-max_drive_torque, -K_p_lon * (v - traj_velocity)));

        // compute final steering angle
        lateral_steering_angle = -steering_angle_feedback + steering_angle_feedforword;
        // lateral_steering_angle = -steering_angle_feedback;

        // steering angle range constrain (-max_steering_angle, max_steering_angle)
        if (lateral_steering_angle > 2*max_steering_angle)
        {
            lateral_steering_angle = 2*max_steering_angle;
        }
        if (lateral_steering_angle < -2*max_steering_angle)
        {
            lateral_steering_angle = -2*max_steering_angle;
        }
		if (goal_dist < 2.5)
		{
			std::cerr << "goal_dist is: " << goal_dist << std::endl;
			drive_torque = 0.0; 
		}

    // prev_lateral_steering_angle = steering_angle_feedback;

	std::cerr << "lateral_steering_angle is " << lateral_steering_angle << std::endl;
	std::cerr << "drive_torque is " << drive_torque << std::endl;
	std::cerr << "--------------------------------------" << std::endl;
}