/**
 * @file main.cpp
 * @author Yang Qiu, st163984@stud.uni-stuttgart.de
 * @brief ros node to control the motion of vehicle model
 * @version 0.1
 * @date 2021-01-29
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <time.h>

#include <matplotlibcpp.h>
#include <VehicleParams.h>
#include <VehicleModel.h>
#include <LateralControl.h>
#include <PurePursuit.h>
#include <PID.hpp>
#include <Stanley.hpp>

#include "ros/ros.h"
#include "tf/tf.h"

using namespace std;
namespace plt = matplotlibcpp; 

int main(int argc, char **argv)
{
	// init node
	ros::init(argc, argv, "motionControl_node");

	ros::NodeHandle nh;

	// create a vehicle parameter object
	VehicleParams vehicle_parameter;

	// create a vehicle model object to get parameter and vehicle status
	VehicleModel vehicle_model(vehicle_parameter);

	// lateral control mode: -1(PID), 0(LQR), 1(Pure Pursuit), 2(Stanley)
	int lateral_control_mode = 2;

	LateralController lateral_control(vehicle_model);
	PurePursuit pure_pursuit(vehicle_model);
	PID pid_controller(vehicle_model);
	Stanley stanley_controller(vehicle_model);

	std::vector<double> steering_f; 
	std::vector<double> steering_r; 
	std::vector<double> drive; 
	std::vector<double> velocity_vec; 
	std::vector<double> velocity_ref_vec; 
	std::vector<double> time_vec; 


	/// subscriber to get current position, callback func: VehicleModel::GetPosition()
	ros::Subscriber pose_sub;
	/// subscriber to get current velocity, callback func: VehicleModel::GetVelocity()
	ros::Subscriber vehicle_sub;
	/// subscriber to get global bahn information, callback func: LateralController::GetGlobalBahn
	ros::Subscriber global_bahn_sub;

	/// publisher to publish control information to the topic /vehicle/cmd_vel
	ros::Publisher cmd_vel_pub;

	/// control message
	geometry_msgs::Twist cmd_vel_msg;

	pose_sub = nh.subscribe("/vehicle/center_pose", 100, &VehicleModel::GetPosition, &vehicle_model);
	vehicle_sub = nh.subscribe("/vehicle/velocity", 100, &VehicleModel::GetVelocity, &vehicle_model);

	cmd_vel_pub = nh.advertise<geometry_msgs::Twist>("/vehicle/cmd_vel", 100);

	if (lateral_control_mode == 0)
	{
		global_bahn_sub = nh.subscribe("/global/bahn", 100, &LateralController::GetGlobalBahn, &lateral_control);
	}
	else if (lateral_control_mode == 1)
	{
		global_bahn_sub = nh.subscribe("/global/bahn", 100, &PurePursuit::GetGlobalBahn, &pure_pursuit);
	}
	else if (lateral_control_mode == 2)
	{
		global_bahn_sub = nh.subscribe("/global/bahn", 100, &Stanley::GetGlobalBahn, &stanley_controller);
	}
	else
	{
		global_bahn_sub = nh.subscribe("/global/bahn", 100, &PID::GetGlobalBahn, &pid_controller);
	}

	// save the result in csv file
	// (x, y, position_err, orientation_err, steering_angle, vx, vy, v_path, kappa, slip_f, slip_r)
	ofstream outFile_Result;
	outFile_Result.open("ControlResult.csv", ios::out);

	// compute the time for one loop to set correct publish rate
	clock_t start, middle, end;

	int it = 0.0; 
	double vel_x = 0.0; //to store current velocity
	double vel_ref_x = 0.0; //to store current ref velocity
	double K3 = 0.0;

	// loop rate
	ros::Rate loop_rate(50);
	while (ros::ok())
	{
		time_vec.push_back(it*vehicle_model.dt_);
		// time to start the calculate
		start = clock();

		// compute predicted vehicle status after getting position and velocity information
		vehicle_model.ComputePredictState();

		if (lateral_control_mode == 0) // LQR
		{
			// get the current and predicted status from vehicle_model
			lateral_control.GetState(vehicle_model);
			// compute error matrix after getting global path
			lateral_control.ComputeTrajectoryError();
			// initialize LQR matrix
			lateral_control.InitMatrix();
			// compute LQR matrix K
			lateral_control.ComputeMatrix();
			// compute feedforword angle
			lateral_control.ComputeFeedForward();
			// compute final steering angle
			lateral_control.ComputeFinalSteerAngle();

			// publish control message
			// set the velocity
			// cmd_vel_msg.linear.x =12;
			cmd_vel_msg.linear.x = lateral_control.traj_velocity;
			cmd_vel_msg.linear.x = 200.0;
			cmd_vel_msg.angular.z = lateral_control.lateral_steering_angle;
			K3 = lateral_control.k3;
		}
		else if (lateral_control_mode == 1) // Pure Pursuit
		{
			// get the current and predicted status from vehicle_model
			pure_pursuit.GetState(vehicle_model);
			// find look ahead point
			pure_pursuit.SearchTargetPoint();
			// compute final steering angle
			pure_pursuit.ComputeSteeringAngle();

			cmd_vel_msg.linear.x = 5;
			cmd_vel_msg.angular.z = pure_pursuit.steering_angle;
		}
		else if (lateral_control_mode == 2) // Stanley
		{
			// get the current and predicted status from vehicle_model
			stanley_controller.GetState(vehicle_model);
			// compute error matrix after getting global path
			stanley_controller.ComputeTrajectoryError();
			// find look ahead point
			stanley_controller.ComputeFeedForward();
			// compute final steering angle
			stanley_controller.ComputeControlInput();

			// publish control message
			// cmd_vel_msg.linear.x = pid_controller.traj_velocity;
			
			cmd_vel_msg.linear.x = stanley_controller.drive_torque;
			cmd_vel_msg.angular.z = stanley_controller.lateral_steering_angle;
		
			vel_x = stanley_controller.curr_vx;
			vel_ref_x = stanley_controller.traj_velocity;
		}
		else // PID
		{
			// get the current and predicted status from vehicle_model
			pid_controller.GetState(vehicle_model);
			// compute error matrix after getting global path
			pid_controller.ComputeTrajectoryError();
			// find look ahead point
			pid_controller.ComputeFeedForward();
			// compute final steering angle
			pid_controller.ComputeControlInput();

			// publish control message
			// cmd_vel_msg.linear.x = pid_controller.traj_velocity;
			cmd_vel_msg.linear.x = pid_controller.drive_torque;
			cmd_vel_msg.angular.z = pid_controller.lateral_steering_angle;
			vel_x = pid_controller.curr_vx;


		}
		// publish control command
		cmd_vel_pub.publish(cmd_vel_msg);

		// time after publishing the message
		middle = clock();

		steering_f.push_back(0.5* cmd_vel_msg.angular.z);
		steering_r.push_back(-0.5* cmd_vel_msg.angular.z);
		drive.push_back(cmd_vel_msg.linear.x);
		velocity_vec.push_back(vel_x);
		velocity_ref_vec.push_back(vel_ref_x);

		// send data to csv file
		// outFile_Result << vehicle_model.curr_x_ << ","
		// 				   << vehicle_model.curr_y_ << ","
		// 				   << lateral_control.matrix_err(0, 0) << ","
		// 				   << lateral_control.matrix_err(2, 0) << ","
		// 				   << lateral_control.lateral_steering_angle << ","
		// 				   << vehicle_model.curr_vx_ << ","
		// 				   << vehicle_model.curr_vy_ << ","
		// 				   << lateral_control.traj_velocity << ","
		// 				   << lateral_control.traj_k << ","
		// 				   << lateral_control.slip_angle_f << ","
		// 				   << lateral_control.slip_angle_r
		// 				   << endl;
		// time after saving data in csv file
		end = clock();

		ros::spinOnce();
		loop_rate.sleep();

		// print the time to calculate LQR steering angle and time to save data
		cout << "time to solve control signal = " << double(middle - start) / CLOCKS_PER_SEC << "s" << endl;
		// cout << "time to save data in csv file = " << double(end - middle) / CLOCKS_PER_SEC << "s" << endl;

		it += 1.0;
	}

	std::cerr << "time vec size: " << time_vec.size();
	std::cerr << "beginning time: " << time_vec.front();
	std::cerr << "end time: " << time_vec.back(); 

	// plot
    plt::figure(1);
    // Set the "super title"
    plt::suptitle("Control Inputs");
    plt::subplot(3, 1, 1);
	plt::plot(time_vec, steering_f,"tab:blue");
	plt::plot(time_vec, steering_r,"tab:red");
    plt::title("Steering Input over Time");
    plt::subplot(3, 1, 2);
    plt::plot(time_vec, drive, "tab:orange");
    plt::title("Drive Input over Time");
	plt::subplot(3, 1, 3);
    plt::plot(time_vec, velocity_vec, "tab:red");
	plt::plot(time_vec, velocity_ref_vec, "tab:blue");
    plt::title("Velocity over Time");
	// Show plots
	plt::show();

	// close csv file
	outFile_Result.close();

	return 0;
}