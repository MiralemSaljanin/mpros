/*
 * Filename: /home/miralem/catkin_ws/src/mpros/vehicle_einspurmodell/src/VehicleParams.cpp
 * Path: /home/miralem/catkin_ws/src/mpros/vehicle_einspurmodell/src
 * Created Date: Friday, February 18th 2022, 9:54:17 am
 * Author: Miralem Saljanin
 * 
 * Copyright (c) 2022 Institute of Automotive Engineering, University of Stuttgart
 */

#include "VehicleParams.h"

/**
 * @brief initialize the parameters
*/
VehicleParams::VehicleParams()
{
	g = 9.81;
	m = 1000;

	inertia = 1100.2;

	friction_coeff = 1.0;

	distance_f = 2.1;
	distance_r = 2.1;
	distance_track = 2;

	cornering_stiffness_f = 90000;
	cornering_stiffness_r = 90000;

	wheel_radius = 0.3515;

	max_steering_angle = 0.52;
	max_velocity = 2;

	//Pacejka Params
	B = 7.0; 
	C = 1.2; 
	D = 2000.0;

	// means it takes 1 second to go from the far left to the far right 
	max_steering_rate = 0.52; 

	dt = 0.01;
}

VehicleParams::~VehicleParams()
{
}