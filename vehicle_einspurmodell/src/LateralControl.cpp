/**
* @file LateralControl.cpp
*
* @brief Implement the LQR based lateral control
*
* @author Yang Qiu, University Stuttgart
* Contact: st163984@stud.uni-stuttgart.de
*
* @date 14.12.2020
*/

#include <VehicleModel.h>
#include <LateralControl.h>

using namespace Eigen;

LateralController::LateralController(VehicleModel &vehicle_model)
{
	m = vehicle_model.mass_;
	cf = vehicle_model.cornering_stiffness_f_;
	cr = vehicle_model.cornering_stiffness_r_;
	lf = vehicle_model.distance_f_;
	lr = vehicle_model.distance_r_;
	iz = vehicle_model.inertia_;
	max_steering_angle = vehicle_model.max_steering_angle_;
	max_steering_rate = vehicle_model.max_steering_rate_;
	g = vehicle_model.g_;
};

LateralController::~LateralController(){};

void LateralController::GetState(VehicleModel &vehicle_model)
{
	curr_x = vehicle_model.curr_x_;
	curr_y = vehicle_model.curr_y_;
	curr_vx = vehicle_model.curr_vx_;
	curr_vy = vehicle_model.curr_vy_;
	curr_psi = vehicle_model.curr_psi_;
	curr_psi_dot = vehicle_model.curr_psi_dot_;
	pre_x = vehicle_model.pre_x_;
	pre_y = vehicle_model.pre_y_;
	pre_vx = vehicle_model.pre_vx_;
	pre_vy = vehicle_model.pre_vy_;
	pre_psi = vehicle_model.pre_psi_;
	pre_psi_dot = vehicle_model.pre_psi_dot_;

	std::cerr << "(curr_vx, curr_vy) is : (" << curr_vx << ", " << curr_vy << ")" << std::endl;
}

void LateralController::GetGlobalBahn(const vehicle_control::Bahn &global_bahn_msg)
{
	waypoints_num = global_bahn_msg.waypoints.size();

	std::cerr << "--------------------------------------" << std::endl;
	std::cerr << "get GlobalBahn success" << std::endl;
	std::cerr << "waypoints_num in GetGlobalBahn is " << waypoints_num << std::endl;

	// X and Y coordinate,Theta, curvature of the trajectory
	vector_traj_X = VectorXd::Zero(waypoints_num, 1);
	vector_traj_Y = VectorXd::Zero(waypoints_num, 1);
	vector_traj_Theta = VectorXd::Zero(waypoints_num, 1);
	vector_traj_Kappa = VectorXd::Zero(waypoints_num, 1);
	vector_traj_Velcity = VectorXd::Zero(waypoints_num, 1);

	vehicle_control::Waypoint temp_waypoint;

	for (int i = 0; i < waypoints_num; i++)
	{
		temp_waypoint = global_bahn_msg.waypoints[i];
		vector_traj_X(i) = temp_waypoint.pose.pose.position.x;
		vector_traj_Y(i) = temp_waypoint.pose.pose.position.y;

		tf::Quaternion q(
			temp_waypoint.pose.pose.orientation.x,
			temp_waypoint.pose.pose.orientation.y,
			temp_waypoint.pose.pose.orientation.z,
			temp_waypoint.pose.pose.orientation.w);
		tf::Matrix3x3 m(q);
		double roll, pitch, yaw;
		m.getRPY(roll, pitch, yaw);

		vector_traj_Theta(i) = yaw;
		vector_traj_Kappa(i) = temp_waypoint.curvature;
		vector_traj_Velcity(i) = temp_waypoint.twist.twist.linear.x;
	}
}

void LateralController::ComputeTrajectoryError()
{
	// to avoid Eigen error, the size of matrix cannot be 0
	if (waypoints_num == 0)
	{
		waypoints_num = 10000;
	}

	// resize those vector befor access an element
	vector_traj_X.resize(waypoints_num);
	vector_traj_Y.resize(waypoints_num);
	vector_traj_Theta.resize(waypoints_num);
	vector_traj_Kappa.resize(waypoints_num);
	vector_traj_Velcity.resize(waypoints_num);

	// find the match point with shortest distance between (curr_x, curr_y) and trajectory
	// initialize the distance with the first point of trajectory
	double d_min = std::pow(pre_x - vector_traj_X(0), 2) + std::pow(pre_y - vector_traj_Y(0), 2);
	// the index of the point
	int index_min = 0;

	for (int i = 0; i < waypoints_num; i++)
	{
		double d = std::pow(pre_x - vector_traj_X(i), 2) + std::pow(pre_y - vector_traj_Y(i), 2);
		if (d < d_min)
		{
			d_min = d;
			index_min = i;
		}
	}

	// compute normal and tangent vector of match point
	// normal vector of match point
	Vector2d vector_normal = Vector2d::Zero(2, 1);
	vector_normal(0, 0) = (-std::sin(vector_traj_Theta(index_min)));
	vector_normal(1, 0) = (std::cos(vector_traj_Theta(index_min)));
	// tangent vector of match point
	Vector2d vector_tangent = Vector2d::Zero(2, 1);
	vector_tangent(0, 0) = (std::cos(vector_traj_Theta(index_min)));
	vector_tangent(1, 0) = (std::sin(vector_traj_Theta(index_min)));

	// distance vector, from trajectory to current position
	Vector2d vector_distance = Vector2d::Zero(2, 1);
	vector_distance(0, 0) = (curr_x - vector_traj_X(index_min));
	vector_distance(1, 0) = (curr_y - vector_traj_Y(index_min));

	// distance error
	double ed = vector_normal.transpose() * vector_distance;
	// the arc between match point and projection point
	double es = vector_tangent.transpose() * vector_distance;

	// two different assumptions:
	// assumption 1: match point may not be the projection point
	double projection_point_theta = vector_traj_Theta(index_min) + vector_traj_Kappa(index_min) * es;
	// assumption 2: match point is the projection point
	// double projection_point_theta = vector_traj_Theta(index_min);

	// distace err rate
	double ed_dot = pre_vy * std::cos(pre_psi - projection_point_theta) +
					pre_vx * std::sin(pre_psi - projection_point_theta);

	double s_dot = (pre_vx * std::cos(pre_psi - projection_point_theta) -
					pre_vy * std::sin(pre_psi - projection_point_theta)) /
				   (1 - vector_traj_Kappa(index_min) * ed);

	// yaw angle error, use std::sin to solve multi-value problems
	double epsi = std::sin(pre_psi - projection_point_theta);
	// yaw angle error rate
	double epsi_dot = pre_psi_dot - vector_traj_Kappa(index_min) * s_dot;

	// error matrix
	// matrix_err = MatrixXd::Zero(4, 1);
	matrix_err(0, 0) = ed;
	matrix_err(1, 0) = ed_dot;
	matrix_err(2, 0) = epsi;
	matrix_err(3, 0) = epsi_dot;

	// curvature of match point, later will be used in FeedForwordControl
	traj_k = vector_traj_Kappa(index_min);
	traj_velocity = vector_traj_Velcity(index_min);
}

void LateralController::InitMatrix()
{
	// initialize error state matrix A
	// see Rajesh Rajamani "Vehicle Dynamics and Control" Chapter2, Page38, Equ2.45
	matrix_a(0, 1) = 1.0;
	matrix_a(1, 2) = 2.0 * (cf + cr) / m;
	matrix_a(2, 3) = 1.0;
	matrix_a(3, 2) = 2.0 * (lf * cf - lr * cr) / iz;

	matrix_a(1, 1) = -2.0 * (cf + cr) / m;
	matrix_a(1, 3) = 2.0 * (lr * cr - lf * cf) / m;
	matrix_a(3, 1) = 2.0 * (lr * cr - lf * cf) / iz;
	matrix_a(3, 3) = -2.0 * (lf * lf * cf + lr * lr * cr) / iz;

	matrix_b(1, 0) = 2.0 * cf / m;
	matrix_b(3, 0) = 2.0 * cf * lf / iz;

	matrix_q(0, 0) = lqr_q1;
	matrix_q(1, 1) = lqr_q2;
	matrix_q(2, 2) = lqr_q3;
	matrix_q(3, 3) = lqr_q4;

	matrix_r(0, 0) = lqr_r1;
}

void LateralController::ComputeMatrix()
{
	// update matrix A
	matrix_a(1, 1) = matrix_a(1, 1) / pre_vx;
	matrix_a(1, 3) = matrix_a(1, 3) / pre_vx;
	matrix_a(3, 1) = matrix_a(3, 1) / pre_vx;
	matrix_a(3, 3) = matrix_a(3, 3) / pre_vx;

	// discrete matrix A and B for discrete LQR
	MatrixXd matrix_i = MatrixXd::Identity(4, 4);
	matrix_a_discrete = (matrix_i - ts * 0.5 * matrix_a).inverse() * (matrix_i + ts * 0.5 * matrix_a);
	matrix_b_discrete = matrix_b * ts;

	// solve LQR-Problem to get gain matrix K
	MatrixXd matrix_a_discrete_t = matrix_a_discrete.transpose();
	MatrixXd matrix_b_discrete_t = matrix_b_discrete.transpose();
	MatrixXd matrix_p = matrix_q;

	int lqr_num = 0;
	while (lqr_num < lqr_max_iteration)
	{
		// solve discrete time algebraic Riccati equation
		// P_next = Q + AT * P * A - AT * P * B * (R + BT * P * B).inverse() * BT * P * A;
		MatrixXd matrix_p_next = matrix_q + matrix_a_discrete_t * matrix_p * matrix_a_discrete -
								 matrix_a_discrete_t * matrix_p * matrix_b_discrete * (matrix_r + matrix_b_discrete_t * matrix_p * matrix_b_discrete).inverse() *
									 matrix_b_discrete_t * matrix_p * matrix_a_discrete;
		lqr_num++;
		matrix_p = matrix_p_next;
	}

	// K = (R + BT * P * B).inverse() * BT * P * A;
	matrix_k = (matrix_r + matrix_b_discrete_t * matrix_p * matrix_b_discrete).inverse() *
			   matrix_b_discrete_t * matrix_p * matrix_a_discrete;

	// std::cerr << "matrix_k is " << matrix_k << std::endl;
	// std::cerr << "--------------------------------------" << std::endl;
}

void LateralController::ComputeFeedForward()
{
	// compute FeerForword angle,page57,equation 3.12
	// understeer gradient kv
	double kv = lr * m / 2 / cf / (lf + lr) - lf * m / 2 / cr / (lf + lr);
	double ay = pre_vx * pre_vx * traj_k;
	k3 = matrix_k(0, 2);

	double feedforword_part1 = traj_k * (lf + lr);
	double feedforword_part2 = kv * ay;
	double feedforword_part3 = lr * traj_k - lf * m * pre_vx * pre_vx * traj_k / 2 / cr / (lf + lr);

	steering_angle_feedforword = feedforword_part1 + feedforword_part2 - k3 * feedforword_part3;
	// or use equation 3.11
	// steering_angle_feedforword = traj_k * (lf + lr - lr * k3 +
	// (m * pre_vx * pre_vx / (lf + lr)) * (lr / 2 / cf  - lf / 2 / cr + (lf / 2 / cr) * k3));

	std::cerr << "steering_angle_feedforword is " << steering_angle_feedforword << std::endl;
}

void LateralController::ComputeFinalSteerAngle()
{
	// compute Feedback angle
	// if (!std::isnan(matrix_err(0, 0)) && !std::isnan(matrix_k(0, 0)))
	if ((matrix_err(0, 0)) != 0 && (matrix_k(0, 0)) != 0)
	{
		MatrixXd matrix_feedback = MatrixXd::Zero(1, 1);
		matrix_feedback = matrix_k * matrix_err;
		steering_angle_feedback = matrix_feedback(0, 0);
	}
	// std::cerr << "steering_angle_feedback is " << steering_angle_feedback << std::endl;

	// compute final steering angle
	lateral_steering_angle = -steering_angle_feedback + steering_angle_feedforword;

	// steering angle range constrain (-max_steering_angle, max_steering_angle)
	if (lateral_steering_angle > max_steering_angle)
	{
		lateral_steering_angle = max_steering_angle;
	}
	if (lateral_steering_angle < -max_steering_angle)
	{
		lateral_steering_angle = -max_steering_angle;
	}

	// decelerate phase
	//original: if (traj_velocity < 2.0)
	if(traj_velocity < 0.2)
	{
		lateral_steering_angle = 0.0;
	}

	// // steering velocity constrain
	// double topic_publish_rate = 10;
	// double steering_angle_diff = lateral_steering_angle - last_steering_angle;
	// if (std::abs(steering_angle_diff) > (max_steering_rate / topic_publish_rate))
	// {
	// 	if (steering_angle_diff > 0) // turn left
	// 	{
	// 		lateral_steering_angle = last_steering_angle + max_steering_rate / topic_publish_rate;
	// 	}
	// 	else // turn right
	// 	{
	// 		lateral_steering_angle = last_steering_angle - max_steering_rate / topic_publish_rate;
	// 	}
	// }
	// last_steering_angle = lateral_steering_angle;

	std::cerr << "lateral_steering_angle is " << lateral_steering_angle << std::endl;
	std::cerr << "--------------------------------------" << std::endl;
}