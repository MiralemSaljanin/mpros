/**
* @file VehicleModel.cpp
*
* @brief create a vehicle model
*
* @author Yang Qiu
*
* @date 05.12.2020
*/

#include <VehicleModel.h>
#include <VehicleParams.h>

VehicleModel::VehicleModel(const VehicleParams &vehicle_params) : g_(vehicle_params.g),
																  mass_(vehicle_params.m),
																  inertia_(vehicle_params.inertia),
																  friction_coeff_(vehicle_params.friction_coeff),
																  distance_f_(vehicle_params.distance_f),
																  distance_r_(vehicle_params.distance_r),
																  distance_track_(vehicle_params.distance_track),
																  cornering_stiffness_f_(vehicle_params.cornering_stiffness_f),
																  cornering_stiffness_r_(vehicle_params.cornering_stiffness_r),
																  wheel_radius_(vehicle_params.wheel_radius),
																  max_steering_angle_(vehicle_params.max_steering_angle),
																  max_velocity_(vehicle_params.max_velocity),
																  dt_(vehicle_params.dt),
																  max_steering_rate_(vehicle_params.max_steering_rate)
{
}

VehicleModel::~VehicleModel()
{
}

void VehicleModel::GetPosition(const geometry_msgs::PoseStamped::ConstPtr &pose_msg)
{


	geometry_msgs::PoseStamped pose_world, pose_map;

	pose_map.pose = pose_msg->pose;


	curr_x_ = pose_map.pose.position.x;
	curr_y_ = pose_map.pose.position.y;

	tf::Quaternion q(
		pose_map.pose.orientation.x,
		pose_map.pose.orientation.y,
		pose_map.pose.orientation.z,
		pose_map.pose.orientation.w);
	tf::Matrix3x3 m(q);
	double roll, pitch, yaw;
	m.getRPY(roll, pitch, yaw);

	curr_psi_ = yaw;
}

void VehicleModel::GetVelocity(const geometry_msgs::Twist::ConstPtr &vel_msg)
{
	global_vX_ = vel_msg->linear.x;
	global_vY_ = vel_msg->linear.y;
	global_v_ = hypot(global_vX_, global_vY_);

	course_angle_ = std::atan2(global_vY_, global_vX_);
	slip_angle_ = course_angle_ - curr_psi_;

	// velocity in vehicle coordinate, used in LQR
	curr_vx_ = std::abs(global_v_ * std::cos(slip_angle_));
	curr_vy_ = global_v_ * std::sin(slip_angle_);

	curr_psi_dot_ = vel_msg->angular.z;
}

void VehicleModel::ComputePredictState()
{
	pre_x_ = curr_x_ + curr_vx_ * dt_ * std::cos(curr_psi_) - curr_vy_ * dt_ * std::sin(curr_psi_);
	pre_y_ = curr_y_ + curr_vx_ * dt_ * std::sin(curr_psi_) + curr_vy_ * dt_ * std::cos(curr_psi_);
	pre_vx_ = curr_vx_;
	pre_vy_ = curr_vy_;
	pre_psi_ = curr_psi_ + curr_psi_dot_ * dt_;
	pre_psi_dot_ = curr_psi_dot_;
}
